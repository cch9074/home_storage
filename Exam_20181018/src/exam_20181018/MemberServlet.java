package exam_20181018;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/member")
public class MemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public MemberServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String sex = request.getParameter("sex");
		String job = request.getParameter("job");
		String[] hoby = request.getParameterValues("hoby"); 
		
		String so = request.getParameter("so");
		
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml>");
		out.print("<head>");
		out.print("<title>회원가입 정보입니다.</title>");
		out.print("<script>");
		if(hoby==null) {
			out.print("alert('취미를 1개 이상 선택하세요');");
			out.print("history.go(-1)");
		
		}
		out.print("</script>");
		out.print("</head>");
		out.print("<body>");
		out.print("아이디 : "+ id+"<p>");
		out.print("비번 : "+ pw +"<p>");
		out.print("성별 : "+ sex +"<p>");
		out.print("직업 : "+ job +"<p>");
		if(hoby!=null) {
			out.print("취미 : ");
			for(String i:hoby) {
				out.print(i+" ");
			}
		}
	
		out.print("<p>");
		out.print("자기소개 : "+ so +"<p>");
		out.print("</body>");
		out.print("</httml>");
		out.close();
	}

}
