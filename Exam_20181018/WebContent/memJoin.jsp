<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script>
	function check(){
		if(joinFrm.id.value==""){
			alert("ID를 입력해주세요");
			joinFrm.id.focus();
			return false;
		}
		if(joinFrm.pw.value==""){
			alert("PW를 입력해주세요");
			joinFrm.pw.focus();
			return false;
		}
		if(joinFrm.job.selectedIndex==0){
			alert("직업을 선택해주세요");
			joinFrm.job.focus();
			return false;
		}
	


				
	}
</script>
</head>
<body>
	<form name="joinFrm" method="post" action="member">
		<table border="1" width="500">
			<tr>
				<td colspan="2" align="center">회원가입</td> 
			</tr>
			<tr>
				<td width="20%" align="center">ID</td> <td><input type="text" name="id"></td>
			</tr>
			<tr>
				<td width="20%" align="center">PW</td> <td><input type="password" name="pw"></td>
			</tr>
			<tr>
				<td width="20%" align="center">성별</td> 
				<td><input type="radio" name="sex" value="남자" checked>남자
					<input type="radio" name="sex" value="여자">여자
				</td>
			</tr>
			<tr>
				<td width="20%" align="center">직업</td> 
				<td><select name="job">
					<option>직업을 선택하세요</option>
	7				<option value ="공무원">공무원</option>
					<option value ="회사원">회사원</option>
					<option value ="학생">학생</option>
					<option value ="주부">주부</option>
					<option value ="기타">기타</option>
				</td>
				</td>
			</tr>
			<tr>
				<td width="20%" align="center">취미</td> 
				<td>
					<input type="checkbox" name="hoby" value="축구" >축구
					<input type="checkbox" name="hoby" value="탁구">탁구
					<input type="checkbox" name="hoby" value="등산">등산
					
				</td>
			</tr>
			<tr>
				<td width="20%" align="center">자기소개</td>
				<td>
					<textarea rows="3" cols="30" name="so"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="등록" onClick="return check()"> 	
					<input type="reset" value="취소"> 	
				</td>
			</tr>
		</table>
	</form>
</body>
</html>