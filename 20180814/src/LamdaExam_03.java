@FunctionalInterface
interface CC{
	int method(int a, int b) ;
}
public class LamdaExam_03 {

	public static void main(String[] args) {

		CC cf;
		cf =(a,b)->{
			return a+b;
		};
		int reusult =cf.method(130, 20);
		System.out.println(reusult);
		//return을 생략할수 있는 경우도 있다.
		cf = (a,b)->a+b;
		
		System.out.println(cf.method(10, 20));
	}

}
