package stream;
import java.util.*;
import java.util.stream.Stream;
class Student{
	private String name;
	private int score;
	Student(){}
	Student(String name, int score){
		this.name = name;
		this.score = score;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
public class StudentExam {
	
	public static void main(String[] args) {
		List<Student> list = new ArrayList();
		list.add(new Student("ȫ�浿",55));
		list.add(new Student("�迵��",66));
		list.add(new Student("��ö��",77));
		
		Stream<Student> stream = list.stream();
		stream.forEach(s-> {
			String name = s.getName();
			int score = s.getScore();
			System.out.println("�̸� : " +name + ", ����: " +score);
		});
		
	}
	
}
