package stream;
import java.util.*;
import java.util.stream.Stream;
public class StreamExam02 {

	public static void main(String[] args) {
		List<String> list =Arrays.asList("��ö��", "�迵��", "ȫ�浿");
		
		Stream<String> stream = list.stream();
		stream.forEach(StreamExam02 :: print);
		System.out.println("===========");
		
		Stream<String> stream2 = list.parallelStream();
		stream2.forEach(StreamExam02 :: print);

	}

	public static void print(String str) {
		System.out.println(str +":"+ Thread.currentThread().getName());
	}
}
