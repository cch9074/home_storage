package stream;
import java.util.*;
import java.util.stream.Stream;
class Student2{
	private String name;
	private int score;
	Student2(){}
	Student2(String name, int score){
		this.name = name;
		this.score = score;
	}
	public String getName() {
		return name;
	}
	
	public int getScore() {
		return score;
	}
}
public class StudentExam2 {

	public static void main(String[] args) {
		List<Student2> list = Arrays.asList(
				new Student2 ("��ö��",60)
				,new Student2 ("�迵��",78)
				,new Student2 ("ȫ�浿",87));
		
		double avg = list.stream()				//��Ʈ�� ����
				.mapToInt(Student2 :: getScore)	//�� ��������
				.average()						//��� ���ϱ�
				.getAsDouble();					//�� ��������
		System.out.println("��� : " + avg);
		
	}
}