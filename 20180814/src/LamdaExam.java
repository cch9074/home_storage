interface AA{
	
	void method() ;
	
}
class AAEx implements AA{
	
	public void method() {
		
		System.out.println("AAAA");
		
	}
}
public class LamdaExam {
	
	public static void main(String[] args) {
		
		AAEx a = new AAEx();
		a.method();
		AA af;
		//임플리먼트 하지 않고 클래스내에서 1회용으로 오버라이딩할수 있다.(익명개채)
		af = ()->{
			System.out.println("BBBB");
		};
		
		af.method();
		af = ()-> {
			
			String str = "Method1 Call";
			System.out.println(str);
		};
		
		af.method();
		a.method();
	}
}