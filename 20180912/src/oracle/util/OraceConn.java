package oracle.util;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class OraceConn {
	
	String myDriver = "oracle.jdbc.driver.OracleDriver";
	String myUrl = "jdbc:oracle:thin:@localhost:1521/xe";
	String myID = "hr";
	String myPass = "1234";
	
	protected Connection myConn;
	public OraceConn() {}
	
	public void makeConn() throws Exception{
		Class.forName(myDriver);
		myConn = DriverManager.getConnection(myUrl, myID, myPass);
		
	}
	public abstract void cleanup() throws Exception;
	

	public void takeDown() throws Exception{
		cleanup();
		myConn.close();
	}
	
}

