package oracle.test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import oracle.util.OraceConn;

public class OracleSQL extends OraceConn{
	PreparedStatement pstmt =null;			//SQL문 불러오기
	ResultSet rs = null;					//결과 저장
	String quary="";
	
	public OracleSQL() {
		super();
	}
	
	
	public List<EmployeeBean> getPay(int money) throws Exception{
		List<EmployeeBean> list = new ArrayList();
		EmployeeBean bean =null;
		quary = "select ename,salary from employee where salary>"+money;
		pstmt = myConn.prepareStatement(quary);
		rs = pstmt.executeQuery();
		
		while(rs.next()) {
			bean = new EmployeeBean();
			bean.setEname(rs.getString("ename"));
//			System.out.println(bean.getEname());
			bean.setSalary(rs.getInt("salary"));
			list.add(bean);
		}
		
		return list;
	}
	public EmployeeBean getMem(int eno) throws Exception{
		EmployeeBean bean = null;
		quary ="select ename,dno from employee where eno = "+eno;
		pstmt = myConn.prepareStatement(quary);
		rs = pstmt.executeQuery();
		
		if(rs.next()) {
			bean = new EmployeeBean();
			bean.setEname(rs.getString("ename"));
			bean.setDno(rs.getInt("dno"));
		}
		
		return bean;
	}
	
	@Override
	public void cleanup() throws Exception {
	
		
	}
	

}
