import java.sql.Connection;
import java.sql.DriverManager;

import org.omg.CORBA.ExceptionList;

public class OracleTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String myDriver = "oracle.jdbc.driver.OracleDriver";
		String myUrl = "jdbc:oracle:thin:@localhost:1521/xe";
		String myID = "hr";
		String myPass = "1234";
		
		Connection conn;
		try {
			Class.forName(myDriver);
			System.out.println("드라이버 로딩 성공");
		}catch(Exception e) {
			System.out.println("드라이버 로싱 실패");
		}
		try {
			conn = DriverManager.getConnection(myUrl, myID, myPass);
			System.out.println("oracle 연결 성공");
		}catch(Exception e) {
			System.out.println("oracle 연결 실패");
		}
	}

}
