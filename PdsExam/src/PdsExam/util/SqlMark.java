package PdsExam.util;

public class SqlMark {
	  public SqlMark() {}

		// // 줄바꿈(Enter)을 html에서 적용시키는 함수
	  public static String lineBreak(String src) {
	    int len = src.length();
	    int linenum = 0, i = 0;

	    for (i = 0; i < len; i++)
	      if (src.charAt(i) == '\n')
	        linenum++;

	    StringBuffer dest = new StringBuffer(len + linenum*3);

	    for (i = 0; i < len; i++) {
	      if (src.charAt(i) == '\n')
	        dest.append("<br>");
	      else
	        dest.append(src.charAt(i));
	    }
	    return dest.toString();
	  }
	  
		// 따옴표(') 사용시 sql문에 맞게 변화하는 함수
	  public static String qMark(String src) {
	    int len = src.length();
	    int linenum = 0, i = 0;

	    for (i = 0; i < len; i++)
	      if (src.charAt(i) == '\'')
	        linenum++;

	    StringBuffer dest = new StringBuffer(len + linenum);

	    for (i = 0; i < len; i++) {
	      if (src.charAt(i) == '\'')
	        dest.append("''");
	      else
	        dest.append(src.charAt(i));
	    }
	    return dest.toString();
	  }

		// tap 사용시 tap 효과(4칸) 내주는 함수
		public static String tabKey(String src) {
	    int len = src.length();
	    int linenum = 0, i = 0;

	    for (i = 0; i < len; i++)
	      if (src.charAt(i) == '\t')
	        linenum++;

	    StringBuffer dest = new StringBuffer(len + linenum*5);

	    for (i = 0; i < len; i++) {
	      if (src.charAt(i) == '\t')
	        dest.append("    ");
	      else
	        dest.append(src.charAt(i));
	    }
	    return dest.toString();
	  }

		// 답글 작성시 본글 앞에 > 표시를 달아주는 함수
		public static String reply(String src) {
	    int len = src.length();
	    int linenum = 0, i = 0;

	    for (i = 0; i < len; i++)
	      if (src.charAt(i) == '\n')
	        linenum++;

	    StringBuffer dest = new StringBuffer(len + linenum+1);
	    
	    dest.append(">");

	    for (i = 0; i < len; i++) {
	      if (src.charAt(i) == '\n')
	        dest.append(">");
	      else
	        dest.append(src.charAt(i));
	    }
	    return dest.toString();
	  }

		// text를 html 태그로 바꿔주는 함수
		public static String htmlTag(String src) {
	    int len = src.length();
	    int linenum = 0, i = 0;

	    for (i = 0; i < len; i++)
	      if((src.charAt(i)=='<')|(src.charAt(i)=='>')|(src.charAt(i)=='&')|(src.charAt(i)==' '))
	        linenum++;

	    StringBuffer dest = new StringBuffer(len + linenum*5);
	    
	    for (i = 0; i < len; i++) {
	      if (src.charAt(i) == '<')
	      	dest.append("&lt;");
	      else if (src.charAt(i) == '>')
	        dest.append("&gt;");
	      else if (src.charAt(i) == '&')
	        dest.append("&amp;");
	      else if (src.charAt(i) == ' ') {
	        if(i==0)
	        	dest.append("&nbsp;");
	        else {
	        	if(src.substring(i-1,i).equals(" ")|src.substring(i-1,i).equals("\n")) {
	        		if(src.substring(i+1,i+2).equals(" "))
	        			dest.append("&nbsp;");
	        		else {
	        			if(src.substring(i-2,i-1).equals(" "))
	        				dest.append(" ");
	        			else
	        				dest.append("&nbsp;");
	        		}
	     			}
	     			else
	     				dest.append(" ");
	        }
	      }
	      else
	      	dest.append(src.charAt(i));
	    }
	    return dest.toString();
	  }
	}