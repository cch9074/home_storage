package PdsExam.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;








public class PdsDAO {
	
	private PdsDAO() {}
	
	private static PdsDAO instance = new PdsDAO();
	
	public static PdsDAO getInstance() {
		return instance;
	}
	public List pdsList() {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<PdsVO> list = new ArrayList();
		
		String quary = "select * from hr_pds order by num desc";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			rs = pstmt.executeQuery();
			
			PdsVO pVO = null;
			while(rs.next()) {
				pVO = new PdsVO();
				pVO.setNum(rs.getInt("num"));
				pVO.setName(rs.getString("name"));
				pVO.setSubject(rs.getString("subject"));
				pVO.setContents(rs.getString("contents"));
				pVO.setRegdate(rs.getTimestamp("regdate"));
				pVO.setReadcnt(rs.getInt("readcnt"));
				pVO.setPass(rs.getString("pass"));
				pVO.setFilename(rs.getString("filename"));
				list.add(pVO);
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return list;
		}
		
	}
	
	public List pdsList(int listcount,int pageStart) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<PdsVO> list = new ArrayList();
		
		String quary = "select x.* from (select * from hr_pds where rownum < ?"
				+"order by num desc) x , (select num from hr_pds where rownum < ? "
				+"order by num asc) y  where x.num = y.num order by x.num desc";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			pstmt.setInt(1, pageStart);
			pstmt.setInt(2, listcount);
			rs = pstmt.executeQuery();
			
			PdsVO pVO = null;
			while(rs.next()) {
				pVO = new PdsVO();
				pVO.setNum(rs.getInt("num"));
				pVO.setName(rs.getString("name"));
				pVO.setSubject(rs.getString("subject"));
				pVO.setContents(rs.getString("contents"));
				pVO.setRegdate(rs.getTimestamp("regdate"));
				pVO.setReadcnt(rs.getInt("readcnt"));
				pVO.setPass(rs.getString("pass"));
				pVO.setFilename(rs.getString("filename"));
				list.add(pVO);
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return list;
		}
		
	}
	public List pdsList(String s_query,int listcount,int pageStart) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<PdsVO> list = new ArrayList();
		
		String quary = "select x.* from (select * from hr_pds where rownum < ? and "+s_query
				+"order by num desc) x , (select num from hr_pds where rownum < ? and "+s_query
				+"order by num asc) y  where x.num = y.num order by x.num desc";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			pstmt.setInt(1, pageStart);
			pstmt.setInt(2, listcount);
			rs = pstmt.executeQuery();
			
			PdsVO pVO = null;
			while(rs.next()) {
				pVO = new PdsVO();
				pVO.setNum(rs.getInt("num"));
				pVO.setName(rs.getString("name"));
				pVO.setSubject(rs.getString("subject"));
				pVO.setContents(rs.getString("contents"));
				pVO.setRegdate(rs.getTimestamp("regdate"));
				pVO.setReadcnt(rs.getInt("readcnt"));
				pVO.setPass(rs.getString("pass"));
				pVO.setFilename(rs.getString("filename"));
				list.add(pVO);
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return list;
		}
		
	}
	
	public PdsVO pdsSelect(int num) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		PdsVO pVO = null;
		
		String quary = "select * from hr_pds where num = ?";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			pstmt.setInt(1, num);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				pVO = new PdsVO();
				pVO.setNum(rs.getInt("num"));
				pVO.setName(rs.getString("name"));
				pVO.setSubject(rs.getString("subject"));
				pVO.setContents(rs.getString("contents"));
				pVO.setRegdate(rs.getTimestamp("regdate"));
				pVO.setReadcnt(rs.getInt("readcnt"));
				pVO.setPass(rs.getString("pass"));
				pVO.setFilename(rs.getString("filename"));
				
			}		
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return pVO;
		}
		
	}
	
	public int insertPsd(PdsVO pVO) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		int row =0;
		String quary = "insert into hr_pds(num,name,subject,contents,regdate,readcnt,pass,filename)"
		        + 		"values(hr_pds_seq.nextval,?,?,?,?,?,?,?)";
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			
			pstmt.setString(1, pVO.getName());
			pstmt.setString(2, pVO.getSubject());
			pstmt.setString(3, pVO.getContents());
			pstmt.setTimestamp(4, pVO.getRegdate());
			pstmt.setInt(5, 0);
			pstmt.setString(6, pVO.getPass());
			pstmt.setString(7, pVO.getFilename());
			
			pstmt.executeUpdate();
			
			row =1;
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
		
	}
	
	public int pdsDelete(PdsVO pVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row = 0;
		String quary = "delete from hr_pds where num= ?";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			pstmt.setInt(1, pVO.getNum());
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return row;
		}
	}

	public int pdsEdit(PdsVO pVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		//ResultSet rs = null;
		int row = 0;
		String quary = "update hr_pds set subject =? ,contents=? ,filename = ? where num= ?";
		
		try {
			conn = DBManager.getConnection();
			pstmt =  conn.prepareStatement(quary);
			
			pstmt.setString(1, pVO.getSubject());
			pstmt.setString(2, pVO.getContents());	
			pstmt.setString(3, pVO.getFilename());
			pstmt.setInt(4, pVO.getNum());
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
		
	}
	
	public int pdsCount() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "select count(*) counter from hr_pds";
		int row = 0;
			try {
				conn = DBManager.getConnection();
				pstmt =  conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				if(rs.next()) {
					row = rs.getInt("counter");
					
				}		
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					DBManager.close(conn,pstmt,rs);
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return row+1;
			}
			
		}
	
	public int pdsCount(String s_query) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query ="";
		
		int row = 0;
			try {
				conn = DBManager.getConnection();
				if(!s_query.equals("")) {
					s_query="where " + s_query;
				}				
				query = "select count(*) from hr_pds "+s_query;
				pstmt =  conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				if(rs.next()) {
					row = rs.getInt(1);
					
				}		
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					DBManager.close(conn,pstmt,rs);
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return row;
			}
			
		}
	
	public void pdsCountNo(int num) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "update hr_pds set readcnt = readcnt +1 where num= ?";
		PdsVO pVO = null;
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				DBManager.close(conn,pstmt,rs);
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			
		}
	
	}

}
