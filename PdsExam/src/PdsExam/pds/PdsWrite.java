package PdsExam.pds;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import PdsExam.db.PdsDAO;
import PdsExam.db.PdsVO;

/**
 * Servlet implementation class PdsWrite
 */
@WebServlet("/write.do")
public class PdsWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdsWrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_write.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");

		//파일첨부
		try {
			ServletContext context = getServletContext();
			String path = context.getRealPath("upload");
			String encType = "utf-8";
			int uploadFileSizeLimit = 2*1024*1024;
			MultipartRequest multi = new MultipartRequest(request, path,uploadFileSizeLimit,encType, new DefaultFileRenamePolicy());
			//중복된 이름을 바꿔서 등록하는 역할
			String name = multi.getParameter("name");
			String subject = multi.getParameter("subject");
			String contents = multi.getParameter("contents");
			String pass = multi.getParameter("pass");
			//한개만 받을 경우
			String filename = multi.getFilesystemName("filename");
		
			Timestamp now = new Timestamp(System.currentTimeMillis());
			
			PdsDAO pDAO = PdsDAO.getInstance();
			PdsVO pVO = new PdsVO();
			
			pVO.setName(name);
			pVO.setSubject(subject);
			pVO.setContents(contents);
			pVO.setPass(pass);
			pVO.setRegdate(now);
			pVO.setFilename(filename);
			
			pDAO.insertPsd(pVO);
		}catch(Exception e) {
			e.getMessage();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("list.do");
		rd.forward(request, response);
			
	}

}
