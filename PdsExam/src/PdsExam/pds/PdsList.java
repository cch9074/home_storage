package PdsExam.pds;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import PdsExam.db.PdsDAO;
import PdsExam.db.PdsVO;

/**
 * Servlet implementation class PdsList
 */
@WebServlet("/list.do")
public class PdsList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdsList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PdsDAO pDAO = PdsDAO.getInstance();
		String url="list.do";
		String s_query="",addtag="",key="";
		int nowpage = 1;//시작 페이지
		int maxlist=5; //페이지당 라인수 
		int totpage=1;
		int totcount = pDAO.pdsCount();
		if(totcount%maxlist==0) {
			totpage = (int)(totcount/maxlist);
		}else{
			totpage = (int)(totcount/maxlist) + 1;
		}
		if(totpage ==0) {			//게시글 총 수가 10개(maxlist)가 안될경우
			totpage = 1;
		}
		if(request.getParameter("page") !=null && !request.getParameter("page").equals("")){
			nowpage = Integer.parseInt(request.getParameter("page"));
			//url="list.do?page="+nowpage;
		}
		if(nowpage>totpage) {
			nowpage = totpage;	
		}
		//총 리스트에서 현재페이지 시작번호 구하기
		int pageStart = nowpage * maxlist + 1;
		int listcount = totcount - ((nowpage-1)*maxlist)  ;
		List<PdsVO> list = pDAO.pdsList(listcount,pageStart);
		request.setAttribute("addtag", addtag);
		request.setAttribute("url", url);
		request.setAttribute("nowpage", nowpage);
		request.setAttribute("totpage", totpage);
		request.setAttribute("totcount", totcount);
		request.setAttribute("pageStart", pageStart);
		request.setAttribute("listcount", listcount);
		request.setAttribute("list", list);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_list.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		PdsDAO pDAO = PdsDAO.getInstance();
		String url="list.do";
		String s_query="",addtag="",key="" ,query="";
		
		if(request.getParameter("key") != null && !request.getParameter("key").equals("")) {
			query = request.getParameter("search");
			key = request.getParameter("key");
			//s_query = "where" + query + "like '%"+ key + "%'";
			s_query = query + " like '%"+ key + "%'";
			addtag = "&search=" + query + "&key="+key;
		}
		
		int nowpage = 1;//시작 페이지
		int maxlist=5; //페이지당 라인수 
		int totpage=1;
		int totcount = pDAO.pdsCount(s_query);
		if(totcount%maxlist==0) {
			totpage = totcount/maxlist;
		}else{
			totpage = totcount/maxlist + 1;
		}
		if(totpage ==0) {			//게시글 총 수가 10개(maxlist)가 안될경우
			totpage = 1;
		}
		if(request.getParameter("page") !=null && !request.getParameter("page").equals("")){
			nowpage = Integer.parseInt(request.getParameter("page"));
			//url="list.do?page="+nowpage;
		}
		if(nowpage>totpage) {
			nowpage = totpage;	
		}
		//총 리스트에서 현재페이지 시작번호 구하기
		int pageStart = nowpage * maxlist + 1;
		int listcount = totcount - ((nowpage-1)*maxlist) +1 ;
		//List<PdsVO> list = pDAO.pdsList(listcount,pageStart);
		List<PdsVO> list = pDAO.pdsList(s_query,listcount,pageStart);
		
		request.setAttribute("addtag", addtag);
		request.setAttribute("url", url);
		request.setAttribute("nowpage", nowpage);
		request.setAttribute("totpage", totpage);
		request.setAttribute("totcount", totcount);
		request.setAttribute("pageStart", pageStart);
		request.setAttribute("listcount", listcount);
		request.setAttribute("list", list);
		
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_list.jsp");
		rd.forward(request, response);
	}

}
