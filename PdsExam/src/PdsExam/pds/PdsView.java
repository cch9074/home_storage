package PdsExam.pds;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import PdsExam.db.PdsDAO;
import PdsExam.db.PdsVO;

/**
 * Servlet implementation class PdsView
 */
@WebServlet("/view.do")
public class PdsView extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdsView() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		PdsDAO pDAO = PdsDAO.getInstance();
		Cookie[] cookies = request.getCookies();
		//Cookie cookieEx = null;
		boolean found = false;
		
		for(Cookie c : cookies) {
			if(c.getName().equals("id"+num)) {
				found = true;
				break;
			}
		}
		//쿠키들 에서 이름 얻어온다음에 이름 같으면 그냥넘기고 없으면 새로만들고 조회수 올려준다 
		if(!found) {
		 	Cookie c = new Cookie("id"+num,"host");		//쿠키 만듬
		 	c.setMaxAge(24*60*60);					//유효시간
		 	response.addCookie(c);					//보냄
		 	pDAO.pdsCountNo(num);
		}
		
		PdsVO pVO = pDAO.pdsSelect(num);
		request.setAttribute("pVO", pVO);
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_view.jsp");
		rd.forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
