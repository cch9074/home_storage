package PdsExam.pds;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import PdsExam.db.PdsDAO;
import PdsExam.db.PdsVO;

/**
 * Servlet implementation class PdsDelete
 */
@WebServlet("/delete.do")
public class PdsDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdsDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt((request.getParameter("num")));
		PdsDAO pDAO = PdsDAO.getInstance();
		PdsVO pVO = pDAO.pdsSelect(num);
		request.setAttribute("pVO", pVO);
		
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_delete.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int num = Integer.parseInt((request.getParameter("num")));
		PdsDAO pDAO = PdsDAO.getInstance();
		PdsVO pVO = pDAO.pdsSelect(num);
		int row = pDAO.pdsDelete(pVO);
		request.setAttribute("row", row);
		
		RequestDispatcher ds = request.getRequestDispatcher("Pds/pds_delete_pro.jsp");
		ds.forward(request, response);
	}

}
