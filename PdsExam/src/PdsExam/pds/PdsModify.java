package PdsExam.pds;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import PdsExam.db.PdsDAO;
import PdsExam.db.PdsVO;

/**
 * Servlet implementation class PdsModify
 */
@WebServlet("/modify.do")
public class PdsModify extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdsModify() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		PdsDAO pDAO = PdsDAO.getInstance();
		PdsVO pVO = pDAO.pdsSelect(num);
		request.setAttribute("pVO", pVO);
		request.setAttribute("filename", pVO.getFilename());
		RequestDispatcher rd = request.getRequestDispatcher("Pds/pds_modify.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int num = Integer.parseInt(request.getParameter("num"));
		ServletContext context = getServletContext();
		String path = context.getRealPath("upload");
		String encType = "utf-8";
		int uploadFileSizeLimit = 2*1024*1024;
		MultipartRequest multi = new MultipartRequest(request, path,uploadFileSizeLimit,encType, new DefaultFileRenamePolicy());
		//중복된 이름을 바꿔서 등록하는 역할
		
		String subject = multi.getParameter("subject");
		String contents = multi.getParameter("contents");

		//한개만 받을 경우
		String filename = multi.getFilesystemName("filename");
	
		PdsDAO pDAO = PdsDAO.getInstance();
		PdsVO pVO = pDAO.pdsSelect(num);	
		pVO.setNum(num);
		pVO.setSubject(subject);
		pVO.setContents(contents);
		if(filename!=null) {
			pVO.setFilename(filename);
		}
		pDAO.pdsEdit(pVO);
		
		RequestDispatcher rd = request.getRequestDispatcher("list.do");
		rd.forward(request, response);
		
	}

}
