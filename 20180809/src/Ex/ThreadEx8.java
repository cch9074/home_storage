package Ex;

public class ThreadEx8 {

	public static void main(String[] args) {
		ThreadEx_1 th1 = new ThreadEx_1();
		ThreadEx_2 th2 = new ThreadEx_2();
		
		th2.setPriority(7);
		
		System.out.println("Prority of th1(-) : " + th1.getPriority());
		//우선순위의 기본값은 5 이다.
		System.out.println("Prority of th2(|) : " + th2.getPriority());
		
		th1.start();
		th2.start();	
	}
}
class ThreadEx_1 extends Thread{
	public void run() {
		for(int i=0;i<300;i++) {
			System.out.print("-");
			for(int j=0;j<100000000;j++) ;
		}
	}
}
class ThreadEx_2 extends Thread{
	public void run() {
		for(int i=0;i<300;i++) {
			System.out.print("|");
			for(int j=0;j<100000000;j++); 
		}
	}
}
