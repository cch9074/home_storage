package sync;
//합계출력 클래스
public class PrintThread extends Thread{
	SharedArea area;
	PrintThread(SharedArea area){
		this.area=area;
	}
	@Override
	public void run() {
		for(int i=0;i<3;i++) {
			//동기화 구역 이 실행중에는 다른 쓰레드가 들어오지 못하게한다.
			//동기화 코드방법==> 일정 구역을 동기화 시키수있다
			synchronized(area) {
				int sum = area.acc1.balance + area.acc2.balance;
				System.out.println("계좌 합계 :"+sum);
			}
			try {
				Thread.sleep(5000);
				
			}catch(InterruptedException e ) {
				System.out.println(e.getMessage());
			}
		}
	}

}
