package sync;
//계좌 클래스
public class Account {
	String accountNo;//계좌번호
	String name;//예금주
	int balance;//잔고
	//새로운 계좌를 생성하는 생성자
	Account(String accountNo, String name){
		this.accountNo = accountNo;
		this.name = name;
		this.balance = 0;
	}
	//입금
	public void desposit(int money) {
		balance += money;
	}
	//출금
	public int  withdraw(int money) {
		if(balance>=money) {
			balance-= money;
			return balance;
		}else {
			System.out.println("잔액이 부족합니다.");
			return 0;
		}
	}
}
