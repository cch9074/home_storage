package sync;
//계좌이체를 수행하는 클래스
public class TransferThead extends Thread{
	SharedArea area;
	TransferThead(SharedArea area){
		this.area = area;
	}
	@Override
	public void run() {
		for(int i=0;i<12 ;i++) {
			synchronized(area) {//동기화 구역 이 실행중에는 다른 쓰레드가 들어오지 못하게한다.
				area.acc1.withdraw(100);
				System.out.print(area.acc1.name+ "계좌 : 100원 인출\t");
				area.acc2.desposit(100);
				System.out.println(area.acc2.name + "계좌 : 100원 입금");
				try {
					Thread.sleep(1000);
				}catch(InterruptedException e ) {
					
				}
			}
		}
	}
}
