package sync;

public class AccountExam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SharedArea area = new SharedArea();
		area.acc1 = new Account("000001","김철수");
		area.acc2 = new Account("000002","김영희");
		
		area.acc1.desposit(10000);
		area.acc2.desposit(20000);
		
		TransferThead trans = new TransferThead(area); 
		trans.start();
		PrintThread prt = new PrintThread (area);
		prt.start();
		//입출금이 일어난 뒤에 합계를 구해도 일정한 값이 나오지 않는 경우가생긴다.
		//동기화를 통해 해결..
		//
	}

}
