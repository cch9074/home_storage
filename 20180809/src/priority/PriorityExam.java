package priority;

public class PriorityExam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int i=1;i<=10;i++) {
			Thread thread = new CalcThread("thread" + i);
			if(i!=10) {
				thread.setPriority(thread.MIN_PRIORITY);
				//우선순위가 높다고 무조건 가장 먼저 하는것은 아니다.
			}else {
				thread.setPriority(thread.MAX_PRIORITY);
			}
			thread.start();
		}
		ThreadGroup group = Thread.currentThread().getThreadGroup();
		String name = group.getName();
		System.out.println(name);
	}
}
