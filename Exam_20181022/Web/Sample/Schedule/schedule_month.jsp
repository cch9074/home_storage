<%@ page import="java.sql.*,java.util.*,sample.schedule.*, sample.util.*" contentType="text/html; charset=EUC-KR" %>
<jsp:useBean id="MyBean" class="sample.schedule.ScheduleQuery" scope="page" />

<%
	DateTime date = new DateTime(); //날짜관련 클래스 생성
	int to_year = date.getYear();//오늘날짜 중 연도
	int to_month = date.getMonth();//오늘날짜 중 월
	int to_day = date.getDay();//오늘날짜 중 일
	String syear = Integer.toString(to_year);
	String smonth = Integer.toString(to_month);
	String sday = Integer.toString(to_day);
	if(to_month < 10 ) smonth = "0" + to_month;
	if(to_day < 10 ) sday = "0" + to_day ;
	String today = syear + smonth + sday;//연,월,일 결합

	String year=request.getParameter("start_year");
	String month=request.getParameter("start_month");
	String start_date = year+month;
	
	//스케줄을 보여줄 달(opt가 '0'이면 이번달,opt가 '1'이면 다음달, opt가 '2' 전달)
	int opt = Integer.parseInt(request.getParameter("opt"));
	int REPEAT1=0; //입력된 스케줄의 갯수를 저장할 변수
	String system_date="";
	String this_year="";
	String sys_month="";
	String sys_day="";
	String sql1="";
	String sql2="";

	if(opt == 1){//다음달을 선택하면 현재 달에 +1를 하여 수행
		start_date = ""+(Integer.parseInt(start_date)+1);
	}else if(opt==2){//전달을 선택하면 현재 달에 -1를 하여 수행
		start_date = ""+(Integer.parseInt(start_date)-1);
	}	
	MyBean.makeConn();

	//스케줄 목록이 들어 있는 DB에서 오늘 날짜에 해당하는 날짜 가져오기
	system_date = MyBean.todaySchedule(today);
	if(system_date != null){
		this_year = system_date.substring(0,4);
		sys_month = system_date.substring(4,6);
		sys_day = system_date.substring(6,8);
	}
	//해당하는 달에 글이 몇개인지를 알아내는 쿼리
	REPEAT1 = MyBean.cntSchedule(start_date);
	//해당 달에 있는 글의번호,제목,날짜,내용을 가져오는 메소드 호출 
	Vector vctDate = MyBean.selSchedule(start_date);	
	ScheduleModel sModel = new ScheduleModel();

	String sys_date = today;
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html;charset=euc-kr">
<STYLE TYPE="text/css">
 td   { font-family: 돋움, Verdana; font-size: 9pt; text-decoration: none; color
: #000000;BACKGROUND-POSITION: left top;}
BODY {font-family: "돋움", "Verdana"; font-size: 9pt}
a    { font-family: 돋움, Verdana; color: #000000; text-decoration: none}
a:hover { font-family:돋움; text-decoration:underline }
</STYLE>
<title>Calendar</title>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
    var cdate = "<%=sys_date%>"
    var cy = cdate.substring(0,4) ;
    var cm =    cdate.substring(4,6)-1;
    var cd =    cdate.substring(6,8);
    var dDate = new Date(cy,cm,cd);
    var dCurMonth = dDate.getMonth()+1;
    var dCurDayOfMonth = dDate.getDate();
    var dCurYear = dDate.getFullYear();
    var objPrevElement = new Object();
    var fw;
    var ii;

    function fToggleColorOver(myElement) {
            myElement.bgColor = "#FFFEE5";
    }

    function fToggleColorOut(myElement) {
        if(myElement.id == "calCell") myElement.bgColor = "#FDF5EA";
            else if(myElement.id == "calCelltoday") myElement.bgColor = "#FDF5EA";
            else myElement.bgColor = "#FFFFFF";
    }

    function FontOver(myElement) {
            myElement.bgColor = "#F7E0C3";
    }

    function FontOut(myElement) {
            if(myElement.id == "calCell") myElement.bgColor = "#EDF4F0";
            else myElement.bgColor = "#FFFFFF";
    }

    function fToggleColor(myElement) {
        var aa = myElement.bgcolor;
        if (myElement.id == "calCell") {
            if(!isNaN(parseInt(myElement.children["calDateText"].innerText))) {
               myElement.bgColor = "#F7E0C3";
                objPrevElement.bgColor = aa;
                objPrevElement = myElement;
            }
        }
        var toggleColor ="#965F20";
        if (myElement.id == "calDateText") {
                myElement.color = toggleColor;
                objPrevElement.color = "#FFFFFF";
                objPrevElement = myElement;
        }
        else if (myElement.id == "calCell") {
            for (var i in myElement.children) {
                if (myElement.children[i].id == "calDateText") {
                    if (myElement.children[i].color == toggleColor) {
                        myElement.children[i].color = "";
                    }
                    else {
                        myElement.children[i].color = "#000000";
                    }
                }
            }
        }
    }

    function fSetSelectedDay(myElement){
        var dPrevDate = new Date(iYear, iMonth, 0);
        return dPrevDate.getDate();
    }

    function fGetDaysInMonth(iMonth, iYear) {
        var dPrevDate = new Date(iYear, iMonth, 0);
        return dPrevDate.getDate();
    }

    function fBuildCal(iYear, iMonth, iDayStyle) {
        var aMonth = new Array();
        aMonth[0] = new Array(7);
        aMonth[1] = new Array(7);
        aMonth[2] = new Array(7);
        aMonth[3] = new Array(7);
        aMonth[4] = new Array(7);
        aMonth[5] = new Array(7);
        aMonth[6] = new Array(7);
        var dCalDate = new Date(iYear, iMonth-1, 1);
        var iDayOfFirst = dCalDate.getDay();
        var iDaysInMonth = fGetDaysInMonth(iMonth, iYear);
        var iVarDate = 1;
        var i, d, w;
        if (iDayStyle == 2) {
            aMonth[0][0] = "Sunday";
            aMonth[0][1] = "Monday";
            aMonth[0][2] = "Tuesday";
            aMonth[0][3] = "Wednesday";
            aMonth[0][4] = "Thursday";
            aMonth[0][5] = "Friday";
            aMonth[0][6] = "Saturday";
        }
        else if (iDayStyle == 1) {
            aMonth[0][0] = "Sun";
            aMonth[0][1] = "Mon";
            aMonth[0][2] = "Tue";
            aMonth[0][3] = "Wed";
            aMonth[0][4] = "Thu";
            aMonth[0][5] = "Fri";
            aMonth[0][6] = "Sat";
        }
        else {
            aMonth[0][0] = "Su";
            aMonth[0][1] = "Mo";
            aMonth[0][2] = "Tu";
            aMonth[0][3] = "We";
            aMonth[0][4] = "Th";
            aMonth[0][5] = "Fr";
            aMonth[0][6] = "Sa";
        }
        for (d = iDayOfFirst; d < 7; d++) {
            aMonth[1][d] = iVarDate;
            iVarDate++;
        }
        for (w = 2; w < 7; w++) {
            for (d = 0; d < 7; d++) {
                if (iVarDate <= iDaysInMonth) {
                    aMonth[w][d] = iVarDate;
                    iVarDate++;
                }
            }
        }
        return aMonth;
   }

    function prev() {
		document.month_form.opt.value = 2;
		document.month_form.submit();
    }

    function next() {
        document.month_form.opt.value = 1;
        document.month_form.submit();
    }

    function today() {
        document.month_form.opt.value = 0;
        document.month_form.submit();
    }

    function fDrawCal(iYear, iMonth, iCellWidth, iCellHeight, sDateTextSize, 				sDateTextWeight, iDayStyle) {
		var myMonth;
		myMonth = fBuildCal(iYear, iMonth, iDayStyle);
		document.write("<table border='0' bordercolor=red width=680 cellpadding='0' cellspacing='0' align=center>");

		document.write("<tr bgcolor='#f5d9b7'>");
		document.write("<td colspan=7 border=0 background='./img/cb_pt01.gif' >");
		document.write("<img src='./img/cb_left01.gif' align='left' border='0' vspace='0' hspace='0'>");
		document.write("<img src='./img/cb_right01.gif' align='right' border='0' vspace='0' hspace='0'>");
		document.write("<table border='0' bordercolor=red cellpadding='0' cellspacing='0' align=right>");
        document.write("<td colspan=7 border=0 align='right'><a href='javascript:prev()'><font color=#568076>지난달보기</a> &nbsp;</font><img src='./img/cb_bl03.gif' border=0 vspace='1'></a>");
        document.write("&nbsp;&nbsp;<%=year%>년&nbsp;<%=month%>월&nbsp;&nbsp;");
        document.write("<a href='javascript:next();'><img src='./img/cb_bl04.gif' border=0 VSPACE='1'><font color=#568076>&nbsp; 다음달보기</font></a></td>");
        document.write("<tr>");
        document.write("</table>");
        document.write("</td></tr>");
		document.write("<table width=680 border='0' bordercolor=red CELLSPACING='1' BGCOLOR='#BE9A60'>");
		document.write("<td border=0 width=100 align='center' bgcolor='#f5d9b7'>" + myMonth[0][0] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][1] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][2] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][3] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][4] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][5] + "</td>");
		document.write("<td border=0 width=100 align='center' bgcolor='#fdf5ea'>" + myMonth[0][6] + "</td>");
        var count=1;
        var view=0;
        for(w = 1; w < 2; w++)
        {
            document.write("<tr>")
            for(d = 0; d < 7; d++)
            {
                //클릭 부분만 제외
                if( d == 0 ){
                    if( count == <%=sys_day%> && <%=sys_month%> == <%=month%>) {
                        document.write("<td id=calCell bgcolor='#f7e0c3' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    else {
                        
                        document.write("<td id=calCell bgcolor='#fdf5ea' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    }
                else{
                    if( count == <%=sys_day%> && <%=sys_month%> == <%=month%>) {
                        document.write("<td id=calCelltoday bgcolor='#fdf5ea' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "'onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    else {
                        
                        document.write("<td id=calCell2 bgcolor='#ffffff' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    }
                if (!isNaN(myMonth[w][d]))
                {
                    count++;
                    document.write(myMonth[w][d]);
                    if(0<myMonth[w][d] && myMonth[w][d]<10)
                    {
                        myMonth[w][d] = "0" + myMonth[w][d];
                    }
                  var j=0;
              var aa=<%=REPEAT1%>;
              var a=new Array(aa);
              <%
              	for(int RE=0;RE<vctDate.size();RE++){
				  sModel = (ScheduleModel)vctDate.elementAt(RE);
              %>
                    a[j]="<%=sModel.date.substring(6,8)%>";//일자
                    j++;
                  
            <%
               }
            %>
                  var k=0;
           <%
              	for(int k=0;k<vctDate.size();k++){
				  sModel = (ScheduleModel)vctDate.elementAt(k);
            %>
               
                    if(a[k]==myMonth[w][d]){
                    document.write("<a href=schedule_read.jsp?no=<%=sModel.no%>><br><img src='./img/cb_bl01.gif' border=0 VSPACE='2'>&nbsp;<%=sModel.title%></a><br>");
                }
                      k++;
           <%
             }
            %>
                
                }
                else
                {
                    document.write("<font id=calDateText onMouseOver='fToggleColor(this)' style='CURSOR:Hand;FONT-FAMILY:Arial;FONT-SIZE:" + sDateTextSize + ";FONT-WEIGHT:" + sDateTextWeight + "' onMouseOut='fToggleColor(this)' onclick=fSetSelectedDay(this)> </font>");
                }
                document.write("</td>")
            }
            document.write("</tr>");
        }
        for(w = 2; w < 7; w++)
        {
            if(myMonth[w][0])
            {
                document.write("<tr>")
                for(d = 0; d < 7; d++)
                {
                    //클릭 부분만 제외
                    if( d == 0 )
                    {
                        if( count == <%=sys_day%> && <%=sys_month%> == <%=month%>)
                        {
                            document.write("<td id=calCell bgcolor='#F7E0C3' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                        else
                        {
                            
                            document.write("<td id=calCell bgcolor='#FDF5EA' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    }
                    else
                    {
                        if( count == <%=sys_day%> && <%=sys_month%> == <%=month%>)
                        {
                            document.write("<td id=calCelltoday bgcolor='#FDF5EA' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight +"' onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                        else
                        {
                            
                           document.write("<td id=calCell2 bgcolor='#FFFFFF' align='left' valign='top' width='" + iCellWidth + "' height='" + iCellHeight + "'onMouseOver='fToggleColorOver(this)' onMouseOut='fToggleColorOut(this)'>");
                        }
                    }
                    if (!isNaN(myMonth[w][d]))
                    {
                        count++;
                        document.write(myMonth[w][d]);
                        if(0<myMonth[w][d] && myMonth[w][d]<10)
                        {
                            myMonth[w][d] = "0" + myMonth[w][d];
                        }
                    var j=0;
                	var aa=<%=REPEAT1%>;
                	var a=new Array(aa);
               <%
               	
              		for(int RE=0;RE<vctDate.size();RE++){
						sModel = (ScheduleModel)vctDate.elementAt(RE);              
               %>
                        a[j]="<%=sModel.date.substring(6,8)%>";
                        j++;
               <%
               		}
               %>
                      
                      var k=0;
                 <%
               	
              		for(int RE=0;RE<vctDate.size();RE++){
            			sModel = (ScheduleModel)vctDate.elementAt(RE);
               %>
               	
                    if(a[k]==myMonth[w][d])
                    {
                    	
                        document.write("<a href=schedule_read.jsp?no=<%=sModel.no%>><br><img src='./img/cb_bl01.gif' border=0 VSPACE='2'>&nbsp;<%=sModel.title%></a><br>");
                        }
                        k++;
                <%
                	}
                %>
                    }
                    else
                    {
                        document.write("<font id=calDateText onMouseOver='fToggleColor(this)' style='CURSOR:Hand;FONT-FAMILY:Arial;FONT-SIZE:" + sDateTextSize +";FONT-WEIGHT:" + sDateTextWeight + "' onMouseOut='fToggleColor(this)' onclick=fSetSelectedDay(this)> </font>");
                    }
                    document.write("</td>")
                }
                document.write("</tr>");
            }
        }
        document.write("</table>");
        }

    function fUpdateCal(iYear, iMonth) {
        myMonth = fBuildCal(iYear, iMonth);
        objPrevElement.bgColor = "";
        document.all.calSelectedDate.value = "";
        for (w = 1; w < 7; w++) {
            for (d = 0; d < 7; d++) {
                if (!isNaN(myMonth[w][d])) {
                    calDateText[((7*w)+d)-7].innerText = myMonth[w][d];
                }
                else {
                    calDateText[((7*w)+d)-7].innerText = " ";
                }
            }
        }
    }

  function nodata(){
        alert("데이터가 없습니다.");
        return false;
  }
// End -->
</script>
</head>

<body leftmargin="0" topmargin="0">
<table border="0" bordercolor=red cellspacing="0" cellpadding="0" width="710">
	<tr>
		<form name="month_form" method="post" action="schedule_month.jsp"> 
		<td background="./img/c_top01.gif" border="0" WIDTH=650 height=50 align=RIGHT VALIGN=BOTTOM VSPACE="0" HSPACE="3" style="BACKGROUND-REPEAT: no-repeat;">
    		<input name="opt" type="hidden" value="">
  			<input name="start_year" type="hidden" value="<%=year%>">
  			<input name="start_month" type="hidden" value="<%=month%>">
    </td>
    </form>
    <td vspace="0" hspace="3" valign=bottom>	
    </td>
  </tr>
</table>
<!-- 메인 테이블 -->
<table border="0" bordercolor=red cellpadding="0" cellspacing="0" width="710">
    <tr>
    <td colspan=3>
      <br>
        <table border="0" cellpadding="0" cellspacing="0" width="660" align=right>
      <tr>
        <td><img src="./img/cb_mu03.gif" border="0" usemap="#imagemap1"></td>
        </tr>
        </table>
    </tr>
</table>

<table border="0" bordercolor=red cellpadding="0" cellspacing="0" width="740">
    <tr>
    <td>
        <table width="680" border="0" bordercolor=red cellspacing="0" cellpadding="0" align=center>
                <td align=center border=0>
                    <script language="JavaScript">
                        var dCurDate = new Date();
                        fDrawCal(<%=year%>, <%=month%>, 94, 92, "15px", "bold", 1);
                    </script>
                </td>
            </talbe>
        </td>
    </tr>
    </form>
</table>
<map name="ImageMap1">
<area shape="rect" coords="612, 0, 678, 20" href="schedule_form.jsp">
<!-- 주별보기 -->
<SCRIPT LANGUAGE="javascript">
<!--
    var now = new Date ();
    monthNow = now.getMonth () + 1;
  yearNow = now.getYear ();
  dateNow = now.getDate();
  yearNow = ( yearNow < 100 ) ? yearNow + 1900 : yearNow;
  fBuildCal(yearNow, monthNow);
  var dCurMonth = now.getMonth()+1;
    var dCurDayOfMonth = now.getDate();
    if(monthNow <10 ) monthNow = "0" + monthNow;
    if(dateNow <10 ) dateNow = "0" + dateNow ;
    var dCurYear = now.getFullYear();
    var objPrevElement = new Object();
    var fw;
    var ii;
    function fGetDaysInMonth(iMonth, iYear)
    {
        var dPrevDate = new Date(iYear, iMonth, 0);
        return dPrevDate.getDate();
    }
    function fBuildCal(iYear, iMonth)
    {
        var aMonth = new Array();
        aMonth[0] = new Array(7);
        aMonth[1] = new Array(7);
        aMonth[2] = new Array(7);
        aMonth[3] = new Array(7);
        aMonth[4] = new Array(7);
        aMonth[5] = new Array(7);
        aMonth[6] = new Array(7);
        var dCalDate = new Date(iYear, iMonth-1, 1);
        var iDayOfFirst = dCalDate.getDay();
        var iDaysInMonth = fGetDaysInMonth(iMonth, iYear);
        var iVarDate = 1;
        var i, d, w;
        //첫날 공백 후부터
        for (d = iDayOfFirst; d < 7; d++)
        {
            aMonth[1][d] = iVarDate;
            iVarDate++;
        }
        //첫째 주부터 일요일부터
        ii=1;
        for (w = 2; w < 7; w++) {
            for (d = 0; d < 7; d++) {
                if (iVarDate <= iDaysInMonth) {
                    if(d==0) {
                        if(iVarDate < now.getDate()) {
                            fw = iVarDate;
                            //fw = ii;
                            ii++;
                            }
                    }
                    aMonth[w][d] = iVarDate;
                    iVarDate++;
                }
            }
        }
        if(fw == null)
        {
                monthNow--;
                if(monthNow < 1)
                {
                    monthNow = 12;
                    yearNow--;
                }
                m = monthNow;
                if(m==12 || m==10 || m==8 || m==7 || m==5 || m==3 || m==1) fw=31;
                else if(m==11 || m==9 || m==6 || m==4) fw=30;
                else fw=28;
       }
    }
	//월별보기, 주별보기
    document.write("<area shape='rect' coords='72, 3, 146, 22' href='trans?data=events/event_week&start_year="+yearNow+"&start_month="+monthNow+"&start_day="+fw+"'>");
//-->
</SCRIPT>
</map>
</FORM>
</body>
</html>

