<%@ page contentType="text/html;charset=KSC5601"
         import="java.util.*,sample.util.*,sample.quest.*" %>
<jsp:useBean id="MyBean" class="sample.quest.qQuery" scope="page" />
<!--
<jsp:include page="/LiscoShop/menu.jsp" />
-->
<%
	MyBean.makeConn();
	//가장최근의 설문지 번호를 받아 설문내용을 출력
	int id = Integer.parseInt(request.getParameter("idx"));

	Vector vctBoard = MyBean.selectBoard(id);
	qModel sModel = new qModel();

	sModel=(qModel)vctBoard.elementAt(0);
	if (sModel.totreply != 0 ) {  // 투표자가 존재하면 처리
	   int r1=(int)(sModel.replynum1*100/sModel.totreply);
	   int r2=(int)(sModel.replynum2*100/sModel.totreply);
	   int r3=(int)(sModel.replynum3*100/sModel.totreply);
	   int r4=(int)(sModel.replynum4*100/sModel.totreply);

	   int tr1 = (sModel.replynum1 *390) / sModel.totreply;
	   int tr2 = (sModel.replynum2 *390) / sModel.totreply;
	   int tr3 = (sModel.replynum3 *390) / sModel.totreply;
	   int tr4 = (sModel.replynum4 *390) / sModel.totreply;

%>

<html>
<head><title>투표 결과</title>
<style type="text/css">
  td {line-height:1em}
</style>
</head>
<body leftmargin="0" topmargin="0">
<table border="0" width="800">
  <tr>
    <td width="20%" height="500" bgcolor="#ecf1ef" valign="top">

	<!-- <jsp:include page="/LiscoShop/Member/login.jsp" /> -->

	</td>
    <td width="80%" valign="top">
    &nbsp;<br>
    <table border="0" width="90%" align="center">
      <tr>
        <td colspan="2"><img src="img/bullet-01.gif"> 
        <font color="blue" size="3">라이브 폴</font><font size="2"> - 결과 보기</font></td>
      </tr>
    </table>
    <p>
    <b><font size="3" color="#0000FF">
    <img src="img/bullet-03.gif" width="15" height="15">&nbsp;<%=sModel.question%></font></b><br>
    <table border="0" width="640" bgcolor="white"> 
      <tr> 
        <td colspan="2" ><font size="2">&nbsp;&nbsp;&nbsp;(총투표자수:<%= sModel.totreply%>명)</font></td></tr>
      <tr>
        <td colspan="2" height="5"></td></tr><!-- 공간 띄우기용 빈 줄입니다. -->
      <tr>
        <td width="250">&nbsp;<font size="2"><img src="img/bullet-06.gif"> 
        <%=sModel.reply1%> - <%=sModel.replynum1%>명(<%=r1%>%)</font></td>
         <td width="390">
         <hr width="<%=tr1%>" align="left" size="7" color="orange"></td>
          <td width="390">&nbsp;</td>
      </tr>
      <tr>
        <td width="250">&nbsp;<font size="2"><img src="img/bullet-06.gif"> 
        <%=sModel.reply2%> - <%=sModel.replynum2%>명(<%=r2%>%)</font></td>
         <td width="390">
         <hr width="<%=tr2%>" align="left" size="7" color="orange"></td>
          <td width="390">&nbsp;</td>
      </tr>
      <tr>
        <td width="250">&nbsp;<font size="2"><img src="img/bullet-06.gif"> 
        <%=sModel.reply3%> - <%=sModel.replynum3%>명(<%=r3%>%)</font></td>
         <td width="390">
         <hr width="<%=tr3%>" align="left" size="7" color="orange"></td>
          <td width="390">&nbsp;</td>
      </tr>
      <tr>
        <td width="250">&nbsp;<font size="2"><img src="img/bullet-06.gif"> 
        <%=sModel.reply4%> - <%=sModel.replynum4%>명(<%=r4%>%)</font></td>
         <td width="390">
         <hr width="<%=tr4%>" align="left" size="7" color="orange"></td>
          <td width="390">&nbsp;</td>
      </tr>
	 </table>
 </td>
  </tr> 
</table>   
</body>          
</html>

<%
	MyBean.takeDown();
	} else { //투표자가 없을 경우 처리
%>
      <script language="javascript">
        alert("투표결과가 없습니다.\n\n 다음에 이용하세요");
        location.replace(document.referrer);
      </script>

<% 
	}
%>