<%@ page contentType="text/html;charset=EUC-KR" import ="java.util.*,sample.util.*,sample.calendar.*, sample.member.*"%>
<jsp:useBean id="MyBean" class="sample.calendar.calendarQuery" scope="page" />
<jsp:useBean id="MyMember" class="sample.member.MemberQuery" scope="page" />

<script language="javascript" src="script/calendar.js">
</script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<%	
	DateTime date = new DateTime();
	String year="",month="",ymd="",query="",db="calendar";	//ymd는 날짜 기억 년월일을 말하며 ex)2004-07-01형식
//	db = request.getParameter("db");
	if(request.getParameter("year")!=null)
		year = request.getParameter("year");
	if(request.getParameter("month")!=null)
		month = request.getParameter("month");
	if(!(year.equals("")||month.equals(""))){
		ymd = year+"-"+month+"-01";															
		date.setDateTime(ymd);					//선택한 년도 월 세팅
	}else{
		ymd = date.getDate();
		ymd = ymd.substring(0,8)+"01";	
		date.setDateTime(ymd);					// 선택하지 않았을경우 현재 년도의 월	
 }
int thisYear = date.getYear();
int thisMonth = date.getMonth();
int thisDay = date.getDay();
int thisWeek = date.getWeek();
int lastDate = date.getLastday();
query = "where date between '"+ymd+"' and '"+ymd.substring(0,8)+lastDate+"' order by date asc";
MyBean.makeConn();
Vector vctC = MyBean.list(db,query);
calendarModel calendarV = new calendarModel();	
int size = vctC.size();
int cnt=0, check=0,rate=0;
MyBean.takeDown();

// 회원인증을 통한 관리자 모드일경우 관리자입 입력가능하게 할 경우 사용
//	if(session.getAttribute("user_id") != null){	//등급이 1이면 관리자이고 입력폼 링크 걸어줌
//		MyMember.makeConn();
//		if(MyMember.rate("member",session.getAttribute("user_id").toString())==1) 
				rate=1;
//		MyMember.takeDown();
//		}
%>
<br><br>
<table width="770" border="1" cellpadding="0" cellspacing="0" align="center" class="grayfont" style="padding:5;">
<form method="post" name="calendarfrm" action="calendar.jsp">
<tr>
	<td align="center" colspan="2" height="50"><font size="2">운영달력</font>
	</td>
</tr>
<tr>
	<td>
<%
			if(ymd.equals("2004-01-01")){
%>
				◀
<%
			}else{
%>
					<a href="calendar.jsp?year=<%=date.getMovemonth(-1).substring(0,4)%>&month=<%=date.getMovemonth(-1).substring(5,7)%>" onfocus="this.blur()" class="linkover">◀</a>
<%
			}
%>	&nbsp;<%=thisYear%>년 <%=thisMonth%>월&nbsp;
<%
			if(ymd.equals("2010-01-01")){%>▶
<%	
			}else{
%>
			<a href="calendar.jsp?year=<%=date.getMovemonth(1).substring(0,4)%>&month=<%=date.getMovemonth(1).substring(5,7)%>" onfocus="this.blur()" class="linkover">▶</a>
<%
				}
%>
	</td>
	<td align="right"><select name="year" class="input" onChange="calendarfrm.submit()">
<%	
			for(int i=2004;i<=2010;i++){
%>
				<option value="<%=i%>"<%if(thisYear==i){%>selected<%}%>><%=i%></option>
<%
			}
%></select>&nbsp;
		<select name="month" class="input" onChange="calendarfrm.submit()">
<%	
		String val="";
		for(int i=1;i<=12;i++){			
			if(i<10) val = "0"+Integer.toString(i);
			else val= Integer.toString(i);
%>
		<option value="<%=val%>" <%if(thisMonth==i){%>selected<%}%>><%=i%></option>
<%
	}
%>
		</select>
		</td>
</tr>
</form>
</table>
<table width="770" border="1" cellpadding="0" cellspacing="0" align="center" class="grayfont">
	<tr align="center" height="30">
		<td width="110"><font color="#FF0066">일</font></td>
		<td width="110">월</td>
		<td width="110">화</td>
		<td width="110">수</td>
		<td width="110">목</td>
		<td width="110">금</td>
		<td width="110"><font color="0099CC">토</td>
	</tr>
	<tr height="60">
<%
		for(int i=1;i<= lastDate;i++,thisWeek++){				
			if(i==1){
				for(int j=1;j<thisWeek;j++){
%>
				<td>&nbsp;</td>
<%
				}
			}
%>
		<td align="center" valign="top" height="80"> 
			<table width="106"  border="0" cellpadding="0" cellspacing="0" align="center" class="grayfont" style="padding:1;">
				<tr>
					<td align="left" valign="top" height="20"><font size="3">
<%
						if(rate==1){																									// 1이랑 같으면 관리자란뜻 달력 입력폼 링크
							ymd = ymd.substring(0,8);
							if(i<10)
								ymd = ymd.substring(0,8)+"0";	 	
	%>
								<a href="javascript:calendarFrm('<%=db%>','<%=ymd+i%>')" onfocus="this.blur()" class="linkover">
<%
						}
						if(thisWeek==1){																					// 요일를 체크하여 색을 표시 1이면 일요일 7이면 토요일		
%>
							<font color="FF0066">
<%
						}else if(thisWeek==7){
%>
							<font color="0099CC">
<%
						}else{
	%>
							<font color="#000000">
<%
						}
	%>
						<%=i%></font><%if(rate==1){%></a><%}%>
					</td>
				</tr>

				<tr>
					<td>
<%
						for(check=0;size!=0;size--){
							calendarV = (calendarModel)vctC.elementAt(cnt);		
							if(Integer.parseInt(calendarV.date.substring(8,10))==i){
								if(check==1){
%>
						<br>
<%
								}
%>
								·
								<a href="javascript:calendarView('<%=db%>',<%=calendarV.num%>)"onfocus="this.blur()" class ="linkover"><%=calendarV.subject%></a>
<%		
								cnt++;
								check=1;
							}else 
								break;
							}
							if(size==0 || check==0){
%>
	&nbsp;
<%
							}
%>
					</td>
				</tr>
			</table>
		</td>
<%
			if(i==lastDate){
				for(int j=0;j<7-thisWeek;j++){
	%>
		<td>&nbsp;</td>
<% 
			}
%>
	</tr>
<%
		}else if(thisWeek==7){
				thisWeek=0;
%>
	</tr>
	<tr height="60">
<%
		}
}
%>
</table>
<br><br><br>
