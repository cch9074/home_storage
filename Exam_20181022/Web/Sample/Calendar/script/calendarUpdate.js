function calendarCheck()
{
	if(!calendarfrm.subject.value)
	{
		alert("제목을 입력하세요.");
		calendarfrm.subject.select();
		return (false);
	}
	if(!calendarfrm.contents.value)
	{
		alert("내용을 입력하세요.");
		calendarfrm.contents.select();
		return (false);
	}
	return (true);
}