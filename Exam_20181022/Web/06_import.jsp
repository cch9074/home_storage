<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.text.*" %>
    <%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP</title>
</head>
<body>
<%Calendar date = Calendar.getInstance();
SimpleDateFormat today = new SimpleDateFormat("yyy년MM월dd일");
SimpleDateFormat now = new SimpleDateFormat("hh시mm분ss초");
%>
오늘은 <b> <%= today.format(date.getTime()) %></b>입니다.<br>
지금 시각은 <b><%=now.format(date.getTime()) %></b> 입니다.

</body>
</html>