package JavaFx_Layout.AnchroPane;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppMain_02 extends Application {
	public void start(Stage stage) throws Exception{
		Parent parent = FXMLLoader.load(getClass().getResource("root.fxml"));
		Scene scene = new Scene(parent);
		
		stage.setScene(scene);
		//크기를 조정할수 없게 고정하는 방법.
		stage.setResizable(false);
		stage.show();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
