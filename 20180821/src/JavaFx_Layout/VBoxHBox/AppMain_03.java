package JavaFx_Layout.VBoxHBox;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppMain_03 extends Application{
	public void start(Stage stage) throws Exception{
		Parent parent = FXMLLoader.load(getClass().getResource("root.fxml"));
		Scene scene = new Scene(parent);
		
		stage.setResizable(false);
		stage.setScene(scene);
		stage.show();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch();
		
	}

}
