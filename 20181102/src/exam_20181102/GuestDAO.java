package exam_20181102;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;



public class GuestDAO {
	
	private static  GuestDAO instance = new GuestDAO();
	
	public static GuestDAO getInstance() {
		return instance;
	}
	
	
	public Connection getConnection() throws Exception{
		Connection conn = null;
		Context init = new InitialContext();
		Context evn = (Context) init.lookup("java:/comp/evn");
		DataSource ds = (DataSource)evn.lookup("jdbc/myoracle");
		conn =  ds.getConnection();
		return conn;
				
	}
	public int guestInsert(GuestVO gVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "insert into hr_guest(no,name,subject,contents,regdate,readcnt)"
		        + "values(hr_guest_seq.nextval,?,?,?,?,?)";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, gVO.getName());
			pstmt.setString(2, gVO.getSubject());
			pstmt.setString(3, gVO.getContents());
			pstmt.setTimestamp(4, gVO.getRegdate());
			pstmt.setInt(5, 0);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
		return row;
	}
	
	public List guestList() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<GuestVO> list = new ArrayList();
		
		
		String query = "select * from hr guest order by no desc";
		
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				GuestVO guest = null;
				while( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));
					list.add(guest);
				}		
					
				
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return list;
			}
			
		}
	public GuestVO guestListNo(int no) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<GuestVO> list = new ArrayList();		
		String query = "select * from hr_guest where no= ?";
		GuestVO guest = null;
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setInt(1, no);
				rs = pstmt.executeQuery();
				
				if( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setContents(rs.getString("contents"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));	
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return guest;
			}
			
		}
	
	public void guestCountNo(int no) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<GuestVO> list = new ArrayList();		
		String query = "update hr_guest set readcnt = readcnt +1 where no= ?";
		GuestVO guest = null;
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setInt(1, no);
				pstmt.executeUpdate();
				

			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				
			}
			
		}
	public int guestEdit(GuestVO guest) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "update hr_guest set subject=?,contents=? where no=?";
		int row =0;
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guest.getSubject());
			pstmt.setString(2, guest.getContents());
			pstmt.setInt(3, guest.getNo());
			
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		return row;
		}
		
	}
	public int guestDeleteNo(int no,String pass) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "delete from hr_guest where no=? and psss=?";
		int row =0;
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, no);
			
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		return row;
		}
		
	}
	
	
}