package exam_20181102;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/write.do")
public class GuestWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public GuestWrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd = request.getRequestDispatcher("Guest/guest_write.jsp");
		rd.forward(request, response);
		//response.sendRedirect("./Guest/guest_write.jsp");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		GuestVO gVO = new GuestVO();
	
		gVO.setName(name);
		gVO.setSubject(subject);
		gVO.setContents(contents);
		gVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		GuestDAO gDAO = GuestDAO.getInstance();
		gDAO.guestInsert(gVO);
		response.sendRedirect("list.do");
		doGet(request, response);
		
	}

}
