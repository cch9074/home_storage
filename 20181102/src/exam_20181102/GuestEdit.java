package exam_20181102;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/edit.do")
public class GuestEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GuestEdit() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int no = Integer.parseInt(request.getParameter("no"));
		GuestDAO gDAO = GuestDAO.getInstance();
		GuestVO guest = gDAO.guestListNo(no);
		request.setAttribute("guest", guest);
		RequestDispatcher ds = request.getRequestDispatcher("Guest/guest_modify.jsp");
		ds.forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int no = Integer.parseInt(request.getParameter("no"));
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		
		GuestVO guest = new GuestVO();
		guest.setNo(no);
		//gVO.setName(name);
		guest.setSubject(subject);
		guest.setContents(contents);
	
		
		GuestDAO gDAO = GuestDAO.getInstance();
		int row = gDAO.guestEdit(guest);
		
		response.sendRedirect("view.do?no="+guest.getNo());
		//response.sendRedirect("list.do");
		doGet(request, response);

	}

}
