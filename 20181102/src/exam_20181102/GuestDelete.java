package exam_20181102;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/delete.do")
public class GuestDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public GuestDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String no = request.getParameter("no");
	//	request.setAttribute("no", no);
		RequestDispatcher rd = request.getRequestDispatcher("Guest/guest_delete.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int no = Integer.parseInt(request.getParameter("no"));
		String pass= request.getParameter("pass");
		GuestDAO gDAO = GuestDAO.getInstance();
		int row = gDAO.guestDeleteNo(no,pass);
		if(row == 1) {
			request.setAttribute("sw", 1);
		}else {
			request.setAttribute("sw", 0);
		}
		RequestDispatcher rd = request.getRequestDispatcher("Guest/guest_delete_pro.jsp");
		rd.forward(request, response);
	}

}
