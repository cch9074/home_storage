package map;
import java.util.*;
import java.util.Map.Entry;
public class HashMapEx3 {
	static Map map = new HashMap();
	
	static void addGroup(String groupName) {
		if(!map.containsKey(groupName)) {
			map.put(groupName, new HashMap());
					//key 			value==>폰넘버를 받을때 사용
		}
	}
	static void addPhoneNo(String name, String tel) {
		addPhoneNo("기타",name,tel);
	}
	static void addPhoneNo(String groupName, String name, String tel) {
		addGroup(groupName);
		HashMap group = (HashMap)map.get(groupName);
							//map 의 key(그룹이름)에 이름과 번호를 넣음
		group.put(tel, name);
				//번호를 key 값으로 하여 이름이 같더라도 번호로 구분할수 있게해줌
	}
	static void printList() {
		Set set = map.entrySet();
		//entry를 통해 key와 value 를 얻을수 있게함.
		Iterator iter = set.iterator();
		while(iter.hasNext()) {
			Map.Entry e = (Map.Entry)iter.next();
			//e 엔트리에 map의 key와 value 값을 받아옴
			Set subset = ((Map)e.getValue()).entrySet();
			//map의 value 가 map타입이므로 entryset를 통해 key와 value를 subset에 받아옴
			Iterator subIt = subset.iterator();
			//set로 받은 것을 다시 출력하기위해 순서자에 저장
				
			System.out.println(" * " + e.getKey()+"["+subset.size()+"]");
										//그룹이름			그룹내 사람수
			while(subIt.hasNext()) {
				//subset에서 받아온 key와 value 를 출력하기
				Map.Entry subE = (Map.Entry)subIt.next();
				String name = (String)subE.getKey();
				String telNo = (String)subE.getValue();
				System.out.println(telNo + " "+name );
				
			}
			System.out.println();
		}
		
	}
	public static void main(String[] args) {
		
		addPhoneNo("친구","이자바","010-111-1111");
		addPhoneNo("친구","김자바","010-222-2222");
		addPhoneNo("친구","김자바","010-333-3333");
		addPhoneNo("회사","김대리","010-444-4444");
		addPhoneNo("회사","김대리","010-555-5555");
		addPhoneNo("회사","박대리","010-666-6666");
		addPhoneNo("회사","이과장","010-777-7777");
		addPhoneNo("세탁","010-888-8888");
		
		printList();

	}

}
