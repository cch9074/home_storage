package map;
import java.util.*;
public class HashMapEx1 {

	public static void main(String[] args) {
		Map map = new Hashtable();

		map.put("myId", "1234");
		map.put("asdf", "1111");
		map.put("asdf", "1234");
		
		Scanner scan = new Scanner(System.in);
		
		while(true) {
			System.out.println("id 와 password를 입력해주세요");
			
			System.out.print("id :");
			String id = scan.next();
			System.out.print("password");
			String password = scan.next();
			
			if(!map.containsKey(id)) {
				System.out.println("없는 아이디입니다.");
				continue;
			}else {
				if(map.get(id).equals(password)) {
					System.out.println("로그인 성공");
					break;
				}else {
					System.out.println("비밀번호가 바르지 않습니다.");
				}
			}
		}
	}

}
