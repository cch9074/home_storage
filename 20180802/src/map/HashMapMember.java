package map;
import java.util.*;
class Member{
	private int sNumber;
	private String name;
	
	public Member(){}
	public Member(int sNumber, String name) {
		this.sNumber = sNumber;
		this.name = name;
	}	
	
	public int getsNumber() {
		return sNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setsNumber(int sNumber) {
		this.sNumber = sNumber;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member member = (Member)obj;
			return (sNumber==this.sNumber && name==this.name);
		}else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		return sNumber+( name).hashCode();
	}
}
public class HashMapMember {

	public static void main(String[] args) {
		//key �� value �� Ÿ���� �����ִ� ���� ����.
		Map<Member,Integer> map = new HashMap<Member,Integer>();
		Member m1 = new Member(35,"��ö��");
		Member m2 = new Member(35,"��ö��");
		Member m3 = new Member(30,"�迵��");

		map.put(m1,1000);
		map.put(m2,2000);
		map.put(m3,3000);
		System.out.println(map.size());
		System.out.println(map);
		
		Set set = map.keySet();
		Iterator iter = set.iterator();
		while(iter.hasNext()) {
			Member m = (Member)iter.next();
			System.out.println(m.getsNumber() +", "+ m.getName() +" : "+map.get(m));
			System.out.println();
		}
	}

}
