package map;
import java.util.*;
public class HashMapExam_02 {
//keySet를 이용해 key를 가져오기
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map map = new HashMap();
		map.put("1","AAAA");
		map.put("2","BBBB");
		map.put("3","CCCC");
		map.put("4","DDDD");
		map.put("5","FFFF");
		
		System.out.println(map.size());
		System.out.println("------");
		//key값'만'set에 넣는다.
		Set set = map.keySet();
		//key값을 모를때 이용
		//key를 통해 값을 얻을수 있다.
		Iterator iter = set.iterator();
		while(iter.hasNext()) {
			Object obj = iter.next();
			//System.out.println(obj);
			Object o = map.get(obj);
			System.out.println(obj + " : "+o);
		}
	}

}
