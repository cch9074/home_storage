package map;
import java.util.*;
import java.util.Map.Entry;
public class HashMapExam_03 {
//entry를 이용해 key 와 value 를 함께 꺼내기.
	public static void main(String[] args) {
		
		Map<String, String> map = new HashMap<String, String>();
		//타입을 지정해줌
		map.put("1","AAAA");
		map.put("2","BBBB");
		map.put("3","CCCC");
		map.put("4","DDDD");
		map.put("5","FFFF");
		//키와 값으로 map안의 값을 얻어낸다
		Set set = map.entrySet();
		Iterator iter = set.iterator();
		//entry를 통해 키와 값을 얻어낼 수 있다.
		
		while(iter.hasNext()) {
			//System.out.println(iter.next());
			
			//entry값을 정해준경우 사상할 때캐스팅해준다.
			Map.Entry<String, String> entry = (Map.Entry<String, String>)iter.next();
			System.out.println(entry.getKey()+":"+entry.getValue());
			
		}
		iter = set.iterator();
		while(iter.hasNext()) {
			//entry에서 캐스팅하지 않을 경우 값을 뽑아올 때 캐스팅해준다.
			Map.Entry entry2 = (Map.Entry)iter.next(); 
			String key = (String)entry2.getKey();
			String value = (String)entry2.getValue();
			
			System.out.println(key + ":"+ value);
		}
	}

}
