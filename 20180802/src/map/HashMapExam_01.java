package map;
import java.util.*;
public class HashMapExam_01 {
	public static void main(String[] args) {
		//생성자는 비슷하다.
		//단 생성하면서 키,값의 타입을 정해주는것이 좋다.
		Map<String, Integer> map = new HashMap<String, Integer>();
			//키(구분자) , 값
		map.put("AAA", 90);
		map.put("BBB", 88);
		map.put("CCC", 95);
		map.put("AAA", 66);
		//같은 키를 가진 값은 중복입력되지 않는다.
		//중복될 시 나중에 들어온값으롤 덮어쓴다.
		System.out.println(map);
		int s = map.get("AAA");
		System.out.println("AAA : " + s);
		//데이터 존재여부
		if(map.isEmpty()) {
			System.out.println("비어있음");
		}else {
			System.out.println(map.size());
		}
		//특정 데이터 존재여부
		if(map.containsKey("DDD")) {
			int jum = map.get("BBB");
			System.out.println("BBB : "+ jum);
		}else {
			System.out.println("존재하지 않음");
		}
	}

}
