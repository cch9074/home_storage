package map;
import java.util.*;
public class HashTableExam_01 {
//동기화가 필요할 경우에 HashTable을 이용한다.
	public static void main(String[] args) {
		Map<String, Integer> map = new Hashtable<String, Integer>();
		map.put("AAA", 100);
		map.put("BBB", 200);
		map.put("CCC", 150);
		map.put("DDD", 250);
		map.put("EEE", 350);
		
		Set set = map.keySet();
		Iterator iter = set.iterator();
		int sum = 0;
		while(iter.hasNext()) {
			Object obj = iter.next();
			int var = (Integer)map.get(obj);
			System.out.println(obj+":"+var);
			sum+=var;
		}
		System.out.println("점수 합계 : "+sum);
	}
}
