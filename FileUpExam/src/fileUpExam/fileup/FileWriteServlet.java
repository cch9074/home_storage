package fileUpExam.fileup;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;


@WebServlet("/write.do")
public class FileWriteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public FileWriteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		ServletContext context = getServletContext();
		//파일 저장 경로 설정
		String path = context.getRealPath("upload");
		//한글이 들어올 경우에
		String encType = "utf-8";
		//업로드 하는 파일의 최대 크기(2MB)
		int uploadFileSizeLimit = 2*1024*1024;
		
		MultipartRequest multi = new MultipartRequest(request, path,uploadFileSizeLimit,encType, new DefaultFileRenamePolicy());
																										//중복된 이름을 바꿔서 등록하는 역할
			String name = multi.getParameter("name");
			String title = multi.getParameter("title");
			//한개만 받을 경우
			String filename = multi.getFilesystemName("filename");
			
			request.setAttribute("name", name);
			request.setAttribute("title", title);
			request.setAttribute("filename", filename);
			
			RequestDispatcher rd = request.getRequestDispatcher("upload_pro.jsp");
			rd.forward(request, response);
			
	}

}
