package fileUpExam.fileup;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

/**
 * Servlet implementation class FileWriteServlet2
 */
@WebServlet("/upload2.do")
public class FileWriteServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public FileWriteServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		
		ServletContext context = getServletContext();
		//파일 저장 경로 설정
		String path = context.getRealPath("upload");
		//한글이 들어올 경우에
		String encType = "utf-8";
		//업로드 하는 파일의 최대 크기(2MB)
		int uploadFileSizeLimit = 2*1024*1024;
		
		MultipartRequest multi = new MultipartRequest(request, path,uploadFileSizeLimit,encType, new DefaultFileRenamePolicy());
		
		Enumeration files = multi.getFileNames();
		while(files.hasMoreElements()) {
			String file = (String)files.nextElement();
			String file_name = multi.getFilesystemName(file);
			String ori_file_name = multi.getOriginalFileName(file);

			out.print("<br> 업로드된 파일명 : "+file_name);
			out.print("<br> 원본 파일명 : "+ori_file_name);
			out.print("<hr>");
		
			
		}
	
	}

}
