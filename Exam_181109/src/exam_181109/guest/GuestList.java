package exam_181109.guest;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GuestList
 */
@WebServlet("/list.do")
public class GuestList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public GuestList() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GuestDAO gDAO = GuestDAO.getInstance();
		String url="list.do";
		String s_query="",addtag="",key="";
		int nowpage = 1;//시작 페이지
		int maxlist=10; //페이지당 라인수 
		int totpage=1;
		int totcount = gDAO.guestCount();
		if(totcount%maxlist==0) {
			totpage = totcount/maxlist;
		}else{
			totpage = totcount/maxlist + 1;
		}
		if(totpage ==0) {			//게시글 총 수가 10개(maxlist)가 안될경우
			totpage = 1;
		}
		if(request.getParameter("page") !=null && !request.getParameter("page").equals("")){
			nowpage = Integer.parseInt(request.getParameter("page"));
			//url="list.do?page="+nowpage;
		}
		if(nowpage>totpage) {
			nowpage = totpage;	
		}
		//총 리스트에서 현재페이지 시작번호 구하기
		int pageStart = nowpage * maxlist + 1;
		int listcount = totcount - ((nowpage-1)*maxlist) +1 ;
		
		//List<GuestVO> list = gDAO.guestList();
		List<GuestVO> list = gDAO.guestList(listcount,pageStart);
		request.setAttribute("addtag", addtag);
		request.setAttribute("url", url);
		request.setAttribute("nowpage", nowpage);
		request.setAttribute("totpage", totpage);
		request.setAttribute("totcount", totcount);
		request.setAttribute("pageStart", pageStart);
		request.setAttribute("listcount", listcount);
		request.setAttribute("list", list);
		RequestDispatcher ds = request.getRequestDispatcher("Guest/guest_list.jsp");
		ds.forward(request, response);
		//response.sendRedirect("Guest/guest_list.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
