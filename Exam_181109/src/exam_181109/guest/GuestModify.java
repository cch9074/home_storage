package exam_181109.guest;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GuestModify
 */
@WebServlet("/edit.do")
public class GuestModify extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuestModify() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int no = Integer.parseInt(request.getParameter("no"));
		GuestDAO gDAO = GuestDAO.getInstance();
		GuestVO guest = gDAO.guestListNo(no);
		request.setAttribute("guest", guest);
		
		RequestDispatcher rd = request.getRequestDispatcher("Guest/guest_write.jsp");
		rd.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String no = request.getParameter("no");
		String name = request.getParameter("name");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		
		GuestVO gVO = new GuestVO();
		gVO.setPass(no);
		gVO.setName(name);
		gVO.setSubject(subject);
		gVO.setContents(contents);
	
		gVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		GuestDAO gDAO = GuestDAO.getInstance();
		gDAO.guestInsert(gVO);
		RequestDispatcher rd = request.getRequestDispatcher("list.do");
		rd.forward(request, response);
	}

}
