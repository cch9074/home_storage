package exam_181109.guest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;





public class GuestDAO {
	
	private static  GuestDAO instance = new GuestDAO();
	
	public static GuestDAO getInstance() {
		return instance;
	}
	
	
	public Connection getConnection() throws Exception{
		Connection conn = null;
		try {
		Context init = new InitialContext();
		Context evn = (Context) init.lookup("java:/comp/env");
		DataSource ds = (DataSource)evn.lookup("jdbc/myoracle");
		conn =  ds.getConnection();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
				
	}
	public void guestInsert(GuestVO gVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "insert into hr_guest(no,name,subject,contents,regdate,readcnt)"
		        + "values(hr_guest_seq.nextval,?,?,?,?,?)";
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, gVO.getName());	
			pstmt.setString(2, gVO.getSubject());
			pstmt.setString(3, gVO.getContents());
			pstmt.setTimestamp(4, gVO.getRegdate());
			pstmt.setInt(5, 0);
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
	}
	public void guestModify(GuestVO gVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String quary = "update hr_guest set subject =? ,contents =? where no = ?";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, gVO.getSubject());
			pstmt.setString(2, gVO.getContents());
			pstmt.setInt(3, gVO.getNo());
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
	}
	
	public List guestList() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<GuestVO> list = new ArrayList();
		
		String query = "select * from hr_guest order by no desc";
		
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				GuestVO guest = null;
				while( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));
					list.add(guest);
				}		
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return list;
			}
			
		}
	
	public List<GuestVO> guestList(int listcount,int pageStart) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<GuestVO> list = new ArrayList();
		
		String query = "select x.* from (select * from hr_guest where rownum < ?"
				+"order by no desc) x , (select no from hr_guest where rownum < ? "
				+"order by no asc) y  where x.no = y.no order by x.no desc";
		
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setInt(1, pageStart+1);
				pstmt.setInt(2, listcount);
				
				
				rs = pstmt.executeQuery();
				GuestVO guest = null;
				while( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));
					list.add(guest);
				}		
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return list;
			}
			
		}
	
	public int guestCount() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = "select count(*) from hr_guest";
		int row = 0;
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				if(rs.next()) {
					row = rs.getInt(1);
					
				}		
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return row;
			}
			
		}
	public void guestCountNo(int no) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "update hr_guest set readcnt = readcnt +1 where no= ?";

		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, no);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			
		}
	
	}
	
	
	public GuestVO guestListNo(int no) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<GuestVO> list = new ArrayList();		
		String query = "select * from hr_guest where no= ?";
		GuestVO guest = null;
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setInt(1, no);
				rs = pstmt.executeQuery();
				
				if( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setContents(rs.getString("contents"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));	
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return guest;
			}
			
		}
}