<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${row==0 }">
	<script>
		alert("등록 실패 \n 잠시 후 다시 시도해주세요")
	</script>	
</c:if>
<html>
<head>
<title>회원등록</title>
<STYLE TYPE="text/css">
<!--
body { font-family: 돋움, Verdana; font-size: 9pt}
td   { font-family: 돋움, Verdana; font-size: 9pt; text-decoration: none; color: #000000; BACKGROUND-POSITION: left top; BACKGROUND-REPEAT: no-repeat;}
-->
.formbox {
	BACKGROUND-COLOR: #F0F0F0; FONT-FAMILY: "Verdana", "Arial", "Helvetica", "돋움"; FONT-SIZE:9pt
} 
--->
</STYLE>


<script type="text/javascript">
function check(){
	//이름체크
	var str = frm.name.value.length;
	//if(frm.name.value==""){
	if(str==0 || str < 3){
		alert("이름을 입력해주세요.")
		frm.name.focus()
		return false
	}
	//아이디 체크
	if(frm.userid.value==""){
		alert("아이디를 입력해주세요.")
		frm.userid.focus()
		return false
		
	}else if(!(a_or_d(frm.userid.value))){
		alert("아이디는 숫자와 영문자만 사용가능합니다.")
		frm.userid.focus()
		return false
	}
	//비밀번호 , 비밀번호 확인
	
	if(frm.passwd.value==""){
		alert("비밀번호를 입력해주세요")
		frm.passwd.focus()
		return false
	}
	if(frm.repasswd.value==""){
		alert("비밀번호확인을 입력해주세요")
		frm.repasswd.focus()
		return false
	}
	if(!(frm.repasswd.value==frm.passwd.value)){
		alert("비밀번호가 일치하지 않습니다.")
		frm.repasswd.focus()
		return false
	}
	
	//전화번호
	switch(check_number(frm.tel.value)){
		case 0 :{
			alert("전화번호는 숫자만!")
			frm.tel.focus();
			return false;
			break;
		}
		case 2 :{ 
			alert("전화번호는 11자 혹은 '-'를 포함한 13자를 입력해주세요.")
			frm.tel.focus();
			return false;
			break;
		}
		case 3:{
			alert("전화번호를 입력하세요");
			frm.tel.focus();
			return false;
			break;
		}
	}
	if(frm.row.value ==""){
		alert("중복체크를 하세요")
		return false;
	}
	
	frm.submit();
}
	//전화번호 체크
	function check_number(tel){
		if(!tel==""){							//공란 아닐경우
			if(tel.length==11){					//11자일경우
				for(i=0;i<tel.length;i++){
					ch = tel.charAt(i);
					if(!(ch>=0 && ch<=9)){		//숫자가 아닌게 들어온 경우 0 (불량)
						return 0;
					}else{
						return 1;				//11자 모두 숫자일경우 (정상)
					}
				}
			}else if(tel.length==13){			//11자가 아닐경우 여부 확인
				arr = tel.split('-');
				tel2=arr[0]+arr[1]+arr[2];
				frm.tel.value = tel2;			//'-'기호를 빼고 다시 검색
				check_number(frm.tel.value);	
				
			}else{								//11자 미만입력
				frm.tel.focus();
				return 2;						//미입력오류 (불량)
			}
		}else{
			return 3;							//전화번호를 입력하세요
		}
	}
	
	function a_or_d(str){	//문자와 숫자만 사용 제한
		lower_str = str.toLowerCase();
		for(i=0;i<lower_str.length;i++){
			ch = lower_str.charAt(i);
			if(((ch<'a') || (ch>'z')) && ((ch<'0') || (ch>'9'))){
				return 0;
			}
		}
		return 1;
	}
	//아이디 중복 체크 팝업
	 function id_check() {
		 var urlString;
			//    urlString = "Member/id_check.jsp" ;
			urlString = "./check.do?userid="+frm.userid.value ;
		
		if(frm.userid.value == ""){
			alert("아이디를 입력하세요")
			frm.userid.focus();
			return false;
		 }
		else if(frm.userid.value.length <4 ){
			alert("4자이상")
			frm.userid.focus();
			return false;
		}else{
				window.open(urlString, "id_check"," resizable=no,x=3000,y=400,width=370,height=250");
		 }
	}
	function post_win(){
		window.open("./postCheck.do","post_check","scrollbar=yes,resizable=no,x=3000,y=400,width=370,height=250")	
	
	}
	</script>
</head>

<body bgcolor="#FFFFFF" LEFTMARGIN=0  TOPMARGIN=0 >
 
 <!-- 탑 메뉴 영역 삽입-->


<table border="0" width="800">
<tr>
  <td width="20%"  bgcolor="#ecf1ef" valign="top" style="padding-left:0;">
	
	<!--로그인 영역 삽입-->
  </td>
  <td width="80%" valign="top">&nbsp;<img src="./img/title1.gif" ><br>    
	<form name=frm method=post action="./insert.do">
	<input type ="hidden" name ="row" value="">
	<table border=0 cellpadding=0 cellspacing=0 border=0 width=730 valign=top>
		<tr><td align=center><br>                            
			<table cellpadding=0 cellspacing=0 border=0 width=650 align=center>       
				<tr>
					<td bgcolor="#7AAAD5">            
						<table cellpadding=0 cellspacing=0 border=0 width=100%>
							<tr bgcolor=#7AAAD5>
								<td align=left BORDER="0" HSPACE="0" VSPACE="0"><img src="./img/u_b02.gif"></td>
								<td align=center bgcolor="#7AAAD5"><FONT COLOR="#FFFFFF"><b>사용자등록&nbsp;</b><font color=black>(</font><font color=red>&nbsp;*&nbsp;</font><font color=black>표시항목은 반드시 입력하십시요.)</font></FONT></td>
								<td align=right BORDER="0" HSPACE="0" VSPACE="0"><img src="./img/u_b03.gif"></td>
							</tr>
						</table>
						<table cellpadding=3 cellspacing=1 border=0 width=100%>
							<tr>
								<td width=110 bgcolor=#EFF4F8>&nbsp;회원 성명<font color=red>&nbsp;*</font></td>
								<TD BGCOLOR=WHITE>
									<input type=text name=name size=16 maxlength=20 value="">성명은 빈칸없이 입력하세요.
								</td>
							</tr>
							
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;회원 ID<font color=red>&nbsp;*</font></td>
								<TD BGCOLOR=WHITE>
									<table cellspacing=0 cellpadding=0>
										<tr>
											<td align=absmiddle>
												<input type=text name=userid size=12 maxlength=16 value="" style="width:120">
											</td>
											<td>
                  			<a href="#" onclick = "return id_check()"><img src="./img/u_bt01.gif" hspace=2 border=0 name=img1  align=absmiddle></a>
                  		
                   		5~16자 이내의 영문이나 숫자만 가능합니다.
                  		</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;비밀번호<font color=red>&nbsp;*</font></td>
								<TD BGCOLOR=WHITE>
								<input type=password name=passwd size=8 maxlength=12 style="width:80">
                 6~12자 이내의 영문이나 숫자만 가능합니다.
								</td>
							</tr>
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;비밀번호확인<font color=red>&nbsp;*</font></td>
								<TD BGCOLOR=WHITE><input type=password name=repasswd size=8 maxlength=12 value="" style="width:80">비밀번호 확인을 위해서 비밀번호를 한번 더 입력해주세요. </td>
							</tr>
							
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;우편번호</td>
								<TD BGCOLOR=WHITE>
									<table cellspacing=0 cellpadding=0>
										<tr>
											<td>
                  			<input type=text name=zipcode size=7 maxlength=7 readonly>
                  
                  		</td>
                  		<td>
                  		<a href ="javascript:post_win()">
      									<img src="./img/u_bt07.gif" hspace=2 border=0 name=img2 align=absmiddle></a>
 											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;주소</td>
								<TD BGCOLOR=WHITE>
									<input type=text name=addr1 size=50 maxlength=100 value="" readonly>
								</td>
							</tr>
							<tr>
								<TD BGCOLOR="#EFF4F8">&nbsp;나머지 주소</td>
								<TD BGCOLOR=WHITE>
									<input type=text name=addr2 size=50 maxlength=100 value="">
								</td>
							</tr>
							<tr>
               <TD BGCOLOR="#EFF4F8">&nbsp;전화번호<font color=red>&nbsp;*</font></td>
                <TD BGCOLOR=WHITE>
                  <input type=text name=tel size=13 maxlength=13 value="">
                  
               </td>
              </tr>
              <tr>
                <TD BGCOLOR="#EFF4F8">&nbsp;E-mail
                	
                </td>
                <td bgcolor=WHITE valign=middle>
                  <input type=text name=email size=30 maxlength=30 value="">
                </td>
              </tr>
              <tr>
                <TD BGCOLOR="#EFF4F8">&nbsp;관심분야
                	
                </td>
								<td bgcolor=WHITE valign=middle> 
									<input type="checkbox" name="favorite" value="건강">건강 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="문화예술">문화예술 &nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="경제">경제 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="연예오락">연예오락 &nbsp;
									<input type="checkbox" name="favorite" value="뉴스">뉴스
									<br><input type="checkbox" name="favorite" value="여행레져">여행레져 &nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="생활">생활 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="스포츠">스포츠 &nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="교육">교육 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="컴퓨터">컴퓨터 &nbsp;&nbsp;
									<input type="checkbox" name="favorite" value="학문">학문 &nbsp;&nbsp;
								</td>
              </tr>
              <tr>
                <TD BGCOLOR="#EFF4F8">&nbsp;직업</td>
                <TD BGCOLOR=WHITE>
                  <select name=job class="formbox">
							<option value="0">선택하세요 ---
							<option value="회사원">회사원
							<option value="연구전문직">연구전문직
							<option value="교수학생">교수학생
							<option value="일반자영업">일반자영업
							<option value="공무원">공무원
							<option value="의료인">의료인
							<option value="법조인">법조인
							<option value="종교,언론,에술인">종교.언론/예술인
							<option value="농,축,수산,광업인">농/축/수산/광업인
							<option value="주부">주부
							<option value="무직">무직
							<option value="기타">기타
					</select>
				</td>                
              </tr>
              <tr>
                <TD BGCOLOR="#EFF4F8">&nbsp;자기소개<font color=red>&nbsp;</font></td>
                <TD BGCOLOR=WHITE>
                  <textarea cols=65 rows=5 name="intro"></textarea>
                </td>
              </tr>
            </table>
            <table cellpadding=0 cellspacing=0 border=0 width=100%>
              <tr bgcolor=#7AAAD5>
                <td valign=bottom>
                  <img src="./img/u_b04.gif" align=left hspace=0 vspace=0 border=0>
                </td>
                <td align=center></td>
                <td valign=bottom>
                  <img src="./img/u_b05.gif" align=right hspace=0 vspace=0 border=0>
                </td>
              </tr>
              <tr bgcolor=#ffffff>
                <td colspan=3 align=center>
                  <a href ="javascript:check()"><img src="./img/u_bt06.gif" vspace=3 border=0 name=img3></a>
                  <img src="./img/u_bt05.gif" border=0 hspace=10 vspace=3 name=img4>
                </td>
              </tr>
            </table> 
          </td>
        </tr>
      </td>
    </tr>
	</table>
	</form>
	</td>
</tr>
</table>

 <!-- copyright 영역 삽입-->
  

</body>
</html>
