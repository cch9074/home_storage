package memberexam.member;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/insert.do")
public class MemberInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public MemberInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		RequestDispatcher rd = request.getRequestDispatcher("./Member/userinfo_insert.jsp");
		rd.forward(request, response);
//		response.sendRedirect("Member/userinfo_insert.jsp");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String userid = request.getParameter("userid");
		String passwd = request.getParameter("passwd");
		String zipcode = request.getParameter("zipcode");
		String addr1 = request.getParameter("addr1");
		String addr2 = request.getParameter("addr2");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		String[] favoriteArr = request.getParameterValues("favorite"); 
		String favorite = ",";
		
		for( int i=0;i<favoriteArr.length;i++) {
			favorite += favoriteArr[i]+",";
		}
		
		String job = request.getParameter("job");
		String intro = request.getParameter("intro");
		
		MemberVO mVO = new MemberVO();
		mVO.setName(name);
		mVO.setUserid(userid);
		mVO.setPasswd(passwd);
		mVO.setZipcode(zipcode);
		mVO.setAddr1(addr1);
		mVO.setAddr2(addr2);
		mVO.setTel(tel);
		mVO.setEmail(email);
		mVO.setFavorite(favorite);
		mVO.setJob(job);
		mVO.setIntro(intro);
		mVO.setFirstdate(new Timestamp(System.currentTimeMillis()));
		mVO.setLastdate(new Timestamp(System.currentTimeMillis()));
		MemberDAO mDAO = MemberDAO.getInstance();
		int row =mDAO.insertMember(mVO);
		if(row==1) {
			request.setAttribute("mVO", mVO);
	//		response.sendRedirect("./Member/userinfo_insert.jsp");
			RequestDispatcher rd = request.getRequestDispatcher("login.do");
			rd.forward(request, response);
		}else {
			request.setAttribute("row", row);
			RequestDispatcher ds = request.getRequestDispatcher("insert.do");
			ds.forward(request, response);
		}
	}

}
