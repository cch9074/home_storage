package memberexam.member;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MemberModify
 */
@WebServlet("/modify.do")
public class MemberModify extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberModify() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		MemberVO mVO = (MemberVO)session.getAttribute("loginUser");		
		MemberDAO mDAO = MemberDAO.getInstance();
		mVO = mDAO.selectMember(mVO.getUserid());
		
		if( !mVO.getFavorite().equals("")) {
			String[] fa = mVO.getFavorite().split(",");
			request.setAttribute("fa", fa);
		}

		request.setAttribute("mVO", mVO);
		
		RequestDispatcher rd = request.getRequestDispatcher("./Member/userinfo_modify.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		String passwd = request.getParameter("passwd");
		String zipcode = request.getParameter("zipcode");
		String addr1 = request.getParameter("addr1");
		String addr2 = request.getParameter("addr2");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		String[] favoriteArr = request.getParameterValues("favorite"); 
		String favorite = "";
		
		for( int i=0;i<favoriteArr.length;i++) {
			favorite += favoriteArr[i]+",";
		}
		
		String job = request.getParameter("job");
		String intro = request.getParameter("intro");
		MemberVO mVO = new MemberVO();

		mVO.setPasswd(passwd);
		mVO.setZipcode(zipcode);
		mVO.setAddr1(addr1);
		mVO.setAddr2(addr2);
		mVO.setTel(tel);
		mVO.setEmail(email);
		mVO.setFavorite(favorite);
		mVO.setJob(job);
		mVO.setIntro(intro);
		System.out.println(passwd+""+favorite );
		MemberDAO mDAO = MemberDAO.getInstance();
		int row = mDAO.memberModify(mVO);
		if(row ==1 ) {
			RequestDispatcher rd = request.getRequestDispatcher("login.do");
			rd.forward(request, response);
		}
//		else {
//			request.setAttribute("row", row);
//			RequestDispatcher rd = request.getRequestDispatcher("modify.do");
//			rd.forward(request, response);
//		}
	}

}
