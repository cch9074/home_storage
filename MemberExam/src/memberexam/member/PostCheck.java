package memberexam.member;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/postCheck.do")
public class PostCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public PostCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		RequestDispatcher ds = request.getRequestDispatcher("Member/post_check.jsp");
		ds.forward(request, response);
		

	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String addr = request.getParameter("addr");
		MemberDAO mDAO = MemberDAO.getInstance();
		Vector vc = mDAO.checkPost(addr);
		
		request.setAttribute("vc", vc);
		RequestDispatcher ds = request.getRequestDispatcher("Member/post_check.jsp");
		ds.forward(request, response);
	}

}
