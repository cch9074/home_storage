package memberexam.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;

import memberexam.util.DBManager;

public class MemberDAO {
	private MemberDAO() {}
	
	private static MemberDAO instance = new MemberDAO();
	
	public static MemberDAO getInstance() {
		return instance;
	}
	//회원 등록 매서드
	public int insertMember(MemberVO mVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "insert into member(idx,name,userid,passwd,zipcode,addr1,addr2,tel,email,job,intro,favorite,firstdate,lastdate)"
		        + "values(member_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, mVO.getName());
			pstmt.setString(2, mVO.getUserid());
			pstmt.setString(3, mVO.getPasswd());
			pstmt.setString(4, mVO.getZipcode());
			pstmt.setString(5, mVO.getAddr1());
			pstmt.setString(6, mVO.getAddr2());
			pstmt.setString(7, mVO.getTel());
			pstmt.setString(8, mVO.getEmail());
			pstmt.setString(9, mVO.getJob());
			pstmt.setString(10, mVO.getIntro());
			pstmt.setString(11, mVO.getFavorite());
			pstmt.setTimestamp(12, mVO.getFirstdate());
			pstmt.setTimestamp(13, mVO.getLastdate());
		
			pstmt.executeUpdate();
			row=1;
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
	}
		
		public int idCheck(String userid) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			int row =0;
			String quary = "select count(*) as counter from member where userid =?  ";
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setString(1, userid);
				
				rs = pstmt.executeQuery();
				if(rs.next()) {
					row = rs.getInt("counter");
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt,rs);
			}	
			return row;
	}
		public Vector<ZipcodeVO> checkPost(String addr) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			Vector<ZipcodeVO> vc = new Vector();
			
			String quary = "select *  from zipcode where dong like'%"+addr+"%'";
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				rs=pstmt.executeQuery(quary);
				ZipcodeVO zc = null;
				while(rs.next()) {
				zc = new ZipcodeVO();
				zc.setZipcode(rs.getString("zipcode"));
				zc.setSido(rs.getString("sido"));
				zc.setGugun(rs.getString("gugan"));
				zc.setDong(rs.getString("dong"));
				zc.setBunji(rs.getString("bunji"));
				vc.add(zc);
				
				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt,rs);
				return vc;
			}	
		
	}
		public int memberLogin(String userid, String passwd) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			int row =-1;				//아이디가 없는  상태
			String quary = "select passwd from member where userid = ?  ";
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setString(1, userid);
				
				rs = pstmt.executeQuery();
				if(rs.next()) {
					
					if(rs.getString("passwd") != null && rs.getString("passwd").equals(passwd)) {
						row = 1;		//로그인 성공
					}else {
						row = 0 ;		//아이디는 있지만 비번이 맞지 않음.
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt,rs);
				return row;
			}	
		}
			
		public MemberVO getMember(String userid) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			String quary = "select * from member where userid = ?  ";
			MemberVO mVO = new MemberVO();
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setString(1, userid);
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					mVO.setIdx(rs.getInt("idx"));
					mVO.setName(rs.getString("name"));
					mVO.setUserid(rs.getString("userid"));
					mVO.setZipcode(rs.getString("zipcode"));
					mVO.setAddr1(rs.getString("addr1"));
					mVO.setAddr2(rs.getString("addr2"));
					mVO.setTel(rs.getString("tel"));
					mVO.setEmail(rs.getString("email"));
					mVO.setJob(rs.getString("job"));
					mVO.setFavorite(rs.getString("favorite"));
					
					
					mVO.setLastdate(rs.getTimestamp("lastdate"));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt,rs);
				return mVO;
			}				
	}
		public void setMember(Timestamp now ,String userid) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			String quary = "update member set lastdate =? where userid = ?  ";
			MemberVO mVO = new MemberVO();
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setTimestamp(1, now);
				pstmt.setString(2, userid);
				pstmt.executeUpdate();
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt);
				
			}				
	}
		public MemberVO selectMember(String userid) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			String quary = "select * from member where userid = ?  ";
			MemberVO mVO = new MemberVO();
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setString(1, userid);
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					mVO.setIdx(rs.getInt("idx"));
					mVO.setName(rs.getString("name"));
					mVO.setUserid(rs.getString("userid"));
					mVO.setPasswd(rs.getString("passwd"));
					mVO.setZipcode(rs.getString("zipcode"));
					mVO.setAddr1(rs.getString("addr1"));
					mVO.setAddr2(rs.getString("addr2"));
					mVO.setTel(rs.getString("tel"));
					mVO.setEmail(rs.getString("email"));
					mVO.setJob(rs.getString("job"));
					mVO.setFavorite(rs.getString("favorite"));
					mVO.setIntro(rs.getString("intro"));
					mVO.setFirstdate(rs.getTimestamp("firstdate"));
					mVO.setLastdate(rs.getTimestamp("lastdate"));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt,rs);
				return mVO;
			}
		}
		
		public int memberModify(MemberVO mVO) {
			Connection conn =null;
			PreparedStatement pstmt = null;
			String quary = "update member set passwd =? , zipcode =? , addr1 =? , addr2 =? , tel =? , email =? , job =? , favorite =? , intro = ?  where userid = ?";
			int row = 0;
			try {
				conn =  DBManager.getConnection();
				pstmt = conn.prepareStatement(quary);
				pstmt.setString(1, mVO.getPasswd());
				pstmt.setString(2, mVO.getZipcode());
				pstmt.setString(3, mVO.getAddr1());
				pstmt.setString(4, mVO.getAddr2());
				pstmt.setString(5, mVO.getTel());
				pstmt.setString(6, mVO.getEmail());
				pstmt.setString(7, mVO.getJob());
				pstmt.setString(8, mVO.getFavorite());
				pstmt.setString(9, mVO.getIntro());
				pstmt.setString(10, mVO.getUserid());
				
				
				pstmt.executeUpdate();
				row = 1;
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBManager.close(conn,pstmt);
				return row;
			}				
	}

}
