package memberexam.member;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Servlet implementation class MemberLogin
 */
@WebServlet("/login.do")
public class MemberLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid = request.getParameter("userid");
		String url ="./Member/userlogin_form.jsp";
		HttpSession session = request.getSession();//세션 형성 검사
		//session.invalidate();					//로그아웃 시 세션을 날려주는 매서드
		if(session.getAttribute("loginUser") != null) {// 로그인 되어있음
			url = "index.jsp";
		}
		request.setAttribute("userid", userid);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
		//response.sendRedirect("./Member/userlogin_form.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userid = request.getParameter("userid");
		String passwd = request.getParameter("passwd");
		String url = "./Member/userlogin_form.jsp";
		MemberDAO mDAO = MemberDAO.getInstance();
		int row = mDAO.memberLogin(userid,passwd);
		if (row ==1) {
			Timestamp now = new Timestamp(System.currentTimeMillis());
			mDAO.setMember(now ,userid);
			MemberVO mVO = mDAO.getMember(userid);
			HttpSession session = request.getSession();		//세션 생성
			session.setAttribute("loginUser", mVO);
			session.setMaxInactiveInterval(3600);
			url = "./Member/userlogin_ok.jsp";
		}else if(row == 0 ) {
			request.setAttribute("message", "비밀번호가 맞지 않습니다.");
		}else {
			request.setAttribute("message", "존재 하지 않는 아이디입니다.");
		}
			request.setAttribute("row", row);
			request.setAttribute("userid", userid);
			
			RequestDispatcher ds = request.getRequestDispatcher(url);
			ds.forward(request, response);
		//doGet(request, response);
	}

}
