package appMainEvent_Ex;

public class Member {
	private String no;
	private String name;
	private String tel;
	
	public String getNo() {
		return no;
	}


	public String getName() {
		return name;
	}


	public String getTel() {
		return tel;
	}


	public Member(String no, String name, String tel) {
		this.no= no;
		this.name= name;
		this.tel = tel;
		
	}

}
