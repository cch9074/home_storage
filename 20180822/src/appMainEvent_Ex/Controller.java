package appMainEvent_Ex;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller implements Initializable{
	@FXML private Button btn1;
	@FXML private Button btn2;
	@FXML private TextField txf1;
	@FXML private TextField txf2;
	@FXML private TextField txf3;
	@FXML private TextField txf4;
	@FXML private TextField txf5;
	List<Member> list = new ArrayList(); 
	Member member;
	
	@Override
	public void initialize(URL loacation, ResourceBundle resourceBundle) {
		
		 {
			String str = "";
			for(int i=0;i<6;i++) {
				int r = (int)(Math.random()*10);
				str+=r;
			}
			this.txf1.setText(str);
		 }
		 
		btn1.setOnAction(new EventHandler <ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("등록버튼 클릭");
				String no = txf1.getText();
				String name = txf2.getText();
				String tel = txf3.getText()+"-"+txf4.getText()+"-"+txf5.getText();
				
				member = new Member(no,name,tel);
				list.add(member);
				//txf1.setText("");
				txf2.setText("");
				txf3.setText("");
				txf4.setText("");
				txf5.setText("");
				
				initialize(null, null);
			}
		});	
		btn2.setOnAction(event->{
			System.out.println("출력버튼 클릭");
			System.out.println("성명\t사번\t전화");
			for(int i=0;i<list.size();i++) {
				Member prm = list.get(i);
				System.out.println(prm.getName()+"\t"+prm.getNo()+"\t"+prm.getTel());
			}
			txf1.setText("");
			txf2.setText("");
			txf3.setText("");
			txf4.setText("");
			txf5.setText("");
			
		});
	}
}
	
		
	
	
