import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AppMainEvent extends Application{
	@Override
	public void start(Stage stage) throws Exception{
		//메인 VBox
		VBox root = new VBox();
		root.setPadding(new Insets(10,10,10,10));
		//상단 HBox
		HBox hbox1 = new HBox();
		hbox1.setPadding(new Insets(10,10,10,10));
		Label label1 = new Label("I D: ");
		TextField tf1 = new TextField();
		hbox1.getChildren().add(label1);
		hbox1.getChildren().add(tf1);
		//중단 HBox
		HBox hbox2 = new HBox();
		hbox2.setPadding(new Insets(10,10,10,10));
		Label label2 = new Label("P W:");
		TextField tf2 = new TextField();
		hbox2.getChildren().add(label2);
		hbox2.getChildren().add(tf2);
		//하단 HBox
		HBox hbox3 = new HBox();
		hbox3.setPadding(new Insets(10,10,10,10));
		hbox3.setAlignment(Pos.CENTER);
		
		Button button1 = new Button("확인");
		//매서드에서 일회용 클래스를 만듦
		button1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("확인버튼이 클릭");
				String inputId = tf1.getText();
				String inputPw = tf2.getText();
				//String labeltext1 = label1.getText();
				System.out.println("ID :"+ inputId + "\nPW :"+ inputPw);
			}
		});
		Button button2 = new Button("취소");
		button2.setOnAction(event->{
			System.out.println("취소버튼이 클릭");
			tf1.setText("");
			tf2.setText("");
	}
		);
		
		hbox3.getChildren().add(button1);
		hbox3.setSpacing(60);
		hbox3.getChildren().add(button2);
		//메인VBox에 넣어줌
		root.getChildren().add(hbox1);
		root.getChildren().add(hbox2);
		root.getChildren().add(hbox3);
		//Scene를 설정하고, 메인 스테이지에 넣어준다.
		Scene scene = new Scene(root);	
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);

	}

}
