import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class RootController implements Initializable{
	@FXML private Button btn1;
	@FXML private Button btn2;
	@FXML private TextField txf1;
	@FXML private TextField txf2;
	
	@Override
	public void initialize(URL loacation, ResourceBundle resourceBundle) {
	btn1.setOnAction(new EventHandler <ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			System.out.println("확인버튼 클릭");
			String str1 = txf1.getText();
			String str2 = txf2.getText();
			
			System.out.println("ID :"+ str1+"PW :"+str2);
		}
		
	});	
	btn2.setOnAction(event->{
		System.out.println("취소버튼 클릭");
		txf1.setText("");
		txf2.setText("");
		}
	);
	}	
		
}		
