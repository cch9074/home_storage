package SignUp.db;

public class SignUpVO {
	int idx;
	String userid;
	String passwd;
	String name;
	String email;
	int level;
	String[] favoriteSongs = new String [10];
	String[] favoriteAlbums = new String [10];
	String[] favoriteArtists = new String [10];
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String[] getFavoriteSongs() {
		return favoriteSongs;
	}
	public void setFavoriteSongs(String[] favoriteSongs) {
		this.favoriteSongs = favoriteSongs;
	}
	public String[] getFavoriteAlbums() {
		return favoriteAlbums;
	}
	public void setFavoriteAlbums(String[] favoriteAlbums) {
		this.favoriteAlbums = favoriteAlbums;
	}
	public String[] getFavoriteArtists() {
		return favoriteArtists;
	}
	public void setFavoriteArtists(String[] favoriteArtists) {
		this.favoriteArtists = favoriteArtists;
	}
}
