package SignUp.db;

import java.sql.Connection;
import java.sql.PreparedStatement;

import SignUp.utill.DBManager;

public class SignUpDAO {
	private static SignUpDAO instance = new SignUpDAO();
	
	public static SignUpDAO getInstance() {
		return instance;
	}
	
	//회원 등록 매서드
	public int insertMember(SignUpVO sVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "insert into SignUp(idx,userid,passwd,name,email,lv)"
		        + "values(SignUp_seq.nextval,?,?,?,?,?)";
		try {
			conn = DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, sVO.getUserid());
			pstmt.setString(2, sVO.getPasswd());
			pstmt.setString(3, sVO.getName());
			pstmt.setString(4, sVO.getEmail());
			pstmt.setInt(5,1);
			
			pstmt.executeUpdate();
			row =1 ;
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn, pstmt);
			return row;
		}
	}
	

}
