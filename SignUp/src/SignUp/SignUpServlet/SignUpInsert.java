package SignUp.SignUpServlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import SignUp.db.SignUpDAO;
import SignUp.db.SignUpVO;

/**
 * Servlet implementation class SignUpInsert
 */
@WebServlet("/insert.do")
public class SignUpInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("Insert.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String userid = request.getParameter("userid");
		String passwd = request.getParameter("passwd");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		SignUpDAO sDAO = SignUpDAO.getInstance();
		SignUpVO sVO = new SignUpVO();
		sVO.setUserid(userid);
		sVO.setPasswd(passwd);
		sVO.setName(name);
		sVO.setEmail(email);
		String url = "";
		int row = sDAO.insertMember(sVO);
		if(row ==1) {
			url = "Insert_pro.jsp";
		}else {
			//회원가입 실패
		}
		request.setAttribute("sVO", sVO);
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	
	}

}
