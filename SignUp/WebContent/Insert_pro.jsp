<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원가입 확인</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript" src="script.js"></script>
</head>
<body>
<h1>회원가입 확인</h1>
<form action="insert.do" method="post" name="regForm">
    <fieldset>
        <legend>회원가입확인</legend>
        <table class="regist_table">
            <tr>
                <th style="width:100px" scope="row"><label for="userid">아이디</label></th>
                <td><input type="text"  name="userid" value ="${sVO.userid }"  /></td>
            </tr>
            <tr>
                <th scope="row"><label for="passwd">패스워드</label></th>
                <td><input type="password" name="passwd" value ="${sVO.passwd }"  /></td>
            </tr>
          
            <tr>
                <th scope="row"><label for="name">이름</label></th>
                <td><input type="text" name="name" value ="${sVO.name }" /></td>
            </tr>    
            <tr>
                <th scope="row"><label for="email">이메일</label></th>
                <td><input type="text" name="email" value ="${sVO.email }" /></td>
            </tr>    
                           
        </table>
  
    </fieldset>
</form>
</body>
</html>