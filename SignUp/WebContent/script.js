/**
 * 
 */
function formValidate(){
    var frm = document.regForm;
    if(frm.userid.value==""){
        alert("아이디를 입력하세요");
        frm.userid.focus();
        return;
    }
    if(frm.passwd.value==""){
        alert('비밀번호를 입력하세요');
        frm.passwd.focus();
        return;
    }
    if(frm.repasswd.value==""){
        alert("비밀번호를 다시입력해주세요");
        frm.repasswd.focus();
        return;
    }
    if(frm.name.value == ""){
        alert("이름을 입력해주세요");
        frm.name.focus();
        return;
    }
    if(frm.email.value == ""){
        alert("이메일을 입력해주세요.");
        frm.email.focus();
        return;
    }

    if(frm.passwd.value != frm.repasswd.value){
        alert("비밀번호가 일치하지 않습니다.");
        frm.repasswd.focus();
        return;
    }
    frm.submit();
    
}