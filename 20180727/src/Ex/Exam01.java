package Ex;
import java.io.File;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

/*
입력형식 : input.txt
번호, 이름, 국어, 영어, 수학(점수)


모니터로 출력 (결과)
~ 성적 일람표 ~
번호 이름 국 영 수 총 평 석차

  합  계                ㅇ
  평  균                   ㅇ


제약
인원수 10명 이내
평균 소수이하 첫째자리 (둘째자리에서 반올

석차 기준 오름차


*/


class Stu
{   
   int no=0;
   String name="";
   int kor = 0, eng = 0, mat = 0;
   
   int total = 0;
   float avg = 0;
   String grade="";
}

class GradeChart
{
   int[] answer = new int[10];
   int LineCnt = 0;
   String except0 = "";
   String except1 = "";
   String[][] stu;
      
   void reading() throws Exception
   {// 데이터파일로부터 값을 불러와서 배열화하는 메서드
      
      Scanner tmp0 = new Scanner(new File("C:\\30R\\JAVA\\input.txt"));
      Scanner scan0 = new Scanner(new File("C:\\30R\\JAVA\\input.txt"));
      
      while(true) // 몇 줄 들어왔나 세는 작업
      {
         try {
         String a = tmp0.nextLine();
         this.LineCnt++;
         }catch(Exception e)
         {
            break;
         } // 라인이 끝난다 = 에러 = 브레이크 = LineCnt완료
      }
      
      stu = new String[LineCnt][5]; // 이걸 위함
      
      for(int i = 0; i < stu.length; i++)
      {
         String a = scan0.nextLine();
         StringTokenizer st = new StringTokenizer(a, ", ");
         
         if(st.countTokens() != 5)
         {
            this.except0 = st.nextToken();
            this.except1 = st.nextToken();
                  
            System.out.println("출석번호 "+this.except0+"번 "+this.except1+"!! 너 입력 잘못했어!!");
            int k = 2;
            while(st.hasMoreTokens())
            {
               this.stu[i][0] = this.except0;
               this.stu[i][1] = this.except1;
               try {
               this.stu[i][k++] = st.nextToken();
               }catch(Exception e)
               {
                  System.out.println(this.except1+" 학생, 너는 대체 어떻게 입력한거니?");
                  for(int x = 2; x < stu[i].length; x++)
                  {
                     this.stu[i][x] = null;
                  }
                  break;
               }
            }
            continue;
         }
         int j = 0;
         while(st.hasMoreTokens())
         {
            this.stu[i][j++] = st.nextToken();
         }
      }
      
      // 리딩 1부 제대로 됐는지 확인
      /*
      for(int i = 0; i < stu.length; i++)
      {
         System.out.println(Arrays.toString(stu[i]));
      }
      */
   }   
   
   public Stu[] translating()
   {
      Stu[] a1 = new Stu[LineCnt];
      
      for(int i = 0; i < a1.length; i++)
      {
         a1[i] = new Stu();
         // 변수 프라이빗인 경우 이 부분을 변경
         // Stu 클래스에서 생성자 선언하고
         // 아래의 값들을 생성자로서 넣어주면 됨
         
         try {
         a1[i].no = Integer.parseInt(this.stu[i][0]);
         a1[i].name = this.stu[i][1];
         a1[i].kor = Integer.parseInt(this.stu[i][2]);
         a1[i].eng = Integer.parseInt(this.stu[i][3]);
         a1[i].mat = Integer.parseInt(this.stu[i][4]);
         }
         catch(Exception e)
         {
            a1[i].no = Integer.parseInt(this.stu[i][0]);
            a1[i].name = this.stu[i][1];
            a1[i].kor = -1;
            a1[i].eng = -1;
            a1[i].mat = -1;
         }
         a1[i].total = a1[i].kor + a1[i].eng + a1[i].mat;
         a1[i].avg = Math.round((a1[i].total / 3f)*10)/10f;
      }
      return a1;
   }
   
   void sorting(Stu[] a1)
   {
      for(int i=0; i < a1.length - 1; i++)
      {
         boolean changed = false;
         
         for (int j=0; j < a1.length - 1 - i; j++)
         {
            if(a1[j].total < a1[j+1].total) // 총점기준
            {
                        Stu tmp = a1[j];
                          a1[j] = a1[j+1]; 
                        a1[j+1] = tmp;
                     changed  = true;                      
            }
         }
         if(!changed) break; 
      }
   }
   
   void disp(Stu[] a1)
   {
      System.out.println("\t>>\t\t*성적일람표*\t\t<<");
      System.out.println("번호      ||  이름    ||  국어    || 영어    || 수학    || 총점    || 평균    || 석차    ");
      for(int i = 0; i < a1.length; i++)
      {
         System.out.printf("%2d\t%6s\t%5d\t%5d\t%5d\t%5d\t%6.1f\t%4d\t",a1[i].no,a1[i].name,a1[i].kor,a1[i].eng,a1[i].mat,a1[i].total,a1[i].avg,i+1);
         System.out.println("");
      }
      
      int korT = 0, engT = 0, matT = 0, TT = 0;
      float korA = 0, engA = 0, matA = 0, avgT = 0;
      
      System.out.print("         합\t계\t");
      for(int i = 0; i < a1.length; i++)
      {
         korT += a1[i].kor; 
         engT += a1[i].eng;
         matT += a1[i].mat;
         TT   += a1[i].total;
         avgT += a1[i].avg;
      }
      korA = Math.round((korT / (float)a1.length)*10)/10f;
      engA = Math.round((engT / (float)a1.length)*10)/10f;
      matA = Math.round((matT / (float)a1.length)*10)/10f;
      avgT = Math.round((avgT / (float)a1.length)*10)/10f;
      
      System.out.printf("%5d\t%5d\t%5d\t%5d%n", korT, engT, matT, TT);
      System.out.print("         평\t균\t");
      System.out.printf("%5.1f\t%5.1f\t%5.1f\t\t%6.1f%n", korA, engA, matA, avgT);
      
   }
   
   void doing() throws Exception
   {
      reading();
      Stu[] a1 = translating();
      sorting(a1);
      disp(a1);
   }
}


public class Exam01
{
   public static void main(String[] args) throws Exception
   {
      GradeChart a1 = new GradeChart();
      
      a1.doing();
   }

}