import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularEx2 {

	public static void main(String[] args) {
		String[] data = {"bat","baby","bonus","c","cA","ca","co","c.","c0","c#","car","combat","count","data","disc"};
		
		String[] pattern = {".*"/*모든 문자열*/,
							"c[a-z]",/*c로시작하는 2자리 영단어*/
							"c[a-zA-Z]",/*대소문자 구분 X*/
							"c[a-zA-Z0-9]"/*숫자와 영어로 조홥된 두글자*/,
							"c\\w",			//동일
							"c."/*두자리 문자열*/,
							"c.*"/*모든 문자열*/,
							"c\\."/*c.*/,
							"c\\d",/*c와 숫자로 이루어진 2자리수 문자열*/
							//== c[0-9]
							"c.*t"/*t로 시작하고 c로 끝나는 문자열*/,
							"[b|c].*"/*b,c로 시작하는 문자열*/,
							".*a.*"/*a를 포함하는 문자열*/,
							".*a.+"/*위와 같지만 a로 끝나는 문자열은 제외*/,
							"[b|c].{2}"/*c,b가 들어가는 3자리 문자열*/};
		
		for(int i=0;i<pattern.length;i++) {
			Pattern p = Pattern.compile(pattern[i]);
			System.out.print("Pattern : " + pattern[i] +"결과 : ");
			for(int j=0;j<data.length;j++) {
				Matcher m = p.matcher(data[j]);
				if(m.matches())
					System.out.print(data[j]+",");
			}
			System.out.println();
		}
	}

}
