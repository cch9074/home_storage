import java.util.regex.Pattern;

public class PatternExam {

	public static void main(String[] args) {
		String regExp = "(02|010)-\\d{3,4}-\\d{4}";
						//02나 010인지 확인 \\d는 숫자를 의미{}안의 숫자는 자리수를 의미
		String tel = "010-2242-3810";
		boolean bool = Pattern.matches(regExp,tel);
						//regExp와 tel을 비교한다.
		if(bool) {
			System.out.println("정규식과 일치합니다.");
		}else {
			System.out.println("전화번호 입력 오류 다시 입력하세요.");
			
		}
		System.out.println("=============");
		regExp="\\w+@\\w+\\.w+";
		// .만 입력하면 모든 문자열
		//\.는 '.'을 포함하는 문자열
		String email = "websnet@nate.com";
		if(bool) {
			System.out.println("정규식과 일치합니다.");
		}else {
			System.out.println("이메일 오류 다시 입력하세요.");
		}
		
		
	}

}
