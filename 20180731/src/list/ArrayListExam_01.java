package list;

import java.util.*;

public class ArrayListExam_01 {
	public static void main(String[] args) {
		ArrayList alist = new ArrayList(/*30*/);//기본크기를 정해줄수 있다 
		List list = new ArrayList();
		List<String> list2 = new ArrayList<String>();
		//제너릭 
		list.add("Java");
		list.add("JSP");
		list.add(1,"Database");
		list.add(123);
		list.add(new Integer("345"));
		//다양한 타입을 저장할 수 있다.
		int size = list.size();
		System.out.println("용량 : "+size);
		//데이터 유무 확인
		boolean bool = list.contains("JSP");
		System.out.println(bool);
		//데이터 번호(위치) 확인
		int index = list.indexOf("JSP");
		System.out.println("JSP 위치 : "+index);
		list.set(0, "Java Program");
		
		//배열의 형태로 출력할수 있다.
		System.out.println(list);
		//출력
		for(int i=0;i<size;i++) {
			Object obj = list.get(i);
			System.out.println(obj);
		}
		list.remove(1);
		list.remove(new Integer("123"));
		System.out.println("=============");
		//Iterator			//토큰과 비슷한 개념
		Iterator iter = list.iterator();
		while(iter.hasNext()) {
			Object obj = iter.next();
			System.out.println(obj);
		}
		//리스트 청소(내용 비움)
		list.clear();
		//리스트 내 데이터 존재여부 검사
		System.out.println(list.isEmpty());
		
	}
}


