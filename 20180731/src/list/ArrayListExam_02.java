package list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListExam_02 {

	public static void main(String[] args) {
		//처음부터 기본값을 가지는 List 만들기
		List list = new ArrayList();
		list.add("AAAA");
		
		List list2 = Arrays.asList("AAA","BBB","CCC");
		//크기 및 값을 고정하기 때문에 새로 add 할수 없다.
		//list2.add("DDD");
		
		System.out.println(list2);

	}

}
