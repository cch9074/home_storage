package list;
import java.util.*;
public class ArrayListExam_03 {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("AAAA");
		list.add("BBBB");
		list.add("CCCC");
		list.add("DDDD");
		
		for(String str : list) {
			System.out.println(str);
		}//순서대로 출력
		System.out.println("----");
		
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i));
		}//index값을 이용해서 출력
		System.out.println("----");
		Iterator ite = list.iterator();
		while(ite.hasNext()) {
			String str = (String)ite.next();
			//return값이 Object이기 때문에 <String>로캐스팅해준다.
			System.out.println(str);
		}
	}
}