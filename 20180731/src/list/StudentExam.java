package list;

import java.util.ArrayList;
import java.util.List;

class Student{
	private int bun;
	private String name;
	private int kor;
	private int eng;
	private int mat;
	private int tot;
	Student(){}
	
	Student(int bun,String name, int kor,int eng,int mat){
		this.bun = bun;
		this.name = name;
		this.kor = kor;
		this.eng = eng;
		this.mat = mat;
		tot = kor + eng + mat;
	}
	void disp(List<Student> list ,String msg) {
		if(msg.equals("�Է�")) {
			print(list);
		}
		if(msg.equals("��������")) {
			sort(list);
			print(list);
		}else {
			System.out.println("�Է�,�������� �� �Է����ּ���");
		}
	}
	public void print(List<Student> list) {
		int size = list.size();
		
			System.out.printf("��ȣ\t%14s\t\t����\t����\t����\t����%n","�̸�");
			for(int i=0;i<size;i++) {
			System.out.print(list.get(i).bun+"\t");
			System.out.printf("%14s",list.get(i).name+"\t");
			System.out.print(list.get(i).kor+"\t");
			System.out.print(list.get(i).eng+"\t");
			System.out.print(list.get(i).mat+"\t");
			System.out.print(list.get(i).tot+"\t");
			System.out.println();		
		}
	}
	void sort(List<Student> list) {
		int size = list.size();
		for(int i=0;i<size;i++) {
			for(int j=i;j<size;j++) {
				if(list.get(i).tot<list.get(j).tot) {
					Student tmp = list.get(i);
					list.set(i,list.get(j));
					list.set(j,tmp);
				}
			}
		}
	}
}
public class StudentExam {
	public static void main(String[] args) {
		List<Student> list = new ArrayList<Student>();
		
		Student stu1 = new Student(1,"��ö��",66,77,88);
		list.add(stu1);
		list.add(new Student(2,"��ö��ö��",77,88,99));
		list.add(new Student(3,"��ö��ö��ö��",67,78,89));
		list.add(new Student(4,"�迵������",55,66,77));
		list.add(new Student(5,"�迵����",44,33,55));
		list.add(new Student(6,"�迵��",88,55,66));
		
		stu1.disp(list,"�Է�");
		stu1.disp(list,"��������");
	}
}
