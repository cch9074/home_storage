package list;
import java.util.*;
class Board{
	private String subject;
	private String content;
	
	public Board(){}
	public Board(String subject, String content) {
		this.subject = subject;
		this.content = content;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
public class BoardExam {

	public static void main(String[] args) {
		
		Vector<Board> vc = new Vector<Board>();		
		//기본크기(10)를 가지고 있다.
		//크기 출력
		System.out.println(vc.capacity());
		vc.add(new Board("제목1","내용1"));
		vc.add(new Board("제목2","내용2"));
		int size = vc.size();
		for(int i=0;i<size ;i++) {
			Board b = vc.get(i);
			System.out.println(b.getContent()+"   "+b.getSubject());
		}
		System.out.println("-----------");
		//vc.add("AAA");	<Board> 로 입력타입을 고정했기때문에 넣을 수 없다.
		Object o = vc.get(0);		//어떤 객체가 들어있는지 모를때는 Object로 받을 수 있다.	
		Board b1 = (Board)vc.get(0);//주소를 가리킨다.
		Board b2 = (Board)vc.get(1);
		vc.set(0, b2);
		vc.set(1, b1);
		//다른 정렬과 다르게 임시로 저장해줄 필요가 없다.
		
		for(int i=0;i<size ;i++) {
			Board b = vc.get(i);
			System.out.println(b.getContent()+"   "+b.getSubject());
			
		}
	}
}
