package Bank;
import java.util.*;
public class BankApp {
	private static Account[] accountArr = new Account[10];
	private static Scanner scanner = new Scanner(System.in);
	static int  memberCount=0;

	public static void main(String[] args) {
		
		boolean flag = true;
		while(flag) {
			System.out.println("--------------------------");
			System.out.println("1.계좌생성 | 2.계좌목록 | 3.예금 | 4.출금 | 5.종료");
			System.out.println("선택>");
			System.out.println("현재 생성가능한 계좌수 : "+ (accountArr.length-memberCount));
			
			int selectNo = scanner.nextInt();
			
			if(selectNo==1) {
				createAccount();
			}else if(selectNo==2) {
				accountList();
			}else if(selectNo==3) {
				deposit();
			}else if(selectNo==4) {
				withdraw(); 
			}else if(selectNo==5) {
				flag=false;
			}
		}
		System.out.println("프로그램 종료");
	}
	
	private static void createAccount() {
		
		if(accountArr[accountArr.length-1]==null) {
			for(int i=0;i<1;i++) {
				System.out.println("------");
				System.out.println("계좌생성");
				System.out.println("------");
				System.out.print("계좌번호 : ");
				String ano =scanner.next();
				try {
					if(findAccount(ano)!=null) {
						System.out.println("같은 계좌번호가 존재합니다. 다시 입력해주세요");
						break;
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.print("예금주 : ");
				String owner =scanner.next();
				System.out.print("초기입금액 : ");
				int balance =scanner.nextInt();
				System.out.print("계좌가 생성되었습니다. ");
				Account a = new Account(ano,owner,balance);
				accountArr[memberCount]=a;
				memberCount++;
			}
		}else {
			System.out.println("더이상 계좌를 생성할 수 없습니다.");
		}
		
	}
	
	private static void accountList() {
		if(memberCount==0) {
			System.out.println("생성된 계좌가 없습니다.");
		}else {
			for(int i=0;i<accountArr.length;i++) {
				if(accountArr[i]!=null)
				System.out.println(accountArr[i].getAno() +"\t"
						+accountArr[i].getOwner()+"\t"+accountArr[i].getBalance());
			}
		}
	}
	
	private static void deposit() {
		if(memberCount==0) {
			System.out.println("생성된 계좌가 없습니다.");
		}else {
			System.out.print("계좌번호 : ");
			String ownerSearch = scanner.next();
			for(int i=0;i<accountArr.length;i++) {
				if(accountArr[i]!=null) {
					String search = accountArr[i].getAno();
					if(search.equals(ownerSearch)) {
						System.out.print("예금액 (원): ");
						int inputMoney = scanner.nextInt();
						accountArr[i].setBalance(accountArr[i].getBalance()+inputMoney);
						System.out.println("예금이 완료되었습니다.");
					}
					else {
						System.out.println("일치하는 계좌가 없습니다.");
					}
				}
			}
		}
	}
	
	private static void withdraw() {
		if(memberCount==0) {
			System.out.println("생성된 계좌가 없습니다.");
		}else {
			System.out.print("계좌번호 : ");
			String ownerSearch = scanner.next();
			for(int i=0;i<accountArr.length;i++) {
				if(accountArr[i]!=null) {
					String search = accountArr[i].getAno();
					if(search.equals(ownerSearch)) {
						System.out.print("출금액  (원): ");
						int outputMoney = scanner.nextInt();
						if(outputMoney<accountArr[i].getBalance()) {
							accountArr[i].setBalance(accountArr[i].getBalance()-outputMoney);
							System.out.println("출금이 완료되었습니다.");
						}else {
							System.out.println("잔액이 부족합니다.");
						}
					}else {
						System.out.println("일치하는 계좌가 없습니다.");
					}
				}
			}
		}	
	}
	
	private static Account findAccount(String ano)throws Exception {
		
		for(int i=0;i<accountArr.length;i++) {
			if(accountArr[i]!=null) {	
				if(accountArr[i].getAno().equals(ano)) {
					return accountArr[i];
				}
			}
		}
		return null;
	}	
	
}
