import java.io.*;

//객체의 직렬화를 위해 인터페이스를 구현해주어야한다.
//오버라이딩 할 매서드가 없다.
class Point implements Serializable {
	int a;
	int b;
	Point(){}
	
	Point(int a, int b){
		this.a = a;
		this.b =b;
	}
	
	public void print() {
		System.out.println("a="+a);
		System.out.println("b="+b);
	}
}

public class Exam_03 {

	public static void main(String[] args) throws Exception{
		Point[] pArr =new Point[2];
		Point p = new Point(10,20);
		Point p2 = new Point(20,30);
		
		pArr[0] = p;
		pArr[1] = p2;
		
		p.print();
		p2.print();
		//입력
		File f = new File("C:\\test\\work\\object.txt");
		FileOutputStream fos = new FileOutputStream(f);
		BufferedOutputStream bof = new BufferedOutputStream(fos);
		ObjectOutputStream  oos = new ObjectOutputStream(bof);
		//오브젝트를 출력하는 스트림을 생성.
		//입력은 되지만 깨진다.
		//oos.writeObject(p);
		//oos.writeObject(p2);
		oos.writeObject(pArr);
		oos.close();
		bof.close();
		fos.close();
		
	}

}
