import java.io.*;
public class Exam_02 {

	public static void main(String[] args)throws Exception {
		File f = new File("C:\\Users\\JSLHRD\\Desktop\\Bin\\20180817\\src\\Exam_01.java");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		//한줄 씩 읽어온다.
		
		String d = "";
		//값을 읽어오면서 여러줄일 경우 널값이 아닐때 까지 반복한다.
		while((d=br.readLine())!=null) {
			//br.readLine();
			System.out.println(d);
		}
		//열어준 역순으로 닫는다.
		br.close();
		fr.close();
	}

}
