import java.io.*;
public class Exam_01 {

	public static void main(String[] args) throws Exception{
		File f = new File("C:\\test\\work\\samp.txt");
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		//bw.write("aaaaa");
		PrintWriter pw = new PrintWriter(bw);//파일 출력
		
		PrintWriter pw2 = new PrintWriter(new BufferedWriter(
				new OutputStreamWriter(System.out)));//콘솔 출력
		
		//bw.close();
		pw.println("파일로 출력합니다.");
		pw.println("파일로 출력합니다.");
		
		pw2.println("콘솔로 출력합니다.");
		pw.close();
		pw2.close();
	}
}
