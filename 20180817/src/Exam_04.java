import java.io.*;

public class Exam_04 {

	public static void main(String[] args) throws Exception{
		File f = new File("C:\\test\\work\\object.txt");
		FileInputStream fos = new FileInputStream(f);
		BufferedInputStream bof = new BufferedInputStream(fos);
		ObjectInputStream  oos = new ObjectInputStream(bof);
		
		Object[] obj = null;
		obj=(Point[])oos.readObject();
		//파일에 저장된 객체의 값을 받아옴
		for(int i=0;i<obj.length;i++) {
			 
		 
		 Point p = (Point)obj[i];
		 p.print();
			 
		}		
		//받아온 객체의 정보를 이용해 객체를 생성
		oos.close();
		bof.close();
		fos.close();

	}

}