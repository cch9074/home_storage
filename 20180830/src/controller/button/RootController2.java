package controller.button;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
public class RootController2 implements Initializable {
	@FXML private ToggleGroup group;
	@FXML private Label lbl;	
	@FXML private Button btnExit;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, 
					                               Toggle oldValue, Toggle newValue) {
				System.out.println(newValue.getUserData().toString());
			}
		});
	}
	
}
