package javafxFinal;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Student {
	private SimpleStringProperty  name;
	private SimpleIntegerProperty  kor;
	private SimpleIntegerProperty  mat;
	private SimpleIntegerProperty  eng;
	public Student() {
		this.name = new SimpleStringProperty();
		this.kor=new SimpleIntegerProperty();
		this.mat=new SimpleIntegerProperty();
		this.eng=new SimpleIntegerProperty();
	}
	
	public Student(String name, int kor, int mat, int eng) {
		this.name = new SimpleStringProperty(name);
		this.kor=new SimpleIntegerProperty(kor);
		this.mat=new SimpleIntegerProperty(mat);
		this.eng=new SimpleIntegerProperty(eng);
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public int getKor() {
		return kor.get();
	}

	public void setKor(int kor) {
		this.kor.set(kor);
	}

	public int getMat() {
		return mat.get();
	}

	public void setMat(int mat) {
		this.mat.set(mat);
	}

	public int getEng() {
		return eng.get();
	}

	public void setEng(int eng) {
		this.eng.set(eng);
	}
	

}
