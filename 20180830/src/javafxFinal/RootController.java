package javafxFinal;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class RootController implements Initializable{
	@FXML private Button btn1;
	@FXML private Button btn2;
	@FXML private BarChart barChart;
	@FXML private TableView<Student> tableView;	

	ObservableList<Student> studentList;
	List<XYChart.Series> list;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		studentList = FXCollections.observableArrayList();
		list =FXCollections.observableArrayList();
		TableColumn tc = tableView.getColumns().get(0);
		tc.setCellValueFactory(
			new PropertyValueFactory("name")
		);
		tc = tableView.getColumns().get(1);
		tc.setCellValueFactory(
			new PropertyValueFactory("kor")
	    );
		tc = tableView.getColumns().get(2);
		tc.setCellValueFactory(
			new PropertyValueFactory("mat")
	    );	
		tc = tableView.getColumns().get(3);
		tc.setCellValueFactory(
			new PropertyValueFactory("eng")
	    );
		tableView.setItems(studentList);
		
	
		
		btn1.setOnAction(event->handleCustom(event));
		btn2.setOnAction(event->handleBarChart(event));

	}
	
	public void handleCustom(ActionEvent event)  {
		try {
			Stage dialog = new Stage(StageStyle.UTILITY);
			dialog.initModality(Modality.WINDOW_MODAL);
			dialog.initOwner(btn1.getScene().getWindow());
			dialog.setTitle("확인");
			Parent parent = FXMLLoader.load(getClass().getResource("form.fxml"));
			
			Button btnFormAdd = (Button) parent.lookup("#btnSave");
			btnFormAdd.setOnAction(e->{
				TextField txtName = (TextField) parent.lookup("#txfName");
				TextField txtKorean = (TextField) parent.lookup("#txfKor");
				TextField txtMath = (TextField) parent.lookup("#txfMat");
				TextField txtEnglish = (TextField) parent.lookup("#txfEng");
				studentList.add(new Student(
					txtName.getText(), 
					Integer.parseInt(txtKorean.getText()),
					Integer.parseInt(txtMath.getText()), 
					Integer.parseInt(txtEnglish.getText())
				));
				
				dialog.close();
			});
			
			Button btnFormCancel = (Button) parent.lookup("#btnExit");
			btnFormCancel.setOnAction(e->dialog.close());
			Scene scene = new Scene(parent);
			
			dialog.setScene(scene);
			dialog.setResizable(false);
			
			dialog.show();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
	}
	public void handleBarChart(ActionEvent event) {
		try {
		Stage dialog = new Stage(StageStyle.UTILITY);
		dialog.initModality(Modality.WINDOW_MODAL);
		dialog.initOwner(btn2.getScene().getWindow());
		dialog.setTitle("차트보기");
		Parent parent = FXMLLoader.load(getClass().getResource("barChart.fxml"));
		Scene scene = new Scene(parent);
		BarChart barChart = (BarChart) parent.lookup("#barChart");
		
		XYChart.Series seriesKorean = new XYChart.Series();
		seriesKorean.setName("국어");   
		ObservableList koreanList = FXCollections.observableArrayList();
		for(int i=0; i<studentList.size(); i++) {
			koreanList.add(new XYChart.Data(studentList.get(i).getName(), studentList.get(i).getKor()));
		}
		seriesKorean.setData(koreanList);
		barChart.getData().add(seriesKorean);
		
		XYChart.Series seriesMath = new XYChart.Series();
		seriesMath.setName("수학");   
		ObservableList mathList = FXCollections.observableArrayList();
		for(int i=0; i<studentList.size(); i++) {
			mathList.add(new XYChart.Data(studentList.get(i).getName(), studentList.get(i).getMat()));
		}
		seriesMath.setData(mathList);
		barChart.getData().add(seriesMath);
		
		XYChart.Series seriesEnglish = new XYChart.Series();
		seriesEnglish.setName("영어");   
		ObservableList englishList = FXCollections.observableArrayList();
		for(int i=0; i<studentList.size(); i++) {
			englishList.add(new XYChart.Data(studentList.get(i).getName(), studentList.get(i).getEng()));
		}
		seriesEnglish.setData(englishList);
		barChart.getData().add(seriesEnglish);
		
		Button btnClose = (Button) parent.lookup("#btnClose");
		btnClose.setOnAction(e->dialog.close());
		
		dialog.setScene(scene);
		dialog.setResizable(false);
		
		dialog.show();
		}catch(Exception e) {
			
		}
	}

}

