package javafxFinal;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class FormController implements Initializable{
	@FXML private TextField txfName;
	@FXML private TextField txfKor;
	@FXML private TextField txfMat;
	@FXML private TextField txfEng;
	@FXML private TableView<Student> tableView;
	List<Student> list = new ArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	public void handleSaveAction(ActionEvent e) {
		String name = txfName.getText();
		int kor = Integer.parseInt(txfKor.getText());
		int mat = Integer.parseInt(txfMat.getText());
		int eng = Integer.parseInt(txfEng.getText());
		
		Student member = new Student(name,kor,mat,eng);
	
	}

	public void handleBtnExitAction(ActionEvent e) {
		Platform.exit();
	}
}
