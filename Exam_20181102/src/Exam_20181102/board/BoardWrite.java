package Exam_20181102.board;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/write.do")
public class BoardWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public BoardWrite() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("./Board/board_write.jsp");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		
		String name = request.getParameter("name");
		String enamil = request.getParameter("enamil");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		String pass = request.getParameter("pass");
		
		//DB저장
		int row = 1;
		if( row==1) {
			response.sendRedirect("Board/board_list.jsp");
			
		}else {
			response.sendRedirect("Board/board_write.jsp");
			
		}
		
	}

}
