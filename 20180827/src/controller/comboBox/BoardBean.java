package controller.comboBox;

public class BoardBean {
	private String subject;
	private String pass;
	private String combo;
	private String dateExit;
	private String content;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCombo() {
		return combo;
	}
	public void setCombo(String combo) {
		this.combo = combo;
	}
	public String getDateExit() {
		return dateExit;
	}
	public void setDateExit(String dateExit) {
		this.dateExit = dateExit;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	

}
