package controller.comboBox;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class RootController implements Initializable{
	@FXML private TextField txtTitle;
	@FXML private PasswordField txtPassord;
	@FXML private ComboBox<String> comboPublic;
	@FXML private DatePicker dateExit;
	@FXML private TextArea txtContent;

	List<BoardBean> list = new ArrayList<BoardBean>();
	BoardBean board;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	public void handleBtnRegAction(ActionEvent event) {
		String subject = txtTitle.getText();
		String pass = txtPassord.getText();
		String combo = comboPublic.getValue();
		LocalDate d = dateExit.getValue();
		String content = txtContent.getText();
		
		board= new BoardBean();
		board.setSubject(subject);
		board.setCombo(combo);
		board.setContent(content);
		board.setDateExit(d.toString());
		board.setPass(pass);;
		//입력
		list.add(board);
		//초기화
		txtTitle.setText("");
		txtPassord.setText("");
		comboPublic.setValue(null);
		dateExit.setValue(null);
		txtContent.setText("");
		
	}
	public void handleBtnCancleAction(ActionEvent event) {
		BoardBean bean;
		System.out.println("제목	비번	공개여부	날짜		내용");
		for(int i=0;i<list.size();i++) {
			bean=list.get(i);
			System.out.print(bean.getSubject()+"\t");
			System.out.print(bean.getPass()+"\t");
			System.out.print(bean.getCombo()+"\t");
			System.out.print(bean.getDateExit()+"\t");
			System.out.println(bean.getContent());
			
		}
		Platform.exit();
	}

}
