package net.database.Custommer;

import java.util.Scanner;

public class CustSQLSentence {
	CustSQL cust  = new CustSQL();
	Scanner scan = new Scanner(System.in);
	
	public void insert() throws Exception {
		cust.makeConn();
		System.out.println("이름을 입력하세요 : ");
		String name = scan.next();
		System.out.println("전화번호를 입력하세요 : ");
		String tel = scan.next();
		System.out.println("주소를 입력하세요 : ");
		String addr = scan.next();
		Scanner scan2 = new Scanner(System.in);
		System.out.println("직장를 입력하세요 ('.' 입력시 기입하지 않음.): ");
		String office = null;
		String choice = scan2.next();
		if(choice.equals(".")) {
			System.out.println("입력을 하지 않습니다.");
		}else {
			office = choice;
		}
		System.out.println("생년월일를 입력하세요 (YYYY-MM-DD) : ");
		String birthday=null;
		String choice2 = scan2.next();
		if(choice2.equals(".")) {
			System.out.println("입력을 하지 않습니다.");	
		}else{
			birthday = choice2;
		}
		System.out.println("성별을 입력하세요 (남/여) : ");
		String sex = scan.next();
	//	System.out.println(name+tel+addr+office+birthday+sex); 정상으로 입력되있는지 확인
		
		System.out.println("등록하시겠습니까 ?" );
		String set = scan2.next();
		if(set.equalsIgnoreCase("y")) {
			CustBean bean = new CustBean();
			
			bean.setName(name);
			bean.setTel(tel);
			bean.setAddr(addr);;
			bean.setOffice(office);
			bean.setBirthday(birthday);
			bean.setSex(sex);
			
			cust.custInsert(bean);
			
			System.out.println("입력되었습니다.");
		}
		if(set.equalsIgnoreCase("n")) {
			System.out.println("입력을 취소합니다.");
		}			
	}
	
	public void update() throws Exception {
		
		cust.makeConn();
		System.out.println("수정하려는 고객의 번호를 입력하세요");
		int setGrade = scan.nextInt();
		System.out.println("수정하려는 연락처를 입력하세요");
		String updateTel = scan.next();
		System.out.println("수정하려는 직장명을 입력하세요");
		String updateOffice = scan.next();
		cust.updateCust(updateTel, updateOffice, setGrade);
	}
	public void delete() throws Exception {	//삭제문
		
		cust.makeConn();
		System.out.println("삭제할 고객번호를 입력하세요");
		int setIdx = scan.nextInt();
		int row = cust.deleteCust(setIdx);
		if(row==0) {
			System.out.println("삭제된 데이터가 없습니다.");
		}else {
			System.out.println(row +"개의 튜플이 삭제되었습니다.");
		}
	}
}
