package net.database.mem;

public class MemSQLExam {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		
		MemSQL mem = new MemSQL();
		
		mem.makeConn();
		mem.memSelect();	
		System.out.println("=============================");
	//	mem.insertMem(7, 500, 1000);
	//	mem.memSelect2(3);	
		mem.updateMem(7,500,1200);
		int row = mem.deleteMem(3);
		if(row==0) {
			System.out.println("삭제된 데이터가 없습니다.");
		}else {
			System.out.println(row +"개의 튜플이 삭제되었습니다.");
		}
		mem.memSelect();
			
	}

}
