package net.database.mem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import net.database.util.MySQLConn;

public class MemSQL extends MySQLConn{
	Statement stmt=null;				//1회성이다.
	PreparedStatement pstmt =null;		//한번 생성하면 계속 사용가능
	ResultSet rs = null;				//SELECT 문을 사용할때 결과저장.
	
	public MemSQL() {
		super();
	}
	
	public void memSelect() throws Exception{
		String sql = "select * from salgrade";
		stmt = myConn.createStatement();
		rs = stmt.executeQuery(sql); 		//지정한 쿼리문을 실행하여 결과를 저장
											//select일경우 executeQuery
	
		while(rs.next()) {
			//칼럼 순서대로 값을 얻어올 수 있다.
			int g = rs.getInt(1);
			int l = rs.getInt(2);
			int h = rs.getInt(3);
			System.out.println(g+"\t"+l+"\t"+h);
		}
		rs.close();
		
	}
	public void memSelect2(int grade) throws Exception{
		String sql = "select * from salgrade where grade>? and"+" losal>=?";
		pstmt = myConn.prepareStatement(sql);	//2번째 매서드는 prepareStatement로 바뀐다.
		pstmt.setInt(1,grade);		//(?의 순서번호,값) 
		pstmt.setInt(2,500);
		
		rs = pstmt.executeQuery(); 			//pstmt로 바뀜
	
		while(rs.next()) {
			//칼럼 순서대로 값을 얻어올 수 있다.
			int g = rs.getInt(1);
			int l = rs.getInt(2);
			int h = rs.getInt(3);
			System.out.println(g+"\t"+l+"\t"+h);
		}
		rs.close();
		
	}
	
	public int insertMem(int g, int l, int h) throws Exception{
		String sql = "INSERT INTO salgrade(grade,losal,hisal) VALUES(?,?,?)";
		pstmt= myConn.prepareStatement(sql);
		pstmt.setInt(1,g);
		pstmt.setInt(2,l);
		pstmt.setInt(3,h);
		int row = pstmt.executeUpdate();				//insert일 경우 executeUpdate
		return row;
		
	}
	public void updateMem(int grade, int losal, int hisal) throws Exception{
		String sql = "update salgrade set losal=?,hisal=? where grade=?";
		pstmt = myConn.prepareStatement(sql);
		pstmt.setInt(1, losal);
		pstmt.setInt(2, hisal);
		pstmt.setInt(3, grade);
		pstmt.executeUpdate();
	
	}
	public int deleteMem(int grade) throws Exception{
		String sql="delete from salgrade where grade=?";
		pstmt=myConn.prepareStatement(sql);
		pstmt.setInt(1, grade);
		int row = pstmt.executeUpdate();
		return row;								//삭제된 튜플의 개수를 리턴한다.
	}
	
	@Override
	public void cleanup() throws Exception {
		pstmt.close();
	}

}
