package net.database.util;

import java.sql.Connection;
import java.sql.DriverManager;

//추상클래스로 만들어 의무적으로 상속받게한다.(new 로 생성 불가.)
public abstract class MySQLConn {
	private String myDriver="com.mysql.jdbc.Driver";
	private String myURL = "jdbc:mysql://localhost:3306/jsl30";
	private String myID="root";
	private String myPass="1234";
	protected Connection myConn;
	
	public MySQLConn() {}
	
	public void makeConn() throws Exception{
		Class.forName(myDriver);
		System.out.println("드라이버 로딩 성공");
		myConn = DriverManager.getConnection(myURL, myID, myPass);	
		System.out.println("데이터베이스 로딩 성공");
	}
	//추상메서드가 포함되므로 추상클래스가 되어야한다.
	public abstract void cleanup() throws Exception;
	
	public void takeDown() throws Exception{
		cleanup();
		myConn.close();
	}
		
	
}
