package treeset;

import java.util.*;

public class TreeSetExam_02 {
	public static void main(String[] args) {
		TreeSet set = new TreeSet();
		int[] score = {80,95,50,35,45,65,10,100};
		for(int i =0 ;i<score.length;i++) {
			set.add(score[i]);
		}
		
		System.out.println(set);
		System.out.println("------------------------");
		//�������� �����ڸ� �̾ƿ�
		Iterator iter = set.descendingIterator();
		while(iter.hasNext()) {
			System.out.print(iter.next() +" ");
		}
		System.out.println();
		System.out.println("------------------------");
		NavigableSet<Integer>  nav = set.descendingSet();
		System.out.println(nav);
		System.out.println(nav.first());
		System.out.println(nav.last());
	}
}
