package treeset;

import java.util.*;
//최대 최소 값 
public class TreeSetExam_01 {

	public static void main(String[] args) {
		TreeSet<Integer> tree = new TreeSet<Integer>();
		//TreeSet또한 입력되는 타입을 지정해줄 수 있다.
		tree.add(new Integer(77));
		tree.add(new Integer(50));
		tree.add(new Integer(85));
		tree.add(new Integer(95));
		tree.add(new Integer(75));
		//처음 데이터를 node데이터로 하고 다음들어오는 데이터를 알아서 '정렬'한다.
		//문자열, 숫자등 기본 래퍼클래스에 한해서 자동정렬된다.
		System.out.println(tree);
		
		//최대값(맨 뒤의 값), 최소값(맨 앞의 값) 꺼내오기
		//순서는 없지만 자동 정렬되어있어서 맨 앞과 맨 뒤의 값을 뽑아오는것이다.
		int min = tree.first();		//최소
		int max= tree.last();		//최대
		System.out.println("최소값 : " +min);
		System.out.println("최대값 : " +max);
		
		System.out.println("88과 같은값 : "+ tree.floor(new Integer(88)));
	
		
	}

}
