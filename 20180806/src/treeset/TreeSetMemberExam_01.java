package treeset;
import java.util.*;
class Member implements Comparable<Member>{
	private String name;
	private int age;
	
	public Member() {}
	public Member(String name, int age) {
		this.name = name;
		this.age = age;
	}
	@Override
	public int compareTo(Member m) {
		
		/*
		if(age>m.age)	
		������������ ����.
		*/
		//������������ ����
		if(age<m.age) {
			return -1;
		}else if(age == m.age) {
			return 0;
		}else {
			return 1;
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
}
public class TreeSetMemberExam_01 {

	public static void main(String[] args) {
		TreeSet<Member> tree = new TreeSet<Member>();
		tree.add(new Member("��ö��",34));
		tree.add(new Member("�迵��",45));
		tree.add(new Member("�����",56));
		tree.add(new Member("�����",56));
		tree.add(new Member("�谩��",76));
		
		Iterator iter =tree.iterator();
		
		while(iter.hasNext()) {
			Member m = (Member) iter.next();
			System.out.println(m);
			System.out.println("�̸� : " +m.getName()+"���� : " +m.getAge());
		}
		
		//System.out.println(tree);
		
	}

}
