package treeset;
import java.util.*;
public class TreeSetEx1 {
//String 으로 입력하였을 때 값 찾기
	public static void main(String[] args) {
		TreeSet tree = new TreeSet();
		String from = "b";
		String to = "d";
		tree.add("abc");
		tree.add("alien");
		tree.add("bat");
		tree.add("car");
		tree.add("Car");
		tree.add("disc");
		tree.add("dance");
		tree.add("dZZZZ");
		tree.add("dzzzz");
		tree.add("elephant");
		tree.add("elevator");
		tree.add("fan");
		tree.add("flower");
		//정렬시 askii 값으로 변환하여 정렬되기 때문에 그 점에 유의 해야 한다.
		// A~Z >> a~z 
		System.out.println(tree);
		System.out.println("range search : from " + from+" to "+ to);
		//시작범위는 포함 이지만 끝 범위는 포함되지 않는다.
		System.out.println("result1 : "+ tree.subSet(from, to));
		//끝범위를 포함시키기 위해  끝 범위에 zzz를 붙이면 포함시킬수 있다.
		System.out.println("result2 : "+ tree.subSet(from, to+"zzz"));
		
		

	}

}
