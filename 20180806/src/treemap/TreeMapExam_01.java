package treemap;
import java.util.*;
import java.util.Map.Entry;
public class TreeMapExam_01 {

	public static void main(String[] args) {
		TreeMap<Integer,String> map = new TreeMap<Integer,String>();
		map.put(new Integer(77),"��ö��");
		map.put(new Integer(66),"�迵��");
		map.put(new Integer(55),"�谩��");
		map.put(new Integer(44),"��ȫ��");
		map.put(new Integer(33),"�迵��");
		
		System.out.println(map);
		//Map.Entry�� ���� Ű�� ���� ������ �� �ִ�.
		Map.Entry<Integer,String> entry = null;
		//		    key	 , value
		entry = map.firstEntry();
		System.out.println("key : " + entry.getKey() + ", value : "+ 
															entry.getValue());
		entry = map.floorEntry(new Integer(77));
		System.out.println("key : " + entry.getKey() + ", value : "+
															entry.getValue());
		System.out.println("----------------------------");
		//�������� ����
		NavigableMap<Integer , String> descMap = map.descendingMap();
		Set<Map.Entry<Integer, String>> entrySet = descMap.entrySet();
		for(Map.Entry<Integer,String> entry1 : entrySet) {
			
			System.out.println("key : " + entry1.getKey() + ", value : "+entry1.getValue());

		}
		
	}
}
