import java.io.*;
import java.util.*;

public class PropertiesExam {

	public static void main(String[] args) throws Exception{

		Properties properties = new Properties();
		String path = PropertiesExam.class.getResource("database.properties.txt").getPath();		
		properties.load(new FileReader(path));
		String driver = properties.getProperty("driver");
		//key와 value 를 쌍으로 가져온다.
		String url = properties.getProperty("url");
		String username=properties.getProperty("username");
		String password = properties.getProperty("password");
		System.out.println("driver : " + driver);
		System.out.println("url : " + url);
		System.out.println("username : " + username);
		System.out.println("password : " + password);
	}
}
