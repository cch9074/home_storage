import java.io.File;
import java.util.*;

public class NameTag_HashTable {
	
	Scanner scan = new Scanner(System.in);
	String tel;
	String[] info;
	
	static Map<String, String[]> map = new Hashtable<String, String[]>(); 
	
	public void input() throws Exception{
		scan = new Scanner(new File("C:\\insa.txt"));
		while(scan.hasNextLine()) {
			String str = scan.nextLine();
			StringTokenizer st = new StringTokenizer(str, ", ");
			int token = st.countTokens();
			info = new String[token];
			for(int i=0;i<token;i++) {
				info[i]=st.nextToken();
			}
			map. put(info[1],info);
		}	
	}
	public void search() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("검색할 전화번호를 입력하세요");
		String input = scanner.nextLine();
		Set set = map.entrySet();
		Iterator iter = set.iterator();
		System.out.println(input+"에 대한 검색 결과 : " );
		while(iter.hasNext()) {
			Map.Entry e = (Map.Entry)iter.next();
			String search = (String)e.getKey();
			String[] line = search.split("-");
			String check ="";
			for(int i=0;i<line.length;i++) {
				check+=line[i];
			}
			if(check.contains(input)) {	
				if(map.containsKey(search)) {
					System.out.println(Arrays.toString(map.get(search)));
				}else {
				System.out.println("없습니다.");
				}
			}
		}
	}
	
	public static void main(String[] args) throws Exception{
		
		NameTag_HashTable n = new NameTag_HashTable();
		n.input();
		n.search();
	}
}
