
import java.io.File;
import java.util.*;
/*
Set 을 이용해서

명함관리 프로그램

명함 받았을 때 필요한 정보들을~

10명정도 분량을 파일로 만들 것

insa.txt
이름, 전화, 회사명, 직급, 이메일

프로그램 실행 시
이미 저 데이터는 들어와있고

실행하면
검색 : 이름이나, 전화를 입력하면
그 사람의 정보가 조로록 나오는 것


이름또는전화  |  회사직급이메일
이 세트가 필요하겠네...

*/
class People
{
   private String name, tel, com, lv, mail;
   public People() {}
   public People(String name, String tel, String com, String lv, String mail)
   {
      this.name = name;
      this.tel  = tel;
      this.com  = com;
      this.lv   = lv;
      this.mail = mail;      
   }
   
   public String getName() {
      return name;
   }
   public void setName(String name) {
      this.name = name;
   }
   public String getTel() {
      return tel;
   }
   public void setTel(String tel) {
      this.tel = tel;
   }
   public String getCom() {
      return com;
   }
   public void setCom(String com) {
      this.com = com;
   }
   public String getLv() {
      return lv;
   }
   public void setLv(String lv) {
      this.lv = lv;
   }
   public String getMail() {
      return mail;
   }
   public void setMail(String mail) {
      this.mail = mail;
   }
   
   
   
}



public class A
{
   static Set<People> insa = new HashSet<People>();
   static People[] pl; 
   
   void load() throws Exception
   {
      int LineCnt = 0;
      Scanner tmp0 = new Scanner(new File("C:\\30R\\JAVA\\insa.txt"));
      Scanner scan0 = new Scanner(new File("C:\\30R\\JAVA\\insa.txt"));
      
      while(true) // 몇 줄 들어왔나 세는 작업
      {
         try {
         String a = tmp0.nextLine();
         LineCnt++;
         }catch(Exception e) { break; }
      }
      
      pl = new People[LineCnt];
      
      for(int i = 0; i < pl.length; i++)
      {
         String a = scan0.nextLine();
         StringTokenizer st = new StringTokenizer(a, ", ");
         
         if(st.countTokens() != 5)
         {
            System.out.println("입력자료 에러\n자료를 확인하라");
            break;
         }
         while(st.hasMoreTokens())
         {
            pl[i] = new People(st.nextToken(),st.nextToken(),st.nextToken(),st.nextToken(),st.nextToken());
         }
         
         insa.add(pl[i]);
      }
   }
   
   void search()
   {
      Scanner scan = new Scanner(System.in);
      
      System.out.print("찾으시는 분의 이름 또는 전화번호를 입력하세요.\n(전화번호의 경우 010-xxxx-xxxx 의 형태로 입력해주세요.)\n검색 : ");
      String input = scan.next().trim();
      
      Iterator iter = insa.iterator();
      while(iter.hasNext())
      {
         People tmp = (People)iter.next();
         if( input.equals(tmp.getName()) || input.equals(tmp.getTel()) )
         {
            System.out.printf("이름 : %s%n전화 : %s%n회사 : %s%n직급 : %s%n메일 : %s%n%n", tmp.getName(), tmp.getTel(), tmp.getCom(), tmp.getLv(), tmp.getMail());
            break;
         }
      }
      if(iter.hasNext() == false)
      {
         System.out.println("입력하신 이름이나 전화번호의 소유자는 없습니다.");
      }
   }
   
   
   public void doing() throws Exception
   {
      load();
      search();
   }
   
   public static void main(String[] args) throws Exception
   {
      A a1 = new A();
      a1.doing();
   }

}