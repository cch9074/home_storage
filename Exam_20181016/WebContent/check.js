/**
 * 
 */
function check(){
		if(frm.name.value==""){
			alert("이름을 입력하세요");
			frm.name.focus();
			return false;
		}
		if(frm.id.value==""){
			alert("id를 입력하세요");
			frm.id.focus();
			return false;
		}else if(frm.id.value.length<4){
			alert("4자 이상 입력하세요");
			frm.id.focus();
			return false;
		}
		if(frm.age.value==""){
			alert("나이를 입력하세요");
			frm.age.focus();
			return false;
		}else if(isNaN(frm.age.value)){
			alert("숫자만 입력하세요");
			frm.age.focus();
			return false;
		}
	}