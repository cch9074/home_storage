package exam_20181016;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MemberInsert
 */
@WebServlet("/memInsert")
public class MemberInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public MemberInsert() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String id = request.getParameter("id");
		String age = request.getParameter("age");
		int  age2 = Integer.parseInt(age);
		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml><head><title>회원가입 정보입니다.</title></head>");
		out.print("<body>");
		out.print("이름 : "+ name+"<p>");
		out.print("아이디 : "+ id+"<p>");
		out.print("나이 : "+ age2+"<p>");
		
		out.print("</body></httml>");
		out.close();
	}

}
