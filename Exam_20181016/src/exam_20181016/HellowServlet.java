package exam_20181016;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/hello")
public class HellowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/httml;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml><head><title>서블릿 연습</title></head>");
		out.print("<body>");
		out.print("JSP/Servlet Test<br>");
		out.print("한글 연습<br>");
		out.print("</body></httml>");
		out.close();
	}
}
