package exam_20151016;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/meminfo")
public class MemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public MemServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		response.setContentType("text/httml;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml><head><title>jsp에서 정보 받아오기</title></head>");
		out.print("<body>");
		out.println("아이디 : "+ id+"<p>");
		out.println("이름 : "+ name+"<p>");
		out.print("</body></httml>");
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		response.setContentType("text/httml;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml><head><title>jsp에서 정보 받아오기</title></head>");
		out.print("<body>");
		out.println("아이디 : "+ id+"<p>");
		out.println("이름 : "+ name+"<p>");
		out.print("</body></httml>");
		out.close();
	}
}
