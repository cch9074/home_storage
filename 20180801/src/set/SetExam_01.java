package set;
import java.util.*;
//HashSet
public class SetExam_01 {

	public static void main(String[] args) {
		Set set = new HashSet();
		set.add("JSP");			
		set.add("홍길동");
		set.add("JAVA");
		set.add("Spring");
		set.add("JSP");
		System.out.println("size(): "+set.size());
		//동일한 값이 입력되면 입력되지 않는다.
		System.out.println(set);
		//순서대로 출력되지 않는다(순서 x)
		Iterator iter = set.iterator();
		while(iter.hasNext()) {
			String str = (String)iter.next();
			System.out.println(str);
		}
		set.remove("홍길동");
		System.out.println(set);
		set.clear();
		if(set.isEmpty()) {
			System.out.println("데이터가 없습니다.");
		}else {
			System.out.println(set);
		}

	}
}
