package set;
import java.util.*;
public class SetExam_03 {
	public static void main (String[] args) {
		
		Set set = new HashSet();
		while(set.size()<7) {
			int r = (int)(Math.random()*45)+1;
			set.add(r);
		}
		System.out.println(set);
		//sort
		//Collections.sort 는 List에서만 사용가능하므로 
		//List(ArrayList, LinkedList)로 캐스팅한 뒤에 이용한다.
		List list = new LinkedList(set);
		Collections.sort(list);
		System.out.println(list);
	
	}
}
