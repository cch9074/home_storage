package set;
import java.util.*;
class Member{
	private String name;
	private int age;
	
	public Member() {}
	public Member(String name, int age) {
		this.name = name;
		this.age = age;
	}
	//이름, 나이가 같으면 같은 객체로 인식하게함.
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member member = (Member)obj;
			return member.name.equals(this.name) && member.age==this.age;
		}else{
			return false;
		}
	}
	@Override
	//해쉬 코드를 새로 부여해 이름과 나이가 같으면 같은 해쉬코드를 갖게 해줌.
	public int hashCode() {
		return name.hashCode() +age;
	}
	//print로 출력할때 나오는 값을 오버라이딩해서 원하는 값을 출력할수 있다.
	@Override
	public String toString() {
		return name + ":" + age;
	}
}
public class SetExam_02 {

	public static void main(String[] args) {
		Set<Member> set2 = new HashSet<Member>();
		Member m1 = new Member("김철수",22);
		set2.add(m1);
		set2.add(new Member("김영희",22));
		set2.add(new Member("김철수",22));
		System.out.println(set2);
		//주소가 3개 찍히므로 3개가 들어가있다는걸 알수 있다.
		
		Iterator iter = set2.iterator();
		while(iter.hasNext()) {
			Member m2 = (Member)iter.next();
			System.out.println(m2);
		}
	}
}
