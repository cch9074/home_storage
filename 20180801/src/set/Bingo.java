package set;
import java.util.*;
public class Bingo {
//set을 이용하여 난수를 발생시킨 후 빙고판 만들기
	public static void main(String[] args) {
		int[][] bingo = new int[5][5];
		
		Set set = new HashSet();
		
		while(set.size()<25) {
			//int타입으로 넣을 경우 자동으로 정렬되기 때문에 
			String r = (int)(Math.random()*50)+1+ "";
			set.add(r);
		}
		
		Iterator iter = set.iterator();
		
		for(int i=0;i<bingo.length;i++) {
			for(int j=0;j<bingo[i].length;j++) {
				int t = Integer.parseInt((String) iter.next());
				bingo[i][j] = t;
				System.out.printf("%2d ",bingo[i][j]);
			}
			System.out.println();
		}
	}
}
