package list;
import java.util.*;
//리스트의 항목을 정렬하기
public class ArrayList_01 {
	public static void main(String[] agrgs) {
		//클래스를 이용한 인스턴스
		ArrayList list = new ArrayList();
		//인터페이스를 이용한 인스턴스
		//클래스를 이용해 만든것보다 메서드수 가 적다
		List list2 = new ArrayList();
		list2.add("AAAA");
		list2.add("DDDD");
		list2.add("가나다");
		list2.add("1234");
		list2.add("2754");
		list2.add(new Integer(50));
		System.out.println(list2);
		//자동 정렬 기능이 구현된 객체 : String, wrapper, date, file
		//사용자가 구현한 클래스는 정렬할 수 없다.
		//단, List에서만 가능하다.
		Collections.sort(list2);
		System.out.println(list2);
	}
	
}
