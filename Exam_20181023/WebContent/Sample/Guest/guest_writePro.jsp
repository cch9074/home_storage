<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="sample.guest.*" %>
<%

	request.setCharacterEncoding("utf-8");
	
%>
<!-- id 는 객채를 선언할 때 사용하는 변수이름과 같은 역할//
	 class 는 생성하려는 객채클래스 -->
<jsp:useBean id="guest" class ="sample.guest.GuestBean">
	<jsp:setProperty name ="guest" property="*" />

</jsp:useBean>

<% 
	guest.setNo(1);
	guest.setRecnt(0);
	guest.setRegdate("2018-10-26");
	String name = guest.getName();
	String email = guest.getEmail();
	String subject = guest.getSubject();
	String content = guest.getContent();
	String pass = guest.getPass();
	response.sendRedirect("guest_list.jsp");
%>

작성자 : <%= name %><br>
이메일 : <%= email %><br> 
제목 : <%= subject %><br>
내용 : <%= content %><br>
비밀번호 : <%= pass %><br> 
