<%@ page import="java.sql.*,java.util.*,sample.schedule.*, sample.util.*" contentType="text/html; charset=UTF-8" %>
<jsp:useBean id="MyBean" class="sample.schedule.ScheduleQuery" scope="page" />
<%
	String title=request.getParameter("title");	
	String year=request.getParameter("start_year");	
	String month=request.getParameter("start_month");	
	String day = request.getParameter("start_day");	
	String date = year+month+day;//연, 월, 일 결합
	String content=request.getParameter("content");	

	MyBean.makeConn();//DB연결

	ScheduleModel sModel = new ScheduleModel();
	sModel.title = title;
	sModel.date = date;
	sModel.content = content;

	int row = MyBean.insertSchedule(sModel);
%>
<html>
<head>
<script language="Javascript">
function alrim(){
	alert("성공적으로 등록하였습니다.")
	//스케줄이 입력된 날짜와 월을 가지고 월별 스케줄을 달력 형식으로 보여주는 schedule_month.jsp
	location.href="schedule_month.jsp?start_year=<%=year%>&start_month=<%=month%>&opt=0";
}
</script>
</head>
<body onload="alrim();">
</body>
</html>
