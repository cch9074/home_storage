<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	String name = request.getParameter("name");				//이름
    String jumin1 = request.getParameter("jumin1");			//주민1
    String jumin2 = request.getParameter("jumin2");			//주민2
    String userid = request.getParameter("userid");			//아이디
    String passwd = request.getParameter("passwd");			//비번
    String repasswd = request.getParameter("repasswd");		//비번확인
    String addr_gubun = request.getParameter("addr_gubun");	//근무지
    String zip1 = request.getParameter("zip1");				//우편번호(앞)
    String zip2 = request.getParameter("zip2");				//우편번호(뒤)
    String addr1 = request.getParameter("addr1");			//주소	
    String addr2 = request.getParameter("addr2");			//주소(나머지)
    String tel1 = request.getParameter("tel1");				//전화번호(앞)
    String tel2 = request.getParameter("tel2");				//전화번호(가운대)
    String tel3 = request.getParameter("tel3");				//전화번호(뒤)
    
    String email = request.getParameter("email");			//이메일
    String fa[] = request.getParameterValues("fa");			//취미(관심분야)
    String job = request.getParameter("job");				//직업
    String intro = request.getParameter("intro");	 		//자기소개
%>

	작성자 : <%= name %><br>
	주민번호 : <%= jumin1 +"-"+ jumin2%><br>
	아이디 : <%= userid %><br>
	비밀번호 :<%=passwd %> 
	<br>
	근무지 : 
	<%
		if(addr_gubun.equals("1")){
			out.println("직장");	    	
	    }else{
	    	out.println("자택");
	    }
	%>
	<br>
	우편번호 : <%= zip1 +"-"+ zip2 %><br>
	주소 : <%= addr1 %><br>
	상세주소 : <%= addr2 %><br>
	전화번호 : <%= tel1 +"-"+tel2+"-"+tel3 %><br>
	이메일 : <%=email %><br>
	취미 : 
	<%
		if(fa!=null){
			for(String selFa : fa ){
				out.print(selFa +" ");
			}
		}
	
	%>
	<br>
	직업 : 
	<% 
		if(job.equals("0")){
			out.println("없음");
		}else{
		out.println(job);
		}
	%>
	<br>
	자기소개 : <%= intro %>
	<br>
	
	<%
	//페이지 이동
	
	//out.print(userid);
	int row = 1;
	if(row==1){
	%>
		<script>
			alert("환영합니다 \n회원가입이 완료되었습니다.")
			<% 
			//request.setAttribute(userid, "userid");
			%>
			location.href="userlogin_form.jsp?userid=<%= userid%>";
		</script>
		<!-- 
		<jsp:forward page ="aaa.jsp" />
		 -->
	<%
	}else{
	%>
		<script>
			alert("죄송합니다 \n다음에 다시 가입해주세요")
			histroy.go(-1)
		</script>
	<% 
	}
	%>
	
	
	
