<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="sample.board.*" %>
<%
	request.setCharacterEncoding("utf-8");
%>
<jsp:useBean id="board" class ="sample.board.BoardBean">
<jsp:setProperty name ="board" property="*" />

</jsp:useBean>
<% 
	board.setNo(1);
	board.setReadcnt(0);
	board.setRegdate("2018-10-26");
	
	int no = board.getNo();
	int readcnt = board.getReadcnt();
	String regdate = board.getRegdate();

	String name = board.getName();
	String email = board.getEmail();
	String subject = board.getSubject();
	String contents = board.getContents();
	String pass = board.getPass();
	//out.print(name);				//받은 데이터를 확인
	
	//DB에 저장.
	//response.
	//response.sendRedirect("board_list.jsp");
	%>
<%-- 
	foward방식(데이타가 뭐가 들어갔는지 알수 없다.)
	request.setAttribute("name", name);
	request.setAttribute("email", email);
	request.setAttribute("subject", subject);
	request.setAttribute("contents", contents);
	request.setAttribute("pass", pass);
	RequestDispatcher d = request.getRequestDispatcher("board_list.jsp");
	d.forward(request, response);
--%>
	

<%--
자바스크립트 중간에 바로 넣을 수 있다.
<jsp:forward page="aaa.jsp" />
--%>

번호 : <%= no %><br>
조회수 : <%= readcnt %><br>
날짜 : <%= regdate %><br>
작성자 : <%= name %><br>
이메일 : <%= email %><br> 
제목 : <%= subject %><br>
내용 : <%= contents %><br>
비밀번호 : <%= pass %><br>

