package controller.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.beans.property.SimpleStringProperty;

public class RootController implements Initializable{
	@FXML ListView<String> listView;
	@FXML TableView<Phone> tableView;
	@FXML ImageView imageView;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		listView.setItems(FXCollections.observableArrayList(
				"繹熱","艙熱","檜熱","艙��","紫��","鼻熱","團熱"));
		listView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue,Number newValue) {
				tableView.getSelectionModel().select(newValue.intValue());
				tableView.scrollTo(newValue.intValue());
				
			}
		});
		ObservableList phoneList = FXCollections.observableArrayList(
				new Phone("繹熱","Phone01.png"),
				new Phone("艙熱","Phone02.png"),
				new Phone("檜熱","Phone03.png"),
				new Phone("艙��","Phone04.png"),
				new Phone("紫��","Phone05.png"),
				new Phone("鼻熱","Phone06.png"),
				new Phone("團熱","Phone07.png")
			);
		TableColumn tcSmartPhone =  tableView.getColumns().get(0);
		tcSmartPhone.setCellValueFactory(new PropertyValueFactory("smartPhone"));
		
		
		TableColumn tcImage = tableView.getColumns().get(1);
		tcImage.setCellValueFactory(new PropertyValueFactory("Image"));

		
		tableView.setItems(phoneList);
		tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Phone>(){
			
			@Override
			public void changed(ObservableValue<? extends Phone> observable,
					Phone oldValue, Phone newValue) {
				if(newValue!=null) {
					imageView.setImage(new Image(getClass().getResource("images/"+newValue.getImage()).toString()));
					
				}
			}
		});
	}
}
