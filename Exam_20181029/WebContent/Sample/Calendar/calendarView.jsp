<%@ page contentType="text/html;charset=UTF-8" import ="java.util.*,sample.util.*,sample.calendar.*,sample.member.*"%>
<jsp:useBean id="MyBean" class="sample.calendar.calendarQuery" scope="page" />

<HTML>
<HEAD>
<TITLE>:: 운영달력 ::</TITLE>
</HEAD>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="script/calendarView.js">
</script>
<%
	String db = request.getParameter("db");
	int num = Integer.parseInt(request.getParameter("num"));
	MyBean.makeConn();
	calendarModel calendarV = MyBean.sel(db,num);
	MyBean.takeDown();
	int rate=0;
	//관리자인자 일반 회원인지 구분하기 위한 세션 검사
//	if(session.getAttribute("younglogin") != null){ //세션이 있으면(로그온 한 경우)
//		MyMember.makeConn(); //회원관리 DB연결
		//rate 메소드를 호출하여 관리자 등급 확인
//		if(MyMember.rate("young_member",session.getAttribute("younglogin").toString())==1)
//			rate=1;
//		MyMember.takeDown();
//	}
%>
<BODY  onload="calendarfrm.subject.select();" leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>
<table width="400" border="1" cellpadding="0" cellspacing="0" align="center" class="grayfont">
 <tr> 
    <td height="56" align="center">운영달력보기</td>
  </tr> 
</table>
<table width="400" border="1" cellpadding="0" cellspacing="0" align="center" class="grayfont" style="padding:3;">
<tr>
	<td align="center" width="20%">date</td>
	<td width="80%"><%=calendarV.date%></td>
</tr>
<tr>
	<td align="center">subject</td>
	<td><%=calendarV.subject%></td>
</tr>
<tr>
	<td  height="167" colspan="2" valign=top style="padding-top:10;  padding-bottom:10; padding-right:20;  padding-left:20; line-height:140%;"><p><%=SqlString.lineBreak(calendarV.contents)%></p></td>
</tr>
<tr>
      <td align="right" colspan="2"><br>
<%if(rate==1){     //관리자일 경우 수정 또는 삭제 가능
%>
			<a href="calendarUpdate.jsp?db=<%=db%>&num=<%=num%>" onfocus="this.blur()" class="linkover">
			<img src="./images/mod.gif" width="53" height="18" border="0"></a>&nbsp;&nbsp;&nbsp;
			<a href="calendarDel.jsp?db=<%=db%>&num=<%=calendarV.num%>&date=<%=calendarV.date%>" onfocus="this.blur()" class="linkover" onclick="return calendarDel();">
			<img src="./images/del.gif" width="49" height="18" border="0"></a>&nbsp;&nbsp;&nbsp;
<%
			}  //관리자가 아닐 경우 수정 및 삭제 불가능
%>
			<a href="javascript:window.close()" onfocus="this.blur()" class="linkover"><img src="./images/check_close.gif" width="63" height="22" border="0"></a></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>
</BODY>
</HTML>

