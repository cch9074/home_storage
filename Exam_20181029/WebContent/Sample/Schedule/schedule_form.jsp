<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
<STYLE TYPE="text/css">
  TD {font-family:"돋움", "Verdana";font-size:9pt}
  BODY {font-family: "돋움", "Verdana"; font-size: 9pt}
  a    { font-family: 돋움, Verdana; color: #000000; text-decoration: none}
  a:hover { font-family:돋움; text-decoration:underline }
</STYLE>
<SCRIPT LANGUAGE="javascript">
<!--
function in_check()
{
	/* 입력값을 검사한다. */
	if (write_form.title.value == "")
	{
		write_form.title.focus();
		alert("제목을 입력하세요.");
		return;
	}
	if (write_form.content.value == "")
	{
		write_form.content.focus();
		alert("내용을 입력하세요.");
		return;
	}
	document.write_form.submit();
}

function Today()
{	//날자 설정 함수
<!--
	var now = new Date ();
	month = now.getMonth ()+1 ;//월
 	year = now.getYear (); //연도
	date = now.getDate();  //일
	
	var start_year="";
	start_year =year - 2004;
	write_form.start_year.options[start_year].selected = true; 
	
	var start_month="";
	start_month = month - 1;
	write_form.start_month.options[start_month].selected = true; 
	
	var start_day="";
	start_day = date - 1;
	write_form.start_day.options[start_day].selected = true; 
	
}
//-->
</SCRIPT>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" onload="Today();">
<form name="write_form" method="post" action="schedule_form_pro.jsp">
<table border=0 width="640" cellpadding="0" cellspacing="0">
	<tr>
    	<td width="620" background="./img/c_top01.gif" height=50 border="0"
          style="BACKGROUND-REPEAT: no-repeat;">
        	<SCRIPT LANGUAGE="javascript">
			<!--
				/*사용자의 컴퓨터에서 현재의 연도와 달을 알아내서[캘린도 보기]를 클릭했을 때
				  올해의 달력이 나오게 만드는 자바 스크립트, [캘린더 보기] 버튼 이미지에 링크를 건다*/
				var now = new Date ();
				month = now.getMonth ()+1 ;
				year = now.getYear ();
				date = now.getDate();
				if(month <10 ) month = "0" + month;
				if(date <10 ) date = "0" + date ;
				year = ( year < 100 ) ? year + 1900 : year;
				document.write("<a href='schedule_month.jsp?opt=0&start_year="+year+"&start_month="+month+"'>");
				document.write("<img src='./img/cb_bt13.gif' name=img5 align=right vspace=3 hspace=3 border=0>");
				document.write("</a>");
			//-->
			</SCRIPT>
		</td>
	</tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<br>
			<table border="0" width="550" cellpadding="0" cellspacing="0" align=center>
				<tr bgcolor="#aad3c9"> 
					<td colspan=2 background="./img/cb_pt02.gif" align="center">
						<img src="./img/cb_left02.gif" vspace="0" hspace="0" align="left">
						<img src="./img/cb_right02.gif" align="right" vspace="0" hspace="0">
					</td>
				</tr>
					<table border="1" width="550" cellpadding="0" cellspacing="1" align="center" bgcolor="#000000">
						<td colspan=2 bgcolor="#faf4ee" align="left">
							<ul>	
							<br>
							<img src="./img/cb_bl01.gif" border=0 vspace="2" hspace="0"> 제&nbsp;&nbsp;&nbsp;&nbsp; 목 :
							<input name="title" size=20 style="border:1 solid #f7d9ad">
							<br>
							<img src="./img/cb_bl01.gif" border=0  vspace="2" hspace="0"> 날&nbsp;&nbsp;&nbsp;&nbsp; 짜  : 
							<select name="start_year">
								<option value=2004>2004년
								<option value=2005>2005년
								<option value=2006>2006년
								<option value=2007>2007년
								<option value=2008>2008년
								<option value=2009>2009년
								<option value=2010>2010년
							</select>&nbsp;&nbsp;
 							<select name="start_month">
 								<option value=01>1월
								<option value=02>2월
								<option value=03>3월
								<option value=04>4월
								<option value=05>5월
								<option value=06>6월
								<option value=07>7월
								<option value=08>8월
								<option value=09>9월
							<!--자바스크립트를 이용하여 10월 부터 12월 표시하는 방법 -->
								<script language="javascript">
								<!--
									var i
									for(i=10;i<=12;i++){
										document.write("<option value="+i+">"+i+"월")
									}
								//-->
								</script>
							</select>&nbsp;&nbsp;
 							<select name="start_day">
 								<option value=01>1일
								<option value=02>2일
								<option value=03>3일
								<option value=04>4일
								<option value=05>5일
								<option value=06>6일
								<option value=07>7일
								<option value=08>8일
								<option value=09>9일
							<!--자바스크립트를 이용하여 10일 부터 31일 표시하는 방법 -->
								<script language="javascript">
								<!--
									var i
									for(i=10;i<=31;i++){
									document.write("<option value="+i+">"+i+"일")
									}
								//-->
								</script>
							</select>
							<br>
    	
    						<img src="./img/cb_bl01.gif" border=0  vspace="2" hspace="0"> 
    						내용 : <br><textarea name="content" rows="15" cols="45" style="border:1 solid #f7d9ad" style="width:90%"></textarea>
  	
  						</td>
					</tr>
				</table>   
				<table border="0" width="550" cellpadding="0" cellspacing="0" align=center>
					<tr>
						<td border=0 align=center width=5 valign=middle colspan=2 background="./img/cb_pt03.gif" cellpadding="0" cellspacing="0">
							<img src="./img/cb_left03.gif" align="left" vspace="0" hspace="0">
						</td>
						<td background="./img/cb_pt03.gif" align=center valign=bottom>
  							<a href="javascript:in_check();">
  							<img src="./img/cb_bt06.gif" name=img2 vspace="0" hspace="0" border="0"></a>
							<a href="javascript:history.back()">
  							<img src="./img/cb_bt07.gif" name=img1 vspace="0" hspace="0" border="0"></a>
						</td>
						<td width=5 ><img src="./img/cb_right03.gif" align="right" vspace="0" hspace="0">
						</td>
					</tr>
				</table>    
		</td>
	</tr>
</table>
</form>
</body>
</html>
