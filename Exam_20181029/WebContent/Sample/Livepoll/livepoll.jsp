<%@ page contentType="text/html;charset=UTF-8"
         import="java.util.*, sample.util.*, sample.quest.*" %>
<jsp:useBean id="MyBean" class="sample.quest.qQuery" scope="page" />

<HTML>
<HEAD><TITLE> 설문조사 참여 </TITLE></HEAD>
<SCRIPT LANGUAGE="javascript">
  function check(poll) {
    for(var i = 0 ; i < poll.reply.length; i++ ) {
      if (poll.reply[i].checked == true) {
        poll.submit();
      }
    }
  }

</SCRIPT>
</head>
<%
	MyBean.makeConn();
	//가장최근의 설문지 번호를 받아 설문내용을 출력
	int num = MyBean.MaxBoard();
	Vector vctBoard = MyBean.listBoard(num);
	qModel inModel = new qModel();

	inModel=(qModel)vctBoard.elementAt(0);
%>
<body bgcolor="#ffffef">
<table border="0" width="100%">
  <tr>
    <td height="25" bgcolor="#6FA0E">
    <p align="center"><font face="돋움" size="2" color="white"><b>Live Poll</b></font></p></td></tr>
  <tr>
    <td><font face="굴림" style="font-size:10pt;">
    <img src="img/bullet-03.gif"> <%=inModel.question%></font></td></tr>
  <tr>
    <td>
    <form name="poll" action="livepoll_ok.jsp?idx=<%=inModel.id%>" method="post">    
	  <input type="radio" name="reply" value="1" ><%=inModel.reply1%><br>
	  <input type="radio" name="reply" value="2" ><%=inModel.reply2%><br>
	  <input type="radio" name="reply" value="3" ><%=inModel.reply3%><br>
	  <input type="radio" name="reply" value="4" ><%=inModel.reply4%><br>

      <input type="image" src="img/vote.gif">
      &nbsp;&nbsp;<input type="image" src="img/vote_view.gif" onClick="livepoll_Result.jsp?idx=<%=inModel.id%>" >
    </form></td>
  </tr>
</table>
<%
   MyBean.takeDown();
%>

</body> 
</html>
