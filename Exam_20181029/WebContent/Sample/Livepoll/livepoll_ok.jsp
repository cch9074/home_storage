<%@ page contentType="text/html;charset=UTF-8"
         import="java.util.*, sample.util.*, sample.quest.*" %>
<jsp:useBean id="MyBean" class="sample.quest.qQuery" scope="page" />

<HTML>
<HEAD><TITLE> 설문조사 참여 처리 </TITLE></HEAD>

<% 
	int id=Integer.parseInt(request.getParameter("idx"));
	String attr ="replynum"+request.getParameter("reply");
    //투표를 한적이 있는지를 쿠키를 이용하여 검사
	boolean found =false;
	Cookie info = null;
	Cookie[] cookies = request.getCookies();

	for(int i=0;i<cookies.length;i++){
		info=cookies[i];
		if(info.getName().equals("pollCookie"+id)){
			found=true;
			break;
		}
	}//for문의 끝

	String newValue=""+System.currentTimeMillis();

	if(found) {  //쿠키가 있으면(투표한 적인 있다면) 방문자가 한 투표는 무시하고 결과 페이지만 보여줍니다)
		response.sendRedirect("livepoll_Result.jsp?idx=" +id);

	}else{

		info=new Cookie("pollCookie" + id,newValue); //쿠키 생성
		response.addCookie(info); //생성된 쿠키를 넘김
  		MyBean.makeConn();
		int row = MyBean.updateBoard(id,attr);

        //투표가 정상적으로 처리되면 원하는 페이지로 이동하도록 링크한다
	    if(row !=0 ) {
		    MyBean.takeDown();
		    response.sendRedirect("livepoll_Result.jsp?idx=" +id);
    
	    }
	}
%>
</html>
