<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<jsp:useBean id="userinfo" class ="sample.userinfo.UserinfoBean">
<jsp:setProperty name ="userinfo" property="*" />

</jsp:useBean>
<%
	String name = userinfo.getName();
	String userid= userinfo.getEmail();
	String passwd = userinfo.getPasswd();
	String zip = userinfo.getZip();
	String addr1 = userinfo.getAddr1();
	String addr2 = userinfo.getAddr2();
	String tel = userinfo.getTel();
	String email = userinfo.getEmail();
	String[] fa = userinfo.getFa();
	String job = userinfo.getJob();
	String intro = userinfo.getIntro();
	
%>
이름 : <%= name %><br>
아이디 : <%= userid %><br>
비번 : <%= passwd %><br>
우편번호 : <%= zip %><br>
주소 : <%= addr1 %><br>
상세주소 : <%= addr2 %><br>
전화번호 : <%= tel %><br>
이메일 : <%= email %><br>
취미 : 
<%
	if(fa!=null){
		for(String fa_list : fa){
			out.println(fa_list +"");
		}
	}
%><br>
직업 : <%= job %><br>
자기소개 : <%= intro %><br>
