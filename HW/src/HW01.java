import java.util.*;
public class HW01 {
	//작성자 : 최한빈
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		
		System.out.println("알파벳을 입력하세요.>(소문자)");//97 ~ 122
		
		String tmp = scan.nextLine();
		
		char ch = tmp.charAt(0);				//char은 스캔으로 직접 입력받을수 없으므로 
												//입력한 문자열(소문자 1글자)에서 첫번째 문자를 뽑아서 쓴다.		
		int t = ch-'a';							// 입력한 문자가 몇번째 알파벳인지 구한다.>> (1<=t && t<=26)
		
		for(int j = 0; j<=t ;j++) {				// 알파벳 순서(t)만큼 반복한다.
			for(char i = 'a'; i<=ch-j; i++) {	// t번 반복하는데 a부터 알파벳 ch-j(0<=j<=t)까지 출력을 반복 하므로 반복 회차에 따라 1자씩 줄여나간다.
												
				System.out.print(i);			// t번 반복하면'a' 만 남게된다.
				
			}
		System.out.println();
		}

	}
}