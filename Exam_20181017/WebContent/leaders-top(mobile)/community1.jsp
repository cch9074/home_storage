<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>강좌안내 | 리더스코리아</title>
	
	<!-- core CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <script>
    	function check(){
    		if(document.my.edu.value==""){
    			alert("이름을 입력하세요");
    			document.my.edu.focus();
    			return false
    		}
    		if(document.my.title.value==""){
    			alert("제목을 입력하세요");
    			document.my.title.focus();
    			return false
    		}
    		if(document.my.cont.value==""){
    			alert("내용을 입력하세요");
    			document.my.cont.focus();
    			return false
    		}
    	
    	}
    </script>
</head><!--/head-->

<body>

    <header id="header">
        
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo_pc.jpg" alt="리더스코리아입시회사"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
						<li><a href="about-us.html">회사소개</a></li>
                        <li><a href="jobarea.html">직업탐구영역</a></li>
                        <li><a href="lecture.html">강좌안내</a></li>
                        <li class="active"><a href="community.html">커뮤니티</a></li>
                        <li><a href="textbook.html">교재안내</a></li> 
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->

    <section id="about-us">
		<div class="about-title">
			<div class="container">
						
				<!-- Our Skill -->
				

					<div class="wow fadeInDown sub">
						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<h2>공지사항Q&A</h2>
							</div>
							<div class="col-xs-6 col-sm-9 text-right">
								<a href="#">HOME</a> > <a href="#">커뮤니티</a> > <a href="#">글쓰기</a>
							</div>
						</div>
						<p class="lead clearfix">EBS 유형 완전분석, 수능점수를 마구마구 올려드립니다. 결제관련 문의는 온라인고객지원센터 02) 555-4287 으로 연란주시면 신속하게 처리하겠습니다</p>
					</div>
					
					<div class="line col-sm-12 "></div>
					
					<div class="row  wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
						
						

						
							
								<div class="sub1-contents">
									
									<div class="board_list_wrap">
										<div class="news-table">
										<form class="form-horizontal" name="my" method="post" action="../boardwrite">
											
											 <div class="form-group">
												<label for="sel" class="col-sm-2 control-label">선택</label>
												<div class="col-sm-10" id="sel" name="sel">
													<select class="form-control">
													  <option>질문선택</option>
													  <option value="n">공지사항</option>
													  <option value="q">일반질문</option>
													</select>
												</div>
											  </div>
											  
											  <div class="form-group">
												<label for="edu" class="col-sm-2 control-label">글쓴이</label>
												<div class="col-sm-10">
												  <input class="form-control" id="edu" placeholder="이름입력" type="text" name="edu">
												</div>
											  </div>
											  
											  <div class="form-group">
												<label for="title" class="col-sm-2 control-label">제목</label>
												<div class="col-sm-10">
												  <input class="form-control" id="title" placeholder="제목입력" type="text" name="title">
												</div>
											  </div>
											 
											  <div class="form-group">
												<label for="content" class="col-sm-2 control-label">내용</label>
												<div class="col-sm-10">
												  <textarea class="form-control" rows="10" id="content" name="cont"></textarea>
												</div>
											  </div>
											  
											   
																
											<div class="writer pull-right">
												<input type ="submit" value="저장" onClick="return check()">
												<input type ="reset" value="뒤로가기">
											</div>
										</form>
										</div>
									</div>	
								</div>
							</div>
							

			</div><!--/.container-->
		</div>
    

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="col-sm-12">
				<p>서울특별시 강남구 압구정동 1430-45 동아B/D 6층 | 상호명 : 리더스코리아 입시회사(주) | 관리자 :허기애 | 정보관리책임자 : 허기애 | 전화 : 02)555-4287 | FAX : 02)555-0103 | 통신판매번호 : 제 강남-10656호</p>
            </div>
            <div class="col-sm-12">
				<p>Copyright 2016 Leaders-Korea.co.kr All Right Reserved.</p>
            </div>
        </div>
    </footer><!--/#footer-->
    

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>