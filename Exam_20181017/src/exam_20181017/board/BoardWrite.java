package exam_20181017.board;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/boardwrite")
public class BoardWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public BoardWrite() {
        super();
        
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String edu = request.getParameter("edu");
		String title = request.getParameter("title");
		String cont = request.getParameter("cont");
		System.out.println("등록되었습니다.");
		//db등록
		//다른페이지로 넘어가는 방법
		response.setContentType("text/html;charset=utf-8");
		response.sendRedirect("leaders-top(mobile)/result.jsp?edu="+edu+"&title="+title+"&cont="+cont);	//get방식
/*		request.setAttribute("edu", edu);
		request.setAttribute("title", title);
		request.setAttribute("cont", cont);
		
		RequestDispatcher dis = 
				request.getRequestDispatcher("leaders-top(mobile)/result.jsp");
		dis.forward(request, response);										//post 방식
*/
		
/*		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<httml><head><title>회원가입 정보입니다.</title></head>");
		out.print("<body>");
		out.print("작성자 : "+ edu+"<p>");
		out.print("제목 : "+ title+"<p>");
		out.print("내용 : "+ cont+"<p>");
		
		out.print("</body></httml>");
		out.close();
*/
	}

}
