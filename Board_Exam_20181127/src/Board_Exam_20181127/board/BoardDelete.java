package Board_Exam_20181127.board;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Board_Exam_20181127.db.BoardDAO;
import Board_Exam_20181127.db.BoardVO;

/**
 * Servlet implementation class BoardDelete
 */
@WebServlet("/delete.do")
public class BoardDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BoardDelete() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		request.setAttribute("bVO", bVO);
		
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_delete.jsp");
		ds.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		int row = bDAO.boardDelete(bVO);
		request.setAttribute("row", row);
				
		//request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("Board/board_delete_pro.jsp");
		ds.forward(request, response);
	//	doGet(request, response);
	}

}
