package Board_Exam_20181127.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class BoardDAO {

	private static BoardDAO instance = new BoardDAO();
	
	public static BoardDAO getInstance() {
		return instance;
	}
		
	public Connection getConnection() throws Exception{
		Connection conn = null;
		Context initContext = new InitialContext();
		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		 DataSource ds = (DataSource)envContext.lookup("jdbc/myoracle");
		conn =  ds.getConnection();
		return conn;
	}
	//리스트 불러오기
	public List boardList() {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BoardVO> list = new ArrayList();
		
		String sql = "select * from tbl_board order by idx desc";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			BoardVO bVO = null;
			while(rs.next()) {
				bVO = new BoardVO();
				bVO.setIdx(rs.getInt("idx"));
				bVO.setName(rs.getString("name"));
				bVO.setEmail(rs.getString("email"));
				bVO.setSubject(rs.getString("subject"));
				bVO.setContents(rs.getString("contents"));
				bVO.setPass(rs.getString("pass"));
				bVO.setRegdate(rs.getTimestamp("regdate"));
				bVO.setReadcnt(rs.getInt("readcnt"));
				list.add(bVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return list;
		}
	}
	
	public List boardList(int listcount,int pageStart) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BoardVO> list = new ArrayList();
		
		String sql = "select x.* from (select * from tbl_board where rownum < ?" 
					+"order by idx desc) x , (select * from tbl_board where rownum < ?" 
					+/*"order by idx asc*/") y  where x.idx = y.idx order by y.idx desc";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, pageStart);
			pstmt.setInt(2, listcount);
			
			rs = pstmt.executeQuery();
			BoardVO bVO = null;
			while(rs.next()) {
				bVO = new BoardVO();
				bVO.setIdx(rs.getInt("idx"));
				bVO.setName(rs.getString("name"));
				bVO.setEmail(rs.getString("email"));
				bVO.setSubject(rs.getString("subject"));
				bVO.setContents(rs.getString("contents"));
				bVO.setPass(rs.getString("pass"));
				bVO.setRegdate(rs.getTimestamp("regdate"));
				bVO.setReadcnt(rs.getInt("readcnt"));
				list.add(bVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return list;
		}
	}
	public List boardList(String s_query, int listcount,int pageStart) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BoardVO> list = new ArrayList();
		
		String sql = "select x.* from (select * from tbl_board where rownum < ? and "+s_query 
					+"order by idx desc) x , (select idx from tbl_board where rownum <? and "+s_query 
					+"order by idx asc) y  where x.idx = y.idx order by x.idx desc";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, pageStart+1);
			pstmt.setInt(2, listcount);
			
			rs = pstmt.executeQuery();
			BoardVO bVO = null;
			while(rs.next()) {
				bVO = new BoardVO();
				bVO.setIdx(rs.getInt("idx"));
				bVO.setName(rs.getString("name"));
				bVO.setEmail(rs.getString("email"));
				bVO.setSubject(rs.getString("subject"));
				bVO.setContents(rs.getString("contents"));
				bVO.setPass(rs.getString("pass"));
				bVO.setRegdate(rs.getTimestamp("regdate"));
				bVO.setReadcnt(rs.getInt("readcnt"));
				list.add(bVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return list;
		}
	}
	
	public int boardCount() {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		
		String sql = "select count(*) from tbl_board";
		int row = 0;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			if(rs.next()) {
				row = rs.getInt("count(*)");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return row;
		}
	}
	
	public int boardCount(String s_query) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		String sql = "";
		int row = 0;
		try {
			conn = getConnection();
			if(!s_query.equals("")) {
				s_query="where " + s_query;
			}
			sql = "select count(*) from tbl_board "+s_query;
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			if(rs.next()) {
				row = rs.getInt("count(*)");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return row;
		}
	}
	
	public int boardInsert(BoardVO bVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "insert into tbl_board(idx,name,email,subject,contents,pass,regdate,readcnt)"
		        + "values(tbl_board_seq.nextval,?,?,?,?,?,?,?)";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bVO.getName());
			pstmt.setString(2, bVO.getEmail());
			pstmt.setString(3, bVO.getSubject());
			pstmt.setString(4, bVO.getContents());
			pstmt.setString(5, bVO.getPass());					
			pstmt.setTimestamp(6, bVO.getRegdate());
			pstmt.setInt(7, 0);
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
		return row;
	}
	public BoardVO boardListNo(int idx) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BoardVO> list = new ArrayList();
		
		String sql = "select * from tbl_board where idx= ?";
		BoardVO bVO = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, idx);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				bVO = new BoardVO();
				bVO.setIdx(rs.getInt("idx"));
				bVO.setName(rs.getString("name"));
				bVO.setEmail(rs.getString("email"));
				bVO.setSubject(rs.getString("subject"));
				bVO.setContents(rs.getString("contents"));
				bVO.setPass(rs.getString("pass"));
				bVO.setRegdate(rs.getTimestamp("regdate"));
				bVO.setReadcnt(rs.getInt("readcnt"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			return bVO;
		}
	
	}
	public void boardCountNo(int idx) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<BoardVO> list = new ArrayList();
		
		String sql = "update tbl_board set readcnt = readcnt +1 where idx= ?";
		BoardVO bVO = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, idx);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			
		}
	
	}
	public int boardEdit(BoardVO bVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "update tbl_board set email = ?,subject =? ,contents=? where idx= ?";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bVO.getEmail());
			pstmt.setString(2, bVO.getSubject());
			pstmt.setString(3, bVO.getContents());	
			pstmt.setInt(4, bVO.getIdx());
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
		return row;
	}
	public int boardDelete(BoardVO bVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "delete from tbl_board where idx= ?";
		
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bVO.getIdx());
			row = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
		return row;
	}
}
