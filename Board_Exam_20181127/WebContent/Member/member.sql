create table member(
	idx number(4)not null,
	name varchar2(20) not null,
	userid varchar2(20) unique,
	//userid varchar2(20) constraint member_userid_uk unique,
	passwd varchar2(20) not null,
	zipcode varchar2(6),
	addr1 varchar2(100),
	addr2 varchar2(100),
	tel varchar2(13) not null,
	email varchar2(50),
	job varchar2(50),
	intro varchar2(2000),
	favorite varchar2(100),
	firstdate date not null,
	lastdate date not null,
	primary key(idx)
	//constraint member_idx_pk primary key(idx)
	--컨트스레인트로 유니크와 프라이머리 키에 이름을 준다.
);
create sequence member_seq;