import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//SQL 과 JAVA 연결하기
public class MyConn {

	public static void main(String[] args) {
		String myDriver = "com.mysql.jdbc.Driver";
		String myURL = "jdbc:mysql://localhost:3306/jsl30";
		String myID="root";
		String myPass="1234";
		Connection myConn;
		Statement stmt;
		ResultSet rs;
		try {
			Class.forName(myDriver);
			System.out.println("드라이버로딩 성공");
		}catch(ClassNotFoundException e) {
			System.out.println("드라이버 로딩 에러");
			
		}
		try{
			myConn = DriverManager.getConnection(myURL, myID, myPass);
			stmt = myConn.createStatement();
			System.out.println("연결 확인");
			String qurey="select count(*) from employee";
			rs = stmt.executeQuery(qurey);
			int cnt =0;
			if(rs.next()) {
				cnt=rs.getInt(1);
			}
			System.out.println("COUNTER :"+cnt);
		}catch(SQLException e) {
			System.out.println("DB연결 실패");
		}
	}
}
