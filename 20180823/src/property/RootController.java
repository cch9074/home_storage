package property;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.text.Font;
//슬라이더의 위치에 따라 글자크기를 줄여줄수 있다.
public class RootController implements Initializable{
	@FXML private Slider slider;
	@FXML private Label label1;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		slider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number>observable,
					Number oldValue, Number newValue) {
				label1.setFont(new Font(newValue.doubleValue()));
			}
		});
	}
}	
