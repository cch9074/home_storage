package control.button;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppMain_ extends Application {
	public void start(Stage stage) throws Exception{
		
		Parent root = (Parent)FXMLLoader.load(getClass().getResource("root.fxml"));
		Scene scene = new Scene(root);
		stage.setTitle("슈미와 난데스까");
		stage.setScene(scene);
		stage.show();
		
	}
	
	public static void main(String[] args) {
		
		launch(args);
	}

}
