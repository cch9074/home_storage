package control.button;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class RootController implements Initializable{
	@FXML private CheckBox chk1;
	@FXML private CheckBox chk2;
	@FXML private Label label1;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	public void checkAction(ActionEvent event) {
		if(chk2.isSelected()) {				//10
			label1.setText("취미 : "+chk2.getText());
			if(chk1.isSelected()) {			//11	
				label1.setText(label1.getText()+chk1.getText());
			}
		}
		else if(chk1.isSelected()) {		//01
			label1.setText("취미 : "+ chk1.getText());
		}
		
		else {								//00
			label1.setText("취미: ");
		}
	}
}	
