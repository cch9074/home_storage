import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class AppMain_03 extends Application{
	
	@Override
	public void start(Stage st) throws Exception{
		//레이아웃의 크기를 설정
		//VBox : 세로, HBox : 가로
		VBox root = new VBox();
		root.setPrefWidth(350);
		root.setPrefHeight(250);
		root.setAlignment(Pos.CENTER);
		//컨트롤러//
		//글상자
		Label label = new Label();
		label.setText("Hellow, JavaFx\n");
		Label label2 = new Label();
		label2.setText("Test");

		label.setFont(new Font(50));
		//버튼 만들기
		Button button = new Button();
		button.setText("확인");
		//클릭시의 동작을 설정
		button.setOnAction(event->Platform.exit());
		//레이아웃에 부착하기
		root.getChildren().add(label);
		root.getChildren().add(button);
		root.getChildren().add(label2);

		//장면 만듦
		Scene scene = new Scene(root);
		
		st.setTitle("AppMain_03 Test");
		//만들어진 장면으로 스테이지에 붙인다.
		st.setScene(scene);
		st.show();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch();
	}

}
