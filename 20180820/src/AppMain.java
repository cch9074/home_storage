import javafx.application.Application;
import javafx.stage.Stage;
//layout 기본틀//
public class AppMain extends Application{
	@Override
	public void start(Stage st) throws Exception{
		//창을 띄움
		st.show();
		
	}
	public static void main(String[] args) {
		//start 메서드를 호출한다.
		//application의 매서드이다.
		launch(args);
		
		
	}

}
