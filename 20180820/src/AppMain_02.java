import javafx.application.Application;
import javafx.stage.Stage;
//init//start//stop을 오버라이딩 할수 있다.
//라이프 사이클의 흐름을 도식화
public class AppMain_02 extends Application {
	
	public AppMain_02(){
		System.out.println(Thread.currentThread().getName()+": AppMain_02() 호출");
	}
	@Override
	public void init() throws Exception{
		System.out.println(Thread.currentThread().getName()+": init 메소드 호출");
	}

	@Override
	public void start(Stage st) throws Exception{

		System.out.println(Thread.currentThread().getName()+": start 메소드 호출");
		st.show();
		
	}
	
	@Override
	public void stop() throws Exception{

		System.out.println(Thread.currentThread().getName()+": stop 메소드 호출");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Thread.currentThread().getName()+":main()메서드 호출");
		launch();

	}

}
