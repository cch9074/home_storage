package JavaFXLayout;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Layout_FXML extends Application{
	@Override
	public void start(Stage stage) throws Exception{
		//FXML이 같은 패키지 내에 있어야한다.
		Parent parent = FXMLLoader.load(getClass().getResource("root.fxml"));
		Scene scene = new Scene(parent);
		
		stage.setTitle("FXML 레이아웃 테스트");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);

	}

}
