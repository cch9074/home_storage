package JavaFXLayout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class LayoutPadding extends Application{
	@Override
	public void start(Stage stage) {
		/*패딩 : 안쪽여백( 버튼 중심)
		HBox root = new HBox();
		root.setPadding((new Insets(50,10,10,50)));
		
		Button button = new Button("확인");
		button.setPrefSize(100, 100);
		*/
		//마진 : 바깥여백(바깥 틀 중심)
		HBox root = new HBox();
		Button button = new Button("확인");
		button.setPrefSize(100, 100);
		root.setMargin(button, new Insets(10,10,50,50));
		root.getChildren().add(button);
		
		Scene scene = new Scene(root);	
		stage.setTitle("패딩 테스트");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch();
	}

}
