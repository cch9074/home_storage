package JavaFXLayout;


import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Layout_Code extends Application{
	@Override
	public void start(Stage stage) throws Exception{
		HBox root = new HBox();
		//간격을 설정(여백을 설정)			top,right,bottom,left
		root.setPadding(new Insets(10,10,10,10));
		//컨트롤러들간의 간격
		root.setSpacing(10);
		
		TextField textfield = new  TextField();
		textfield.setPrefWidth(200);
		
		Button button = new Button();
		button.setText("확인");		
		/*
		root.getChildren().add(textfield);
		root.getChildren().add(button);
		*/
		ObservableList list = root.getChildren();
		list.add(textfield);
		list.add(button);
		Scene scene = new Scene(root);
		
		stage.setTitle("레이아웃 테스트");
		stage.setScene(scene);
		
		stage.show();
	}

	public static void main(String[] args) {
		launch();

	}

}
