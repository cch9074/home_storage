package multi;

import java.awt.Toolkit;

public class BeepPrintExam {

	public static void main(String[] args) {
		Thread thread = new BeepThread();
		//	thread.setName("쓰레드 2");
		//무명클라스
//		클래스를 따로 생성하지 않고도 쓰래드를 사용할수 있다.
//		Thread thread = new Thread() {
//			@Override
//			public void run() {
//				
//				Toolkit toolkit = Toolkit.getDefaultToolkit();
//				
//				for(int i=0;i<10;i++) {
//					toolkit.beep();
//					System.out.println(currentThread());
//					//스피커에 소리를 출력하는 매서드
//					try {
//						Thread.sleep(500);
//					}catch(Exception e) {
//						
//					}
//				}
//			}	
//		};
//		내부클래스에 인터페이스를 상속시켜서 사용할 수 있다.
//		Thread thread2 = new Thread(new Runnable (){
//			@Override
//			public void run() {
//				
//				Toolkit toolkit = Toolkit.getDefaultToolkit();
//				
//				for(int i=0;i<10;i++) {
//					toolkit.beep();
//					//스피커에 소리를 출력하는 매서드
//					try {
//						Thread.sleep(500);
//					}catch(Exception e) {
//					}
//				}
//			}
//		});
		thread.start();
		//thread2.start();
		for(int i=0;i<10;i++) {
			System.out.println("띵");
			
			try {
				Thread.sleep(500);
			}catch(Exception e ){
				
			}
		}
	}
}
