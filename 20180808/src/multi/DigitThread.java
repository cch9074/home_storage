package multi;

public class DigitThread extends Thread{
	
	@Override
	public void run() {
		for(int i=0;i<10;i++) {
			System.out.println(i);
			try {
				Thread.sleep(1000);
				//1초동안 정지(작업 시간을 분배하기 위한 매서드)
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}