package board_20181113.board;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/view.do")
public class BoardView extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public BoardView() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		bDAO.boardCountNo(idx);
		BoardVO bVO = bDAO.boardListNo(idx);
		request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_view.jsp");
		ds.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		bVO.setEmail(email);
		bVO.setSubject(subject);
		bVO.setContents(contents);
				
		bDAO.boardEdit(bVO);
		
		request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_view.jsp?idx="+idx);
		ds.forward(request, response);
		//response.sendRedirect("list.do");
		//doGet(request, response);
	}

}
