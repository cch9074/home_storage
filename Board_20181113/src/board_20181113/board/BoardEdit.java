package board_20181113.board;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/edit.do")
public class BoardEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public BoardEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_modify.jsp");
		ds.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		doGet(request, response);
	}

}
