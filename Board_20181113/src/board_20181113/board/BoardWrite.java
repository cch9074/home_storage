package board_20181113.board;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/write.do")
public class BoardWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public BoardWrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_write.jsp");
		ds.forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		String pass = request.getParameter("pass");
		
		BoardVO bVO = new BoardVO();
		
		bVO.setName(name);
		bVO.setEmail(email);
		bVO.setSubject(subject);
		bVO.setContents(contents);
		bVO.setPass(pass);
		bVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		BoardDAO bDAO = BoardDAO.getInstance();
		if(bDAO.boardInsert(bVO)==1) {
			response.sendRedirect("list.do");	
		};
			
	}

}
