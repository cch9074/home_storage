package singleProject;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Menu1 {
	private SimpleStringProperty cofee;
	private SimpleStringProperty image;
	private SimpleIntegerProperty pay;
	private SimpleIntegerProperty cnt;
	

	
	public Menu1() {
		this.cofee = new SimpleStringProperty();
		this.image = new SimpleStringProperty();
		this.pay = new SimpleIntegerProperty();
		this.cnt = new SimpleIntegerProperty();
		
	}
	public Menu1(String cofee, String image, int pay) {
		this.cofee = new SimpleStringProperty(cofee);
		this.image = new SimpleStringProperty(image);
		this.pay = new SimpleIntegerProperty(pay);
		this.cnt = new SimpleIntegerProperty(0);
	}
	public String getCofee() {
		return cofee.get();
	}
	public void setCofee(String cofee) {
		this.cofee.set(cofee);
	}
	public String getImage() {
		return image.get();
	}
	public void setImage(String image) {
		this.image.set(image);;
	}
	public int getPay() {
		return pay.get();
	}
	public void setPay(int pay) {
		this.pay.set(pay);
	}
	public int getCnt() {
		return cnt.get();
	}
	public void setCnt(int cnt) {
		this.cnt.set(cnt);
	}
	
}
