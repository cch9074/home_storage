package singleProject;

public class CofeeBean {
	private int code;
	private String coname;
	private String copay;
	private int cnt;
	
	public int countM() {
		return cnt--;
	}
	
	public int countP() {
		return cnt++;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public String getCopay() {
		return copay;
	}
	public void setCopay(String copay) {
		this.copay = copay;
	}

}
