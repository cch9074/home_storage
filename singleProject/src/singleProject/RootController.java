package singleProject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class RootController implements Initializable {
	@FXML private ListView<String> menulistView;
	@FXML private ImageView menuImgListView;
	@FXML private TableView<Menu1> menuTableView;
	@FXML private TableView<Menu1> selectTableView;
	private ObservableList<Menu1> selectList;		//선택된 항목을 넣을 리스트
	int total;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
//		menulistView.setItems(FXCollections.observableArrayList(
//				
//				));
//		menulistView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
//			@Override
//			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//				menuTableView.getSelectionModel().select(newValue.intValue());
//				menuTableView.scrollTo(newValue.intValue());
//			}
//		});
		// 메뉴표시 테이블
		ObservableList cofeeList = FXCollections.observableArrayList(
		    new Menu1("아메리카노", "cofee001.png",2000),
		    new Menu1("카페라떼", "cofee002.png",2500),
		    new Menu1("바닐라라떼", "cofee003.png",3000),
		    new Menu1("카페모카", "cofee004.png",3000)
		   
		);
		TableColumn tccofee = menuTableView.getColumns().get(0);
		tccofee.setCellValueFactory(
			new PropertyValueFactory("cofee")
		);
		tccofee.setStyle("-fx-alignment: CENTER;");
		
		TableColumn tcpay = menuTableView.getColumns().get(1);
		tcpay.setCellValueFactory(
			new PropertyValueFactory("pay")
	    );
		tcpay.setStyle("-fx-alignment: CENTER;");
		
		menuTableView.setItems(cofeeList);
		
		menuTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Menu1>() {
			@Override
			public void changed(ObservableValue<? extends Menu1> observable, Menu1 oldValue, Menu1 newValue) {
				if(newValue!=null) {
					menuImgListView.setImage(new Image(getClass().getResource("images/" + newValue.getImage()).toString()));
				
				}
			}
		});
		//선택된 메뉴 테이블
		selectList = FXCollections.observableArrayList();
		TableColumn tcsecofee = selectTableView.getColumns().get(0);
		tcsecofee.setCellValueFactory(
			new PropertyValueFactory("cofee")
		);
		tcsecofee.setStyle("-fx-alignment: CENTER;");
		
		TableColumn tcsepay = selectTableView.getColumns().get(1);
		tcsepay.setCellValueFactory(
			new PropertyValueFactory("pay")
	    );
		tcsepay.setStyle("-fx-alignment: CENTER;");
		
		TableColumn tcsecnt = selectTableView.getColumns().get(2);
		tcsecnt.setCellValueFactory(
			new PropertyValueFactory("cnt")
	    );
		tcsecnt.setStyle("-fx-alignment: CENTER;");
		selectTableView.setItems(selectList);
	}
	public void plusButtonAction(ActionEvent event) {
		Menu1 sel = menuTableView.getSelectionModel().getSelectedItem();
		System.out.println(sel.getCofee());
		selectList.add(sel);
		selectTableView.setItems(selectList);
		for(int i=0;i<selectList.size();i++) {
			if(sel.getCnt()==0){
				System.out.println("새로 들어온 항목입니다 ");
				sel.setCnt(1);
				break;
			}
			else if(i!=0 && selectList.get(i).getCofee().equals(sel.getCofee())) {
				System.out.println("일치한 항목이 있습니다. 갯수를 올립니다.");
				selectList.get(i).setCnt(sel.getCnt()+1);
				System.out.println("일치하는 항목 번호" +i);
				selectList.remove(selectList.size()-1);
				System.out.println("삭제하는 항목 번호" +(i));
				System.out.println(sel.getCnt());
			}
		}
	//테이블뷰로 바꿔서 다시 해보기
					
	}
	public void MButtonAction(ActionEvent event) throws Exception{
		Menu1 sel = selectTableView.getSelectionModel().getSelectedItem();
		System.out.println(sel.getCnt());
		sel.setCnt(sel.getCnt()-1);
		System.out.println(sel.getCnt());
		if(sel.getCnt()==0) {
			selectList.remove(sel);
		}
		System.out.println("삭제되었습니다.");
	}
}
