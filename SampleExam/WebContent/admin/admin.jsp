<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${aVO.rate != 0}">
	
	<c:set var ="set" value="readonly"/>
</c:if>



<html>
<head>
<title>관리자 정보 - 관리자페이지</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
<!--
body,td,th {
   font-size: 12px;
   color: #606060;
}
body {
   margin-left: 0px;
   margin-top: 0px;
}
-->
</style>
<script type="text/javascript">
function check(){
	frm.submit();
}
function id_check(){
	 var urlString;
		//    urlString = "admin/id_check.jsp" ;
		urlString = "check2.do?adminid="+frm.adminid.value ;
		window.open(urlString, "id_check"," resizable=no,x=3000,y=400,width=370,height=250");	
}
</script>
<script type="text/javascript">
function check1(){
	frm.submit();
	
}
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr><td><jsp:include page="top_menu.jsp" flush="true" /></td></tr>
   <tr>
      <td align="center" height="100%" valign=middle><br>
         <table width="30%" border="1" cellspacing="0" cellpadding="3" bgcolor="#FFCC66" bordercolor="#FFFFFF" bordercolorlight="#000000">
            <tr> 
               <td height=40 align="center" style="font-size: 15px;"><b>관리자 [등록][수정]</b>
               </b></td>
            </tr>
         </table><br>
         <table width="60%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              
            <!-- 
            <input type="hidden" name="num" value="${aVO.num}">
             -->
              
               <td>
               <form name="frm" method="post" action="admin.do">
               <c:if test="${aVO.rate == 0}">
               	<table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="DDDDDD">
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자아이디</strong></td>
                        <td bgcolor="ffffff"><input name="adminid" type="text" value="" style="width:150; height:18; padding:2; border:1 solid slategray" size="120" ${set } ><a href="javascript:id_check()">검색</a></td>
                     </tr>
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자비밀번호</strong></td>
                        <td bgcolor="ffffff"><input name="adminpass" type="password" value=""  style="width:150; height:18; padding:2; border:1 solid slategray" size="120"></td>
                     </tr>
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자등급</strong></td>
                        <td bgcolor="ffffff"><select name="rate">
                        <option value="1"<c:if test="${aVO.rate==1}">selected</c:if>>1등급</option>
                        <option value="2"<c:if test="${aVO.rate==2}">selected</c:if>>2등급</option>
                        <option value="3"<c:if test="${aVO.rate==3}">selected</c:if>>3등급</option>
                        <option value="4"<c:if test="${aVO.rate==4}">selected</c:if>>4등급</option>
                        </select>
                        </td>
                     </tr>
                  </table>
                  </c:if>
                  <c:if test="${aVO.rate != 0  }">
                  <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="DDDDDD">
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자아이디</strong></td>
                        <td bgcolor="ffffff"><input name="adminid" type="text" value="${aVO.adminid }" style="width:150; height:18; padding:2; border:1 solid slategray" size="120" readonly></td>
                     </tr>
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자비밀번호</strong></td>
                        <td bgcolor="ffffff"><input name="adminpass" type="password" value=""  style="width:150; height:18; padding:2; border:1 solid slategray" size="120"></td>
                     </tr>
                     <tr>
                        <td width="30%" align="center" bgcolor="EcECEC"><strong>관리자등급</strong></td>
                        <td bgcolor="ffffff"><select name="rate">
                        <option value="1"<c:if test="${aVO.rate==1}">selected</c:if>>1등급</option>
                        <option value="2"<c:if test="${aVO.rate==2}">selected</c:if>>2등급</option>
                        <option value="3"<c:if test="${aVO.rate==3}">selected</c:if>>3등급</option>
                        <option value="4"<c:if test="${aVO.rate==4}">selected</c:if>>4등급</option>
                        </select>
                        </td>
                     </tr>
                  </table>
                  </c:if>
                  </form>
               </td>
              
            </tr>
		
         </table><br>
         <table width="60%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td align=center>
               <c:choose>
               <c:when test="${aVO.rate !=0 }">
               <a href="javascript:check();">[수정]</a>&nbsp;
               </c:when>
               <c:when test="${aVO.rate ==0 }">
               <a href="javascript:check()">[수정]·[등록]</a>
               </c:when>
               </c:choose>
               &nbsp;<a href="notice.do">[취소]</a></td>
            </tr>
         </table>
      
</body>
</html>