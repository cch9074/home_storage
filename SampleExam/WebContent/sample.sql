create table sam_admin(
	num number not null,
	adminid varchar2(20) not null,
	adminpass varchar2(20) not null,
	regdate date,
	rate number(2),
	primary key(num)
)
create sequence sam_admin_num;
insert into sam_admin(num,adminid,adminpass,regdate,rate) values (sam_admin_num.nextval,'admin','1234',sysdate,0);


create table sam_notice(
	num number not null,
	subject varchar2(255) not null,
	contents varchar2(4000) not null,
	regdate date,
	readcnt number,
	primary key(num)
)
create sequence sam_notice_num;