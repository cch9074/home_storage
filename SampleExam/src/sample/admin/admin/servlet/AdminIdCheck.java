package sample.admin.admin.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sample.admin.admin.db.AdminDAO;
import sample.admin.admin.db.AdminVO;

/**
 * Servlet implementation class AdminIdCheck
 */
@WebServlet("/check.do")
public class AdminIdCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public AdminIdCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adminid = request.getParameter("adminid");
		AdminDAO aDAO = AdminDAO.getInstance();
		int row = aDAO.selectAdmin(adminid);
		request.setAttribute("row", row);
		request.setAttribute("adminid", adminid);
		
		RequestDispatcher rd = request.getRequestDispatcher("admin/id_check.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adminid = request.getParameter("adminid");
		AdminDAO aDAO = AdminDAO.getInstance();
		int row = aDAO.selectAdmin(adminid);
		AdminVO aVO = aDAO.setAdmin(adminid);
		request.setAttribute("row", row);
		request.setAttribute("adminid", adminid);
		request.setAttribute("rate", aVO.getRate());
		System.out.println(aVO.getRate());
		RequestDispatcher rd = request.getRequestDispatcher("admin/id_check.jsp");
		rd.forward(request, response);
	}

}
