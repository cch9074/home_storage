package sample.admin.admin.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.admin.admin.db.AdminVO;
import sample.admin.notic.db.NoticeDAO;
import sample.admin.notic.db.NoticeVO;

/**
 * Servlet implementation class AdminNotice
 */
@WebServlet("/notice.do")
public class AdminNotice extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminNotice() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NoticeDAO nDAO = NoticeDAO.getInstance();
		List<NoticeVO> list = nDAO.noticeList();
		request.setAttribute("list", list);
		HttpSession session = request.getSession();
		if(session.getAttribute("loginUser") != null) {
			AdminVO aVO = (AdminVO) session.getAttribute("loginUser");
			request.setAttribute("aVO", aVO);
		}
		
		String url = "/admin/notice_list.jsp";
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
