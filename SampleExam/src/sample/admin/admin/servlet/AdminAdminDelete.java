package sample.admin.admin.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.admin.admin.db.AdminDAO;
import sample.admin.admin.db.AdminVO;

/**
 * Servlet implementation class AdminAdminDelete
 */
@WebServlet("/adminDelete.do")
public class AdminAdminDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminAdminDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		AdminVO aVO = (AdminVO) session.getAttribute("loginUser");
		request.setAttribute("loginUser", aVO);
		String url ="";
		if(aVO.getRate()==0) {
			 url="admin/admin_delete.jsp";
		}else {
			 url="notice.do";
		}
		request.setAttribute("aVO", aVO);
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String adminid= request.getParameter("adminid");
		String adminpass =request.getParameter("adminpass");
		AdminDAO aDAO = AdminDAO.getInstance();
		AdminVO aVO = aDAO.setAdmin(adminid);
		
		if(aVO.getAdminpass().equals(adminpass)) {

			aDAO.deleteAdmin(adminid);	
		}
		HttpSession session = request.getSession();
		
		RequestDispatcher rd = request.getRequestDispatcher("notice.do");
		rd.forward(request, response);
		
		
	}

}
