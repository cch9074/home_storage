package sample.admin.admin.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.admin.admin.db.AdminDAO;
import sample.admin.admin.db.AdminVO;

/**
 * Servlet implementation class AdminIndex
 */
@WebServlet("/index.do")
public class AdminIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminIndex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/admin/index.jsp";
		
		HttpSession session = request.getSession();
		if(session.getAttribute("loginUser")!= null) {
			AdminVO aVO = (AdminVO) session.getAttribute("loginUser");
			request.setAttribute("aVO", aVO);
			url = "notice.do";
		}
		
		
		RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String adminid = request.getParameter("adminid");
		String adminpass = request.getParameter("adminpass");
		String url = "/admin/index.jsp";
		int row = 0;
		AdminDAO aDAO = AdminDAO.getInstance();
		row = aDAO.idCheck(adminid,adminpass);
		
		if(row == 1 ) {
			AdminVO aVO = aDAO.setAdmin(adminid);
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", aVO);
			session.setMaxInactiveInterval(3600);
			
			request.setAttribute("aVO", aVO);
			url = "notice.do";
			
		}else {
			
		}
			RequestDispatcher rd = request.getRequestDispatcher(url);
			rd.forward(request, response);
		
	}

}
