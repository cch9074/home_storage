package sample.admin.admin.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sample.admin.notic.db.NoticeDAO;
import sample.admin.notic.db.NoticeVO;

/**
 * Servlet implementation class AdminWrite
 */
@WebServlet("/write.do")
public class AdminWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminWrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/admin/notice_write.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		NoticeVO nVO = new NoticeVO();
		
		nVO.setSubject(subject);
		nVO.setContents(contents);
		
		Timestamp now = new Timestamp(System.currentTimeMillis());
		nVO.setRegdate(now);
		NoticeDAO nDAO = NoticeDAO.getInstance();
		int row = nDAO.writeNotice(nVO);
		
			

		RequestDispatcher rd = request.getRequestDispatcher("notice.do");
		rd.forward(request, response);
		
	}

}
