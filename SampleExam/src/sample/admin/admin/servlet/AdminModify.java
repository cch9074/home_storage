package sample.admin.admin.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import sample.admin.notic.db.NoticeDAO;
import sample.admin.notic.db.NoticeVO;

/**
 * Servlet implementation class AdminModify
 */
@WebServlet("/modify.do")
public class AdminModify extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminModify() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int num = Integer.parseInt(request.getParameter("num"));
		NoticeDAO nDAO = NoticeDAO.getInstance();
		NoticeVO nVO = nDAO.noticeListNo(num);
		
		request.setAttribute("nVO", nVO);
		
		RequestDispatcher rd = request.getRequestDispatcher("admin/notice_modify.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int num = Integer.parseInt(request.getParameter("num"));
		NoticeDAO nDAO = NoticeDAO.getInstance();
		NoticeVO nVO = nDAO.noticeListNo(num);

		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");

		nVO.setSubject(subject);
		nVO.setContents(contents);
				
		nDAO.updateNotice(nVO);
		
		request.setAttribute("nVO", nVO);
		RequestDispatcher ds = request.getRequestDispatcher("view.do?num="+num);
		ds.forward(request, response);
	}

}
