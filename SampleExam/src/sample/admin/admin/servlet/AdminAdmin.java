package sample.admin.admin.servlet;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sample.admin.admin.db.AdminDAO;
import sample.admin.admin.db.AdminVO;

/**
 * Servlet implementation class AdminAdmin
 */
@WebServlet("/admin.do")
public class AdminAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("loginUser") != null) {
			AdminVO aVO = (AdminVO) session.getAttribute("loginUser");
			request.setAttribute("aVO", aVO);
			
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("admin/admin.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adminid = request.getParameter("adminid");
		String adminpass = request.getParameter("adminpass");
		Timestamp now = new Timestamp(System.currentTimeMillis());
		int rate = Integer.parseInt(request.getParameter("rate"));
		
		AdminDAO aDAO = AdminDAO.getInstance();
		AdminVO aVO = new AdminVO();
		aVO.setAdminid(adminid);
		aVO.setAdminpass(adminpass);
		aVO.setRegdate(now);
		aVO.setRate(rate);
		AdminVO aVOselect = aDAO.setAdmin(adminid);		//아이디 중복검사
		if(aVOselect==null) {		//아이디없을경우 추가
			aDAO.insertAdmin(aVO);
			System.out.println("생성되었습니다.");
		}else {
			aDAO.notifyAdmin(aVO);				//있을경우 정보 변경
			System.out.println("수정되었습니다.");
					
		}
		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		RequestDispatcher rd = request.getRequestDispatcher("notice.do");
		rd.forward(request, response);
	}

}
