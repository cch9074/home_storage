package sample.admin.admin.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import sample.admin.notic.db.NoticeVO;
import sample.util.DBManager;

public class AdminDAO {

private AdminDAO() {}
	
	private static AdminDAO instance = new AdminDAO();
	
	public static AdminDAO getInstance() {
		return instance;
	}
	
	public int idCheck(String adminid, String adminpass) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int row =-1;
		
		String quary = "select adminpass from sam_admin where adminid = '"+adminid+"'";
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				//System.out.println(quary+rs.getString("adminpass"));
				
				if(rs.getString("adminpass") != null && rs.getString("adminpass").equals(adminpass)) {
					row = 1;
					System.out.println("로그인 성공");
				}else {
					row = 0;
				}
			}
		//	System.out.println(row);
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return row;
		}	
		

	}
	public AdminVO setAdmin(String adminid) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AdminVO aVO = new AdminVO();
		String quary = "select * from sam_admin where adminid =?  ";
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			pstmt.setString(1, adminid);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				aVO.setNum(rs.getInt("num"));
				aVO.setAdminid(rs.getString("adminid"));
				aVO.setAdminpass(rs.getString("adminpass"));
				aVO.setRegdate(rs.getTimestamp("regdate"));
				aVO.setRate(rs.getInt("rate"));
			}
		}catch (Exception e ) {
			
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return aVO;
		}
	}
	public int selectAdmin(String adminid) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AdminVO aVO = new AdminVO();
		String quary = "select * from sam_admin where adminid =?  ";
		int row = 0; 
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			pstmt.setString(1, adminid);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				aVO.setNum(rs.getInt("num"));
				aVO.setAdminid(rs.getString("adminid"));
				aVO.setAdminpass(rs.getString("adminpass"));
				aVO.setRegdate(rs.getTimestamp("regdate"));
				aVO.setRate(rs.getInt("rate"));
				row =1;
			}
		}catch (Exception e ) {
			
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt,rs);
			return row;
		}
	}

	public int insertAdmin(AdminVO aVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
				
		String quary = "insert into sam_admin(num,adminid,adminpass,regdate,rate)"
		        + "values(sam_admin_num.nextval,?,?,?,?)";
		
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, aVO.getAdminid());
			pstmt.setString(2, aVO.getAdminpass());
			pstmt.setTimestamp(3, aVO.getRegdate());
			pstmt.setInt(4, aVO.getRate());
			System.out.println(quary);
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
	
	}
	public void notifyAdmin(AdminVO aVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		
		String quary = "insert into sam_admin(num,adminid,adminpass,regdate,rate)"
		        + "values(sam_notice_num.nextval,?,?,?,?)";
		
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, aVO.getAdminid());
			pstmt.setString(2, aVO.getAdminpass());
			pstmt.setTimestamp(3, aVO.getRegdate());
			pstmt.setInt(4, aVO.getRate());
			
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			//return row;
		}
	}
	
	public void deleteAdmin(String adminid) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "delete from sam_admin where adminid= ?";
		System.out.println(quary);
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			pstmt.setString(1, adminid);
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			
		}
	}
}
