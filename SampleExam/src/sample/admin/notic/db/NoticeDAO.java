package sample.admin.notic.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sample.util.DBManager;

public class NoticeDAO {
	private static NoticeDAO instance = new NoticeDAO();
	
	public static NoticeDAO getInstance() {
		return instance;
	}
	public int writeNotice(NoticeVO nVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "insert into sam_notice(num,subject,contents,regdate,readcnt)"
		        + "values(sam_notice_num.nextval,?,?,?,?)";
		System.out.println(quary);
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			pstmt.setString(1, nVO.getSubject());
			pstmt.setString(2, nVO.getContents());
			pstmt.setTimestamp(3, nVO.getRegdate());
			pstmt.setInt(4, 0);
			
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
	
	}
	
	public List noticeList() {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<NoticeVO> list = new ArrayList();
		
		String quary = "select * from sam_notice order by num desc";
		
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			
			
			rs = pstmt.executeQuery();
			NoticeVO nVO = null;
			while(rs.next()) {
				nVO = new NoticeVO();
				nVO.setNum(rs.getInt("num"));
				nVO.setSubject(rs.getString("subject"));
				nVO.setContents(rs.getString("contents"));
				nVO.setRegdate(rs.getTimestamp("regdate"));
				nVO.setReadcnt(rs.getInt("readcnt"));
				list.add(nVO);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				DBManager.close(conn,pstmt,rs);
			}catch (Exception ee) {
				ee.printStackTrace();
			}
		return list;
		}
	}
	
	public NoticeVO noticeListNo(int num) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<NoticeVO> list = new ArrayList();
		
		String sql = "select * from sam_notice where num= ?";
		NoticeVO nVO = null;
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				nVO = new NoticeVO();
				nVO.setNum(rs.getInt("num"));
				nVO.setSubject(rs.getString("subject"));
				nVO.setContents(rs.getString("contents"));
				nVO.setRegdate(rs.getTimestamp("regdate"));
				nVO.setReadcnt(rs.getInt("readcnt"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				DBManager.close(conn,pstmt,rs);
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			return nVO;
		}
	}
	
	public void noticeCountNo(int num) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		
		List<NoticeVO> list = new ArrayList();
		
		String sql = "update sam_notice set readcnt = readcnt +1 where num= ?";
		NoticeVO nVO = null;
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				DBManager.close(conn,pstmt);
			}catch (Exception ee) {
				ee.printStackTrace();
			}	
			
		}
	}
	
	public int updateNotice(NoticeVO nVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "update sam_notice set subject =? ,contents=? where num= ?";
		System.out.println(quary);
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			pstmt.setString(1, nVO.getSubject());
			pstmt.setString(2, nVO.getContents());	
			pstmt.setInt(3, nVO.getNum());
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
	}
	
	public int deleteNotice(int num) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		int row = 0;
		String quary = "delete from sam_notice where num= ?";
		System.out.println(quary);
		try {
			conn =  DBManager.getConnection();
			pstmt = conn.prepareStatement(quary);
			pstmt.setInt(1, num);
			row = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBManager.close(conn,pstmt);
			return row;
		}
	}
	
}
