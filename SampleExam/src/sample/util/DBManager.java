package sample.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBManager {
	
	public static Connection getConnection() {
		Connection conn =null;
		try {
			
		Context initContext = new InitialContext();
		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		DataSource ds = (DataSource)envContext.lookup("jdbc/myoracle");
		conn = ds.getConnection();
		System.out.print("DB연결 성공");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void close(Connection conn, PreparedStatement stmt, ResultSet rs) {
		try {
			rs.close();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("SQL 입력완료");
		}
	}
	
	public static void close(Connection conn, PreparedStatement stmt) {
		try {
			
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("SQL 입력완료");
		}
	}
}

