package exam_20181109.guest;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GuestList
 */
@WebServlet("/list.do")
public class GuestList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public GuestList() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GuestDAO gDAO = GuestDAO.getInstance();
		List<GuestVO> list = gDAO.guestList();
		request.setAttribute("list", list);
		RequestDispatcher ds = request.getRequestDispatcher("Guest/guest_list.jsp");
		ds.forward(request, response);
		//response.sendRedirect("Guest/guest_list.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
