package exam_20181109.guest;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exam_20181109.board.BoardDAO;
import exam_20181109.board.BoardVO;

/**
 * Servlet implementation class GeustWrite
 */
@WebServlet("/write.do")
public class GuestWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GuestWrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("./Guest/guest_write.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String no = request.getParameter("no");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String content = request.getParameter("content");
		
		
		GuestVO gVO = new GuestVO();
		gVO.setPass(no);
		gVO.setName(name);
		gVO.setEmail(email);
		gVO.setSubject(subject);
		gVO.setContent(content);
	
		gVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		GuestDAO gDAO = GuestDAO.getInstance();
		gDAO.guestInsert(gVO);
		response.sendRedirect("Board/board_list.jsp");
		doGet(request, response);
	}

}
