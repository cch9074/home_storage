package exam_20181109.guest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import exam_20181109.board.BoardVO;

public class GuestDAO {
	
	private static  GuestDAO instance = new GuestDAO();
	
	public static GuestDAO getInstance() {
		return instance;
	}
	
	
	public Connection getConnection() throws Exception{
		Connection conn = null;
		Context init = new InitialContext();
		Context evn = (Context) init.lookup("java:/comp/evn");
		DataSource ds = (DataSource)evn.lookup("jdbc/myoracle");
		conn =  ds.getConnection();
		return conn;
				
	}
	public void guestInsert(GuestVO gVO) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "insert into hr_board(no,name,email,subject,content,pass,regdate,readcnt)"
		        + "values(hr_board_seq.nextval,?,?,?,?,?,?,?)";
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, gVO.getName());
			pstmt.setString(2, gVO.getEmail());
			pstmt.setString(3, gVO.getSubject());
			pstmt.setString(4, gVO.getContent());
			pstmt.setString(5, gVO.getPass());
			pstmt.setTimestamp(6, gVO.getRegdate());
			pstmt.setInt(7, 0);
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			}catch (Exception ee) {
				ee.printStackTrace();
			}		
		}
	}
	
	public List guestList() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<GuestVO> list = new ArrayList();
		
		
		String query = "select * from hr guest order by no desc";
		
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				GuestVO guest = null;
				while( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));
					list.add(guest);
				}		
					
				
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return list;
			}
			
		}
	public GuestVO guestListNo(int no) {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<GuestVO> list = new ArrayList();		
		String query = "select * from hr_guest where no= ?";
		GuestVO guest = null;
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				pstmt.setInt(1, no);
				rs = pstmt.executeQuery();
				
				if( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setContent(rs.getString("contents"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));	
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return guest;
			}
			
		}
}