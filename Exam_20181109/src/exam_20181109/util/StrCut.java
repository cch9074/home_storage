package exam_20181109.util;

public class StrCut {
	public StrCut() {}

  public static String cutStr(int strlong,String str) {
		int len = str.length();
		if(len > strlong)
			str = str.substring(0,strlong)+"...";
		return str;
  }

  public static String comma(int jungsu) {
		String str = Integer.toString(jungsu);
		int len = str.length();
		String str2 = "";
		if(len < 3) str2 = str;
		else {
			for(int i=len%3 ; i<=len ; i+=3) {
				int temp = 0;
				if(i==0) i += 3;
				if(i>=3) temp = i - 3;
				str2 += str.substring(temp,i);
				if(i!=len) str2 += ",";
			}
		}
		return str2;
  }

	public static String comma(String str) {
		int len = str.length();
		String str2 = "";
		if(len < 3) str2 = str;
		else {
			for(int i=len%3 ; i<=len ; i+=3) {
				int temp = 0;
				if(i==0) i += 3;
				if(i>=3) temp = i - 3;
				str2 += str.substring(temp,i);
				if(i!=len) str2 += ",";
			}
		}
		return str2;
  }
}