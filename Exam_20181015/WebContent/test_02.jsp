<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>서블릿 테스트</title>
</head>
<body>
<% 	//자바코드를 넣기 위한 자리
	int a = 100;
	int b = 200;
	int c = a + b;
	//out.println(a+"+"+b+"="+c);
	//java 가 아니기 때문에 system 이 안들어간다.
	//<%= > 는 출력하는 일종의 기호
%>

	결과 : <%=a %> + <%=b %>=<%=c %>입니다.
	줄바꿈
</body>
</html>