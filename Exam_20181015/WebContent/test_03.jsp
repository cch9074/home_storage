<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% //모델 1방식

	int cnt =0;
	String str = "내용입니다.";

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<table border="1">
		<tr>
			<td>번호</td><td>제목</td>
		</tr>
		<%	//반복문 같은 자바언어를 사용할 수 있다.
			for (int i=0;i<5;i++){//for문 시작
		%>
	
			<tr>
				<td><%=++cnt %></td><td><%=str %></td>
			</tr>		
		<%
			}	//for문의 끝
		%>
	</table>
</body>
</html>