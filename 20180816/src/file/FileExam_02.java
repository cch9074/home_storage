package file;
import java.io.*;
public class FileExam_02 {
	public static void main(String[] args) {
		//file 생성시 경로를 지정해줘야한다.
		File f = new File("C:\\test\\work");
		if(!f.exists()) {
			f.mkdir();
		}
		try {
			Thread.sleep(3000);
		}catch(InterruptedException e) {}
		
		//file객체로 주소를 지정해줄수 있다.
		File f1 = new File(f, "adc.txt");
		
		if(!f1.exists()) {
			try {
				f1.createNewFile();
			}catch(Exception e) {
				e.getMessage();
			}
		}
		
		try {
			Thread.sleep(3000);
		}catch(InterruptedException e) {}
		
		if(f1.exists()) {
			f1.delete();
		}
	}
}
