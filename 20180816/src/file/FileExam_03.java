package file;
import java.io.*;
public class FileExam_03 {

	public static void main(String[] args) {
		//파일을 불러오거나 생성,삭제를 하기위해 파일을 먼저 찾아준다(경로를 지정해준다).
		File f = new File("C:\\Users\\JSLHRD\\Desktop\\Bin\\20180816\\src\\file");
		if(f.exists()) {
			File[] file = f.listFiles();
			for(int x=0;x<file.length;x++) {
				//확장자 이름을 제한해줄수 있다.
				if(file[x].getName().endsWith(".java"))
				System.out.println((x+1) + ":"+ file[x].getName());
			}
		}else {
			System.out.println("파일이 존재하지 않습니다.");
			
		}	
	}
}
