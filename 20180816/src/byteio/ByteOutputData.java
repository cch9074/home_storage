package byteio;
import java.io.*;
public class ByteOutputData {

	public static void main(String[] args) throws Exception{
		File f = new File("C:\\test\\work\\pridata.txt");
		//버퍼를 이용하면 속도를 빠르게 할 수있다.
		BufferedOutputStream bos = new BufferedOutputStream(
											new FileOutputStream(f));
		DataOutputStream dos = new DataOutputStream(bos);
		//기본자료형을 넣을 수 있다.
		dos.writeInt(20);
		dos.writeInt(20);
		dos.writeInt(20);
		dos.writeInt(20);
		dos.writeInt(20);
		
		dos.writeDouble(3.14);
	//	dos.writeChar(' ');
		dos.writeBytes("ABCDEFG");
		dos.close();
		
	}

}
