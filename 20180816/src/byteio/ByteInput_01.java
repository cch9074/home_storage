package byteio;

import java.io.*;
//파일객체를 생성하여 파일의 내용 불러오기
public class ByteInput_01 {

	public static void main(String[] args)throws IOException {
		//키보드로 입력받기(byte)
		System.out.println("입력 :");
//		int a = System.in.read();
//		System.out.println("a = " + a);
		
	/*	FileInputStream fis = new FileInputStream(FileDescriptor.in);
		
		int b = fis.read();
		System.out.println("b = "+ b);
	*/
		
		File f = new File("C:\\test\\work\\abc.txt");
		FileInputStream fis = new FileInputStream(f);
		byte[] by = new byte[1024];
		//실제 읽은 갯수를 저장
		//읽으면서 배열에 넣는다.
		int count = fis.read(by);
		for(int i=0;i<count;i++) {
			System.out.println( i+1 + "="+ (char)by[i]);
		}
	}

}
