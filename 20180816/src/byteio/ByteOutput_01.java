package byteio;
import java.io.*;
import java.util.Scanner;
//파일객체를 생성하여 실제 파일을 만들고, 파일에 내용넣기
public class ByteOutput_01 {
	public static void main(String[] args) {
		//파일이 있다면 덮어쓰면서 생성한다.
		File f = new File("C:\\test\\work\\abc.txt");
		try {
			FileOutputStream fo = new FileOutputStream(f);
			//입력되는 값을 콘솔에도 출력할수 있다.
			FileOutputStream foc = new FileOutputStream(FileDescriptor.out);
			
			byte[] data = {65,66,68,70,72,(byte)('!')};
	
			fo.write(data);
			foc.write(data);	
			
		}catch (FileNotFoundException ee) {
			System.out.println("파일을 찾을수 없습니다.");
		}catch (IOException e) {
			System.out.println("파일 입출력 에러");
		} 
		System.out.println("작업 끝!");
	}
}