package byteio;
import java.io.*;
public class ByteInputData {
		
	public static void main(String[] args) throws Exception{
		File f = new File("C:\\test\\work\\pridata.txt");
		DataInputStream dis = new DataInputStream(
				new BufferedInputStream(new FileInputStream(f)));
		
		int a = 0;
		int a1 = 0;
		int a2 = 0;
		int a3 = 0;
		int a4 = 0;
		double b  =0;
		byte[] c = null;
	
		a = dis.readInt();
		a1 = dis.readInt();
		a2 = dis.readInt();
		a3 = dis.readInt();
		a4 = dis.readInt();
		
		b = dis.readDouble();
		
		c = new byte[10]; 
		dis.read(c);
		dis.close();
		
		System.out.println("a =" + a);
		System.out.println("a1=" + a1);
		System.out.println("a2=" + a2);
		System.out.println("a3=" + a3);
		System.out.println("a4=" + a4);
		
		System.out.println("b=" + b);
		System.out.println("c=" + new String(c));
	}
}