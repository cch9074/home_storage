package Exam_20181101;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/InsertBoard")
public class InsertBoard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public InsertBoard() {
        super();
      
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	//폼태그를 통해 넘어오므로 post 방식이다.
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BoardBean board = new BoardBean();
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		String pass = request.getParameter("pass");
		
		board.setName(name);
		board.setEmail(email);
		board.setSubject(subject);
		board.setContents(contents);
		board.setPass(pass);
		
		request.setAttribute("board", board);
		
		request.setAttribute("name", name);
		request.setAttribute("email", email);
		request.setAttribute("subject", subject);
		request.setAttribute("contents", contents);
		request.setAttribute("pass", pass);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Board/board_write_pro.jsp");
		dispatcher.forward(request, response);
		
		
	}

}
