var bExplorer4plus = (navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.substring(0,1) >= "4");

function CheckUIElements()
{
        var yMenuFrom, yMenuTo, yButtonFrom, yButtonTo, yOffset, timeoutNextCheck;

        if ( bExplorer4plus ) {  // IE 용 설정
                //yButtonFrom = parseInt (divLinkButton.style.top, 10);
                //yButtonTo   = document.body.scrollTop + document.body.clientHeight - 55;
                yMenuFrom   = parseInt (shop.style.top, 10);
                yMenuTo     = document.body.scrollTop + 65; // 위쪽 위치 91
        }

        timeoutNextCheck = 100;

        if ( Math.abs (yButtonFrom - (yMenuTo + 152)) < 6 && yButtonTo < yButtonFrom ) {
                setTimeout ("CheckUIElements()", timeoutNextCheck);
                return;
        }


        if ( yButtonFrom != yButtonTo ) {
                yOffset = Math.ceil( Math.abs( yButtonTo - yButtonFrom ) / 10 );
                if ( yButtonTo < yButtonFrom )
                        yOffset = -yOffset;

                if ( bExplorer4plus )
                        divLinkButton.style.top = parseInt (divLinkButton.style.top, 10) + yOffset;

                timeoutNextCheck = 10;
        }
        if ( yMenuFrom != yMenuTo ) {
                yOffset = Math.ceil( Math.abs( yMenuTo - yMenuFrom ) / 20 );
                if ( yMenuTo < yMenuFrom )
                        yOffset = -yOffset;

                if ( bExplorer4plus )
                        shop.style.top = parseInt (shop.style.top, 10) + yOffset;

                timeoutNextCheck = 10;
        }

        setTimeout ("CheckUIElements()", timeoutNextCheck);
}

function Slide()
{
        var y;

        // 페에지 로딩시 포지션
        if ( bExplorer4plus ) {
                shop.style.top = document.body.scrollTop;
                shop.style.visibility = "visible";
        }

        CheckUIElements();
        
        return true;
}