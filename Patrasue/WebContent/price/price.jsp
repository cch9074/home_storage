<%@ page contentType="text/html;charset=euc-kr" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<style type="text/css">
<!--
body {
	background-image:   url();
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #370809;
}
-->
</style><script language="javascript" src="../flash.js">
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function flevGetDivProperty() { // v1.0
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl)
	this.opera = (window.opera); this.ns4 = (document.layers); this.ie = (document.all);
	this.ns6 = (document.getElementById && !document.all && !this.opera);
    var sV = "", sD = arguments[0], oD = MM_findObj(sD), sP = arguments[1]; if (oD == null) {return 0;}
	var sF = (sD.indexOf("?") > 0) ? sD.substring(sD.indexOf("?")+1) : "";
	if ((sF != "") && (this.ie)) {sD = "parent.frames['" + sF + "']." + sD.substring(0,sD.indexOf("?"));}
	if ((sP == "left") || (sP == "top")) {
		if (!this.ns4) {oD = oD.style;} sV = eval("oD." + sP);
		if ((this.ie) && (sV == "")) { // IE (on PC) bug with nested layers
			if (sP == "top") { sV = eval(sD + ".offsetTop");} 
			else { sV = eval(sD + ".offsetLeft");}}}
	else {if (this.opera) {oD = oD.style;
			if (sP == "height") { sV = oD.pixelHeight;} 
			else if (sP == "width") { sV = oD.pixelWidth;}}
		else if (this.ns4) {sV = eval("oD.clip." + sP);} 
		else if (this.ns6) {sV = document.defaultView.getComputedStyle(oD, "").getPropertyValue(sP);} 
	    else if (this.ie) { 
			if (sP == "width") {sV = eval(sD + ".offsetWidth");} 
			else if (sP == "height") {sV = eval(sD + ".offsetHeight");}}}
	sV = (sV == "") ? 0 : sV; if (isNaN(sV)) {if (sV.indexOf('px') > 0) { sV = sV.substring(0,sV.indexOf('px'));}} 
	return parseInt(sV); 
}

function flevPersistentLayer() { // v3.4
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl/dreamweaver/)
	var sD = arguments[0], oD = eval("MM_findObj('" + sD + "')"), iWW, iWH, iSX, iSY, iT = 10, sS = "";
	if (!document.layers) {oD = oD.style;} if (oD.tmpTimeout != null) {clearTimeout(oD.tmpTimeout);}
	var sXL = arguments[1], sXC = arguments[2], sXR = arguments[3], sYT = arguments[4], sYC = arguments[5], sYB = arguments[6];
	var iS = (arguments.length > 7) ? parseInt(arguments[7]) : 0, iPx = (arguments.length > 8) ? parseInt(arguments[8]) : 0;
	if (window.innerWidth) {var oW = window; iWW = oW.innerWidth; iWH = oW.innerHeight; iSX = oW.pageXOffset; iSY = oW.pageYOffset;}
	else if (document.documentElement && document.documentElement.clientWidth) {
		var oDE = document.documentElement; iWW = oDE.clientWidth; iWH = oDE.clientHeight; iSX = oDE.scrollLeft; iSY = oDE.scrollTop;}
	else if (document.body) {var oDB = document.body; iWW = oDB.clientWidth; iWH = oDB.clientHeight; iSX = oDB.scrollLeft; iSY = oDB.scrollTop;}
	else {return;}
	var iCX = iNX = flevGetDivProperty(sD, 'left'), iCY = iNY = flevGetDivProperty(sD, 'top');
	if (sXL != "") {iNX = iSX + parseInt(sXL);} 
	else if (sXC != "") {iNX = Math.round(iSX + (iWW/2) - (flevGetDivProperty(sD, 'width')/2));}
	else if (sXR != "") {iNX = iSX + iWW - (flevGetDivProperty(sD, 'width') + parseInt(sXR));}
	if (sYT != "") {iNY = iSY + parseInt(sYT);}
	else if (sYC != "") {iNY = Math.round(iSY + (iWH/2) - (flevGetDivProperty(sD, 'height')/2));}
	else if (sYB != "") {iNY = iSY + (iWH - flevGetDivProperty(sD, 'height') - parseInt(sYB));}
	if ((iCX != iNX) || (iCY != iNY)) {if (iS > 0) {
			if (iPx > 0) {iT = iS;
				var iPxX = iPx, iPxY = iPx, iMX = Math.abs(iCX - iNX), iMY = Math.abs(iCY - iNY);
				// take care of diagonal movement
				if (iMX < iMY) {iPxY = (iMX != 0) ? ((iMY/iMX)*iPx) : iPx;}
				else {iPxX = (iMY != 0) ? ((iMX/iMY)*iPx) : iPx;}
				if (iPxX >= iMX) {iPxX = Math.min(Math.ceil(iPxX), iPx);}
				if (iPxY >= iMY) {iPxY = Math.min(Math.ceil(iPxY), iPx);}
				// temporary X/Y coordinates
				if ((iCX < iNX) && (iCX + iPxX < iNX)) {iNX = iCX + iPxX;}
				if ((iCX > iNX) && (iCX - iPxX > iNX)) {iNX = iCX - iPxX;}
				if ((iCY < iNY) && (iCY + iPxY < iNY)) {iNY = iCY + iPxY;}
				if ((iCY > iNY) && (iCY - iPxY > iNY)) {iNY = iCY - iPxY;} }
			else {var iMX = ((iNX - iCX) / iS), iMY = ((iNY - iCY) / iS); 
				iMX = (iMX > 0) ? Math.ceil(iMX) : Math.floor(iMX); iNX = iCX + iMX; 
				iMY = (iMY > 0) ? Math.ceil(iMY) : Math.floor(iMY); iNY = iCY + iMY; } }
		if ((parseInt(navigator.appVersion)>4 || navigator.userAgent.indexOf("MSIE")>-1) && (!window.opera)) {sS="px";}
		if (iMX != 0) {eval("oD.left = '" + iNX + sS + "'");} if (iMY != 0) {eval("oD.top = '" + iNY + sS + "'");}}
	var sF = "flevPersistentLayer('" + sD + "','" + sXL + "','" + sXC + "','" + sXR + "','" + sYT + "','" + sYC + "','" + sYB + "'," + iS + "," + iPx + ")"; oD.tmpTimeout = setTimeout(sF,iT);
}

function flevStartPersistentLayer() { // v3.4
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl/dreamweaver/)
	if (arguments.length < 8) {return;}	var sD = arguments[0]; if (sD == "") {return;}
	var	oD = eval("MM_findObj('" + sD + "')"); if (!oD) {return;} var iCSS = parseInt(arguments[1]);
	var sXL = arguments[2], sXC = arguments[3], sXR = arguments[4], sYT = arguments[5], sYC = arguments[6], sYB = arguments[7];
	var iS = (arguments.length > 8) ? parseInt(arguments[8]) : 0, iPx = (arguments.length > 9) ? parseInt(arguments[9]) : 0;
	if (iCSS != 0) { if (!document.layers) {oD = oD.style;} sXL = parseInt(oD.left), sYT = parseInt(oD.top);}
	var sF = "flevPersistentLayer('" + sD + "','" + sXL + "','" + sXC + "','" + sXR + "','" + sYT + "','" + sYC + "','" + sYB + "'," + iS + "," + iPx + ")";
	eval(sF);
}
//-->
</script>
</head>

<body onLoad="flevStartPersistentLayer('quick',1,'','','','','','',10)">
<div id="quick" style="position:absolute; left:949px; top:99px; width:70; height:162; z-index:1">
  <TABLE WIDTH=70 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
      <TD> <IMG SRC="../images/quick_menu_01.gif" WIDTH=70 HEIGHT=31 ALT=""></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.studio1004.com" target="_blank"><IMG SRC="../images/quick_menu_02.gif" ALT="" WIDTH=70 HEIGHT=43 border="0"></a></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.2030semi.com" target="_blank"><IMG SRC="../images/quick_menu_03.gif" ALT="" WIDTH=70 HEIGHT=44 border="0"></a></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.hyodophoto.com" target="_blank"><IMG SRC="../images/quick_menu_04.gif" ALT="" WIDTH=70 HEIGHT=44 border="0"></a></TD>
    </TR>
  </TABLE>
</div>
<table width="943" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td height="23" colspan="2"><img src="../images/top_1.gif" width="943" height="23"></td>
  </tr>
  <tr>
    <td width="263" height="66" valign="top"><script>flashWrite('../flash/sub_logo.swf', '263','66','movie','#ffffff','','transparent')</script></td>
    <td width="680" height="66" valign="top"><script>flashWrite('../flash/menu.swf', '680','66','movie','#ffffff','','transparent')</script></td>
  </tr>
</table>
<table width="943" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="8" valign="top"><img src="../images/sub_s.gif" width="943" height="9"></td>
  </tr>
</table>
<table width="943" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="263" height="671" rowspan="2" valign="top" background="../images/price_sub_bg.gif"><table width="263" height="671" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="55" rowspan="6" valign="top">&nbsp;</td>
        <td width="208" height="48" valign="top"><img src="../images/price_sub_menu_title.gif" width="208" height="48"></td>
      </tr>
      <tr>
        <td width="208" height="24" valign="top"><a href="price.jsp"><img src="../images/price_sub_menu_1.gif" width="208" height="28" border="0"></a></td>
      </tr>
      <tr>
        <td width="208" height="103" valign="top"><img src="../images/price_sub_menu_2.gif" width="208" height="103"></td>
      </tr>
      <tr>
        <td width="208" height="119" valign="top"><img src="../images/price_sub_banner_1.gif" width="208" height="119"></td>
      </tr>
      <tr>
        <td width="208" height="122" valign="top"><img src="../images/price_sub_banner_2.gif" width="208" height="122"></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/price_sub_bannner_bg.gif" width="208" height="369"></td>
      </tr>
    </table></td>
    <td><img src="../images/price_banner.gif" width="680" height="179"></td>
  </tr>
  <tr>
    <td valign="top"><img src="../images/price_text.gif" width="680" height="610"></td>
  </tr>
</table>
<table width="943" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="../images/sub_copy.gif" width="943" height="62" border="0" usemap="#Map"></td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="396,36,429,51" href="../admin/index.jsp">
</map>
</body>
</html>
