
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">

<style type="text/css">
<!--
body {
	background-color: #370809;
}
-->
</style>

<script language="JavaScript" type="text/JavaScript" src="../flash.js">
</script>
<script language="javascript">

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function flevGetDivProperty() { // v1.0
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl)
	this.opera = (window.opera); this.ns4 = (document.layers); this.ie = (document.all);
	this.ns6 = (document.getElementById && !document.all && !this.opera);
    var sV = "", sD = arguments[0], oD = MM_findObj(sD), sP = arguments[1]; if (oD == null) {return 0;}
	var sF = (sD.indexOf("?") > 0) ? sD.substring(sD.indexOf("?")+1) : "";
	if ((sF != "") && (this.ie)) {sD = "parent.frames['" + sF + "']." + sD.substring(0,sD.indexOf("?"));}
	if ((sP == "left") || (sP == "top")) {
		if (!this.ns4) {oD = oD.style;} sV = eval("oD." + sP);
		if ((this.ie) && (sV == "")) { // IE (on PC) bug with nested layers
			if (sP == "top") { sV = eval(sD + ".offsetTop");} 
			else { sV = eval(sD + ".offsetLeft");}}}
	else {if (this.opera) {oD = oD.style;
			if (sP == "height") { sV = oD.pixelHeight;} 
			else if (sP == "width") { sV = oD.pixelWidth;}}
		else if (this.ns4) {sV = eval("oD.clip." + sP);} 
		else if (this.ns6) {sV = document.defaultView.getComputedStyle(oD, "").getPropertyValue(sP);} 
	    else if (this.ie) { 
			if (sP == "width") {sV = eval(sD + ".offsetWidth");} 
			else if (sP == "height") {sV = eval(sD + ".offsetHeight");}}}
	sV = (sV == "") ? 0 : sV; if (isNaN(sV)) {if (sV.indexOf('px') > 0) { sV = sV.substring(0,sV.indexOf('px'));}} 
	return parseInt(sV); 
}

function flevPersistentLayer() { // v3.4
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl/dreamweaver/)
	var sD = arguments[0], oD = eval("MM_findObj('" + sD + "')"), iWW, iWH, iSX, iSY, iT = 10, sS = "";
	if (!document.layers) {oD = oD.style;} if (oD.tmpTimeout != null) {clearTimeout(oD.tmpTimeout);}
	var sXL = arguments[1], sXC = arguments[2], sXR = arguments[3], sYT = arguments[4], sYC = arguments[5], sYB = arguments[6];
	var iS = (arguments.length > 7) ? parseInt(arguments[7]) : 0, iPx = (arguments.length > 8) ? parseInt(arguments[8]) : 0;
	if (window.innerWidth) {var oW = window; iWW = oW.innerWidth; iWH = oW.innerHeight; iSX = oW.pageXOffset; iSY = oW.pageYOffset;}
	else if (document.documentElement && document.documentElement.clientWidth) {
		var oDE = document.documentElement; iWW = oDE.clientWidth; iWH = oDE.clientHeight; iSX = oDE.scrollLeft; iSY = oDE.scrollTop;}
	else if (document.body) {var oDB = document.body; iWW = oDB.clientWidth; iWH = oDB.clientHeight; iSX = oDB.scrollLeft; iSY = oDB.scrollTop;}
	else {return;}
	var iCX = iNX = flevGetDivProperty(sD, 'left'), iCY = iNY = flevGetDivProperty(sD, 'top');
	if (sXL != "") {iNX = iSX + parseInt(sXL);} 
	else if (sXC != "") {iNX = Math.round(iSX + (iWW/2) - (flevGetDivProperty(sD, 'width')/2));}
	else if (sXR != "") {iNX = iSX + iWW - (flevGetDivProperty(sD, 'width') + parseInt(sXR));}
	if (sYT != "") {iNY = iSY + parseInt(sYT);}
	else if (sYC != "") {iNY = Math.round(iSY + (iWH/2) - (flevGetDivProperty(sD, 'height')/2));}
	else if (sYB != "") {iNY = iSY + (iWH - flevGetDivProperty(sD, 'height') - parseInt(sYB));}
	if ((iCX != iNX) || (iCY != iNY)) {if (iS > 0) {
			if (iPx > 0) {iT = iS;
				var iPxX = iPx, iPxY = iPx, iMX = Math.abs(iCX - iNX), iMY = Math.abs(iCY - iNY);
				// take care of diagonal movement
				if (iMX < iMY) {iPxY = (iMX != 0) ? ((iMY/iMX)*iPx) : iPx;}
				else {iPxX = (iMY != 0) ? ((iMX/iMY)*iPx) : iPx;}
				if (iPxX >= iMX) {iPxX = Math.min(Math.ceil(iPxX), iPx);}
				if (iPxY >= iMY) {iPxY = Math.min(Math.ceil(iPxY), iPx);}
				// temporary X/Y coordinates
				if ((iCX < iNX) && (iCX + iPxX < iNX)) {iNX = iCX + iPxX;}
				if ((iCX > iNX) && (iCX - iPxX > iNX)) {iNX = iCX - iPxX;}
				if ((iCY < iNY) && (iCY + iPxY < iNY)) {iNY = iCY + iPxY;}
				if ((iCY > iNY) && (iCY - iPxY > iNY)) {iNY = iCY - iPxY;} }
			else {var iMX = ((iNX - iCX) / iS), iMY = ((iNY - iCY) / iS); 
				iMX = (iMX > 0) ? Math.ceil(iMX) : Math.floor(iMX); iNX = iCX + iMX; 
				iMY = (iMY > 0) ? Math.ceil(iMY) : Math.floor(iMY); iNY = iCY + iMY; } }
		if ((parseInt(navigator.appVersion)>4 || navigator.userAgent.indexOf("MSIE")>-1) && (!window.opera)) {sS="px";}
		if (iMX != 0) {eval("oD.left = '" + iNX + sS + "'");} if (iMY != 0) {eval("oD.top = '" + iNY + sS + "'");}}
	var sF = "flevPersistentLayer('" + sD + "','" + sXL + "','" + sXC + "','" + sXR + "','" + sYT + "','" + sYC + "','" + sYB + "'," + iS + "," + iPx + ")"; oD.tmpTimeout = setTimeout(sF,iT);
}

function flevStartPersistentLayer() { // v3.4
	// Copyright 2002, Marja Ribbers-de Vroed, FlevOOware (www.flevooware.nl/dreamweaver/)
	if (arguments.length < 8) {return;}	var sD = arguments[0]; if (sD == "") {return;}
	var	oD = eval("MM_findObj('" + sD + "')"); if (!oD) {return;} var iCSS = parseInt(arguments[1]);
	var sXL = arguments[2], sXC = arguments[3], sXR = arguments[4], sYT = arguments[5], sYC = arguments[6], sYB = arguments[7];
	var iS = (arguments.length > 8) ? parseInt(arguments[8]) : 0, iPx = (arguments.length > 9) ? parseInt(arguments[9]) : 0;
	if (iCSS != 0) { if (!document.layers) {oD = oD.style;} sXL = parseInt(oD.left), sYT = parseInt(oD.top);}
	var sF = "flevPersistentLayer('" + sD + "','" + sXL + "','" + sXC + "','" + sXR + "','" + sYT + "','" + sYC + "','" + sYB + "'," + iS + "," + iPx + ")";
	eval(sF);
}
//-->
</script>

<link href="../style.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="flevStartPersistentLayer('quick',1,'','','','','','',10)">
<div id="quick" style="position:absolute; left:949px; top:99px; width:70; height:162; z-index:1">
  <TABLE WIDTH=70 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
      <TD> <IMG SRC="../images/quick_menu_01.gif" WIDTH=70 HEIGHT=31 ALT=""></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.studio1004.com" target="_blank"><IMG SRC="../images/quick_menu_02.gif" ALT="" WIDTH=70 HEIGHT=43 border="0"></a></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.2030semi.com" target="_blank"><IMG SRC="../images/quick_menu_03.gif" ALT="" WIDTH=70 HEIGHT=44 border="0"></a></TD>
    </TR>
    <TR>
      <TD> <a href="http://www.hyodophoto.com" target="_blank"><IMG SRC="../images/quick_menu_04.gif" ALT="" WIDTH=70 HEIGHT=44 border="0"></a></TD>
    </TR>
  </TABLE>
</div>
<table width="943" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td height="23" colspan="2"><img src="../images/top_1.gif" width="943" height="23"></td>
  </tr>
  <tr>
    <td width="263" height="66" valign="top"><script>flashWrite('../flash/sub_logo.swf', '263','66','movie','#ffffff','','transparent')</script></td>
    <td width="680" height="66" valign="top"><script>flashWrite('../flash/menu.swf', '680','66','movie','#ffffff','','transparent')</script></td>
  </tr>
</table>
<table width="943" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="9" colspan="4"><img src="../images/sub_s.gif" width="943" height="9"></td>
  </tr>
  <tr valign="top">
    <td width="55" background="../images/com_left_bg.gif"><table width="55" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="55" valign="top"><img src="../images/com_left_img.gif" width="55" height="673"></td>
      </tr>
    </table></td>
    <td width="196" valign="top" background="../images/com_mid_bg.gif"><table width="196%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="196" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="top"><img src="../images/com_sub_memu_title.gif" width="196" height="48"></td>
            </tr>
            <tr>
              <td valign="top"><a href="notice_list.jsp"><img src="../images/com_sub_memu_1.gif" width="196" height="31" border="0"></a></td>
            </tr>
            <tr>
              <td valign="top"><a href="faq_list.jsp"><img src="../images/com_sub_memu_2.gif" width="196" height="31" border="0"></a></td>
            </tr>
            <tr>
              <td valign="top"><a href="freebbs_list.jsp"><img src="../images/com_sub_memu_3.gif" width="196" height="32" border="0"></a></td>
            </tr>
            <tr>
              <td valign="top"><a href="diary_list.jsp"><img src="../images/com_sub_memu_4.gif" width="196" height="31" border="0"></a></td>
            </tr>
            <tr>
              <td valign="top"><a href="manager.jsp"><img src="../images/com_sub_memu_5.gif" width="196" height="36" border="0"></a></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/com_bbs_banner01.gif" width="196" height="131"></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/com_bbs_banner02.gif" width="196" height="113"></td>
      </tr>
      <tr>
        <td width="196" height="210" valign="top"><img src="../images/com_bbs_banner03.gif" width="196" height="210"></td>
      </tr>
    </table></td>
    <td width="12" valign="top" background="../images/com_mid_img02_bg.gif"><table width="12" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12"><img src="../images/com_mid_img02.gif" width="12" height="673"></td>
      </tr>
    </table></td>
    <td width="680" valign="top"><table width="680" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="../images/com_bannner.gif" width="680" height="173"></td>
        </tr>
      <tr>
        <td><img src="../images/com_banner_bottom.gif" width="680" height="6"></td>
      </tr>
		<form method="post" >
      <tr>
        <td><table width="680" border="0" cellpadding="0" cellspacing="0" background="../images/body_bg.gif">
          <tr>
            <td width="30" height="483" valign="top" background="../images/body_bg.gif">&nbsp;</td>
            <td width="620" valign="top" background="images/body_bg.gif"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2"><img src="../images/com_bbs_title.gif" width="620" height="66"></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top"><img src="../images/bbs_write_title.gif" width="620" height="31"></td>
                </tr>
                <tr>
                  <td height="9" colspan="2" valign="top"></td>
                </tr>
                <tr valign="middle">
                  <td width="80" height="25"><img src="../images/arr01.gif" width="12" height="7">작 성 자</td>
                  <td width="548" height="25"><input type="text" name="name"  style="width:150;height:18;" class="input1" ></td>
                </tr>
                <tr valign="middle">
                  <td width="80" height="25"><img src="../images/arr01.gif" width="12" height="7">비밀번호</td>
                  <td height="25"><input type="password" name="pass" style="width:150;height:18;" class="input1"></td>
                </tr>
                <tr valign="middle">
                  <td width="80" height="25"><img src="../images/arr01.gif" width="12" height="7">제 목 </td>
                  <td height="25"><input type="text" name="subject"  style="width:450;height:18;" class="input1" size=50 ></td>
                </tr>
                <tr>
                  <td height="9" colspan="2" valign="top"></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top"><textarea name="contents" rows="15" cols="85">&nbsp;</textarea></td>
                </tr>
                <tr align="center" valign="bottom">
                  <td height="30" colspan="2">
					<img src="../images/btn_write.gif" border="0" align="absmiddle">&nbsp; <img src="../images/btn_cancel.gif" border=0 align="absmiddle">							
						</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
            </table></td>
            <td width="30" valign="top" background="images/body_bg.gif">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      </form>
			<tr>
        <td><img src="../images/com_body_bottom.gif" width="680" height="11"></td>
        </tr>
    </table>      </td>
  </tr>
</table>
<table width="943" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="../images/sub_copy.gif" width="943" height="62" border="0" usemap="#Map"></td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="394,34,431,51" href="admin/index.jsp">
</map>
</body>
</html>
