package exam_1108.board;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/write.do")
public class BoardWrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public BoardWrite() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("Board/board_write.jsp");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String content = request.getParameter("content");
		String pass = request.getParameter("pass");
		
		BoardVO bVO = new BoardVO();
		bVO.setName(name);
		bVO.setEmail(email);
		bVO.setSubject(subject);
		bVO.setContent(content);
		bVO.setPass(pass);
		bVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		BoardDAO bDAO = BoardDAO.getInstance();
		bDAO.boardInsert(bVO);
		response.sendRedirect("Board/board_list.jsp");
		
	}

}
