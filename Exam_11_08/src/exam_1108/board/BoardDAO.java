package exam_1108.board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class BoardDAO {
	
	private BoardDAO() {} //접근 금지
	
	private static BoardDAO instance = new BoardDAO(); //
	
	public static BoardDAO getInstance() {
		return instance;
	}
	//커넥션 풀
	public Connection getConnection() throws Exception {
		Connection conn = null;
		Context init = new InitialContext();
		Context env = (Context)init.lookup("java:/comp/env");
		DataSource ds = (DataSource)env.lookup("jdbc/myoracle");
		conn = ds.getConnection();
		return conn;
	}
	

    public void boardInsert(BoardVO bVO) {
	   Connection conn = null;
	   PreparedStatement pstmt = null;
	   ResultSet rs = null;
	   
	   String sql = "insert into hr_board(no,name,email,subject,content,pass,regdate,readcnt)"
			        + "values(hr_board_seq.nextval,?,?,?,?,?,?,?)";
	   try {
		conn = getConnection();
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, bVO.getName());
		pstmt.setString(2, bVO.getEmail());
		pstmt.setString(3, bVO.getSubject());
		pstmt.setString(4, bVO.getContent());
		pstmt.setString(5, bVO.getPass());
		pstmt.setTimestamp(6, bVO.getRegdate());
		pstmt.setInt(7, 0);
		
		pstmt.executeUpdate();
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		try {
			if(rs != null) rs.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		}catch (Exception ee) {
			ee.printStackTrace();
		}
	}
}
    //전체 목록 메소드
}