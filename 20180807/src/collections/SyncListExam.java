package collections;
import java.util.*;
public class SyncListExam {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("AAA");
		list.add("BBB");
		list.add("CCC");
		list.add("DDD");

		for(int i =0;i<list.size();i++) {
			System.out.println(list.get(i));
			
		}
		List<String> syncList = Collections.synchronizedList(list);
		for(int i =0;i<syncList.size();i++) {
			System.out.println(syncList.get(i));
			
		}
		//동기화 하면서 새로운 List를 생성할 수 있다.
		List<Integer> sList = Collections.synchronizedList(new ArrayList<Integer>());
		sList.add(123);
		sList.add(234);
		
	}

}
