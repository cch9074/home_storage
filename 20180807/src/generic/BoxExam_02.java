package generic;
import java.util.*;
class Box2<T>{
	//'T'ype 
	//클래스를 만들 때 제너릭을 입력해주어 생성할 때 입력한 타입을 지정하여 사용할수있다.
	//제너릭을 하지 않을 경우 Object로 입력되기 때문에 형변환 하는 번거로움을 덜수 있다.
	private T t;
	
	public T get() {
		return t;
	}
	public void set(T t) {
		this.t= t;
	}
}
public class BoxExam_02 {
	public static void main(String[] args) {
		Box2<String> box =new Box2<String>();
		box.set("김철수");
		String str = box.get();
		System.out.println(str);
		
		Box2<Integer> box2 = new Box2<Integer>();
		box2.set(33);
		int a = box2.get();
		System.out.println(a);
		
	}
}
