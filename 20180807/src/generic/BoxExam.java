package generic;
import java.util.*;
class Box{
	
	private Object obj; //모든 객체를 넣을 수 있다.
//	private int a;
	Box(){}
	public void set(Object obj) {
		this.obj = obj;
	}
	public Object get() {
		return obj;
	}
}
class Apple{
	
}
public class BoxExam {

	public static void main(String[] args) {
		/*
		List list = new ArrayList();
		list.add("김철수");
		//제너릭을 사용하지 않으면 값을 가져올때 캐스팅을 해야하는 번거로움이 있다.
		String str = (String)list.get(0);
		
		List<String> list2 = new ArrayList<String>();
		list2.add("김철수철수");
		//제너릭을 사용하여 캐스팅할 필요가 없다.
		String str2 = list2.get(0);
		 */		
		Box box = new Box();
		box.set("김철수");
		String str = (String)box.get();
		
		System.out.println(str);
		box.set(new Apple());
		Apple a = (Apple)box.get();
	}

}
