package generic;
class Tv{
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
class Product<T,M>{
	//변수는무엇을 써도 상관없지만 상징성있는 것으로 하는게 좋다.
	
	private T kind;
	private M model;
	
	public T getKind() {
		return kind;
	}
	public void setKind(T kind) {
		this.kind = kind;
	}
	public M getModel() {
		return model;
	}
	public void setModel(M model) {
		this.model = model;
	}
}
public class MultiExam {

	public static void main(String[] args) {

		Product<String, Integer> p = new Product<String, Integer>();
		p.setKind("삼성TV");
		p.setModel(3321);
		
		String str = p.getKind();
		int i = p.getModel();
		System.out.println("Kind : " + str +" Model : " + i);
		Product<Tv,String> p2 = new Product<>();
		//생성자의 제너릭이 공란이면 앞에서 선언한 제너릭과 동일한 것으로 인식한다.
		p2.setKind(new Tv());
		p2.setModel("김철수");
		
		Tv t = p2.getKind();
		t.setName("김철수");
		String str3 = t.getName();
		String str2 = p2.getModel();
		System.out.println("Kind : "+ str3 +" Model : "+str2);
	}
}
