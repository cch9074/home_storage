package generic;
//제너릭을 이용하여 반환타입 지정하기
class Util{
	public static <T> Box3<T> boxing(T t){
		Box3<T> box = new Box3<T>();
		box.setT(t);
		return box;
	}
}
class Box3<T>{
	private T t;
	
	public T getT() {
		return t;
	}
	public void setT(T t) {
		this.t= t;
	}
}
public class MethodExam {
	
	public static void main(String[] args) {
		Box3<Integer> box = Util.<Integer>boxing(100);
		int val = box.getT();
		System.out.println("val : "+ val);
		
		Box3<String> box2 = Util.boxing("김철수");
		String str = box2.getT();
		System.out.println(str);
	}
}
