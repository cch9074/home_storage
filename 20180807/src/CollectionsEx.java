import java.util.*;
import static java.util.Collections.*;
public class CollectionsEx {

	public static void main(String[] args) {
		List list = new ArrayList();
		System.out.println(list);
		
		addAll(list,1,2,3,4,5);
		System.out.println(list);
		//오른쪽으로 2칸씩 이동
		rotate(list,2);
		System.out.println(list);
		//2번과 0번을 자리바꿈
		swap(list,0,2);
		System.out.println(list);
		//자리 섞기
		shuffle(list);
		System.out.println(list);
		//정렬(오름차순
		sort(list);
		System.out.println(list);
		//역순으로 정렬
		sort(list, reverseOrder());
		System.out.println(list);
		//list에서 3번째 자리의 값을 가져옴
		int idx = binarySearch(list, 3);
		System.out.println("index of 3 : " +idx);
		//최대값, 최소값
		System.out.println("max : "+max(list));
		System.out.println("min : "+min(list));
		//역순으로 바꾼뒤 최대값을 구하여 최소값
		System.out.println("min : "+max(list,reverseOrder()));
		
		fill(list,9);
		System.out.println("list : "+list);
		
		List newList = nCopies(list.size(),	2);
		System.out.println("newList : "+newList);
		
		System.out.println(disjoint(list,newList));
		
		copy(list,newList);
		System.out.println("newList : "+newList);
		System.out.println("list : "+list);
		
		replaceAll(list, 2, 1);
		System.out.println("list : "+list);

		Enumeration e = enumeration(list);
		ArrayList list2 = list(e);
		
		System.out.println("list2 : "+list2);
	}
}
