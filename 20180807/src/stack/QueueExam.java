package stack;
import java.util.*;
class Message{
	private String command;
	private String to;
	public Message() {}
	public Message(String command, String to) {
		this.command = command;
		this.to=to;
	}
	public String getCommand() {
		return command;
	}
	public String getTo() {
		return to;
	}
}
public class QueueExam {

	public static void main(String[] args) {
		Queue<Message> q = new LinkedList<Message>();
		q.offer(new Message("sendMail","김철수"));
		q.offer(new Message("sendSMS","김영희"));
		q.offer(new Message("sendKatok","김국진"));
		//FIFO방식이므로 먼저 입력되는것ㅇ ㅣ먼처 출력된다.
		while(!q.isEmpty()) {
			Message m = q.poll();
			//조건이 범위가 아닌 점이므로 switch를 사용
			switch(m.getCommand()) {
			//조건이 String이므로 ""를 씌워준다.
			case "sendMail" : 
				System.out.println(m.getTo()+"님에게 메일이 왔습니다.");
				break;
			case "sendSMS" : 
				System.out.println(m.getTo()+"님에게 SMS이 왔습니다.");
				break;
			case "sendKatok" : 
				System.out.println(m.getTo()+"님에게 카톡이 왔습니다.");
				break;	
			}
		}
	}
}
