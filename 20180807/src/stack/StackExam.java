package stack;
import java.util.*;
class Coin{
	private int value;
	public Coin() {}
	public Coin(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}
}

public class StackExam {

	public static void main(String[] args) {
		//스택도 클래스이다.
		Stack<Coin> stack = new Stack<Coin>();
		stack.push(new Coin(100));
		stack.push(new Coin(50));
		stack.push(new Coin(500));
		stack.push(new Coin(10));
		while(!stack.isEmpty()) {
			Coin c = stack.pop();
			//스택 값 뽑아오기
			//스택의 특성상 들어간 순서의 역순으로 뽑아온다.
			System.out.println(c.getValue());
		}
	}

}
