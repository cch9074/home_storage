package collection;
//List 의 종류 ArrayList / LinkedList
import java.util.*;
class Person{
	private int no;
	private String name;
	Person (){}
	
	Person(int no, String name){
		this.no= no;
		this.name = name;
	}
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
public class Exam_02 {

	public static void main(String[] args) {
		List<Person> list = new ArrayList<Person>();
 
		//Person 만 들어갈 수 있게 명명해줄 수 있다.
		list.add(new Person(12345,"AAAAA"));
		list.add(new Person(23456,"BBBBB"));
		list.add(new Person(34567,"CCCCC"));
		list.add(new Person(45678,"DDDDD"));
		list.add(2,new Person(56789,"FFFFF"));
		
		//List 에 객체도 넣을수 있다.
		//list.add(1234);
		for(int i=0;i<list.size();i++) {
			//입력값의 기본타입은 Object로 받을 수 있다.
			Person per  = (Person)list.get(i);
			Person per2 = list.get(i);			
			//Person객체만 들어가있기 때문에 캐스팅을 하지 않아주어도 된다.
			System.out.println(per.getName()+per.getNo());
		}
		
		List list2 = new LinkedList();
		//자료의 삽입 및 삭제가 빈번할 경우LinkedList를 이용하는것이 좋다.
		list2.add("AAAA");
		list2.add(1234);
		for(int i=0;i<list.size();i++) {
			System.out.println(list2.get(i));
		}
	}
}
