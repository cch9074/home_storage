package calendar;

import java.util.Calendar;

public class CalendarExam_02 {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		int ampm = cal.get(Calendar.AM_PM);
		if(ampm==Calendar.AM) {
			System.out.print("현재시간은 AM :");
		}else {
			System.out.print("현재시간은 PM :");
		}
		System.out.println(hour+"시 "+min+"분"+sec+"초");
	}
}
