package calendar;

import java.util.Calendar;
import java.util.Scanner;
//캘린더 클래스를 이용해서 달력만들기.
public class CalendarExam_03 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("년도와 월을 순서대로 입력해주세요");
		int year = scan.nextInt();
		int mon = scan.nextInt();
	//	int year = Integer.parseInt(args[0]);
	//	int mon = Integer.parseInt(args[1]);
		
		int START_DAY_OF_WEEK = 0;
		int END_DAY= 0;
		
		Calendar sDay = Calendar.getInstance();
		Calendar eDay = Calendar.getInstance();
		sDay.set(year, mon-1,1);
		eDay.set(year, mon,1);
		System.out.println(sDay.get(Calendar.MONTH));
		
		eDay.add(Calendar.DATE, -1);
		START_DAY_OF_WEEK = sDay.get(Calendar.DAY_OF_WEEK);
		
		END_DAY = eDay.get(Calendar.DATE);		
		System.out.println("	"+year +"년"+mon+"월");
		System.out.println(" SU MO TU WE TH FR SA");
		
		for(int i=1; i<START_DAY_OF_WEEK;i++) {
			System.out.print("   ");
		}
		
		for(int i=1,n=START_DAY_OF_WEEK;i<=END_DAY;i++,n++) {
			System.out.print((i<10)? "  "+i: " "+i);
			if(n%7==0) {
				System.out.println();
			}
		}
	}
}
