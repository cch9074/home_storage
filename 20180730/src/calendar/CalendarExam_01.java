package calendar;

import java.util.Calendar;

public class CalendarExam_01 {

	public static void main(String[] args) {
		//Calendar 객체를 이용해 연/월/일을 가져올수 있다.
		
		Calendar cal = Calendar.getInstance();
		System.out.println(cal.get(Calendar.YEAR));				//연
		System.out.println(cal.get(Calendar.MONTH)+1);			//월
		//*자바에서는 1월달이 0이다
		System.out.println(cal.get(Calendar.DATE));				//일
		System.out.println(cal.get(Calendar.DAY_OF_WEEK));		//요일
		//*요일은 일요일 == 1이다.
		System.out.println(cal.get(Calendar.HOUR));				//시간
		System.out.println(cal.get(Calendar.MINUTE));			//분
		System.out.println(cal.get(Calendar.SECOND));			//초
		
	}
}
