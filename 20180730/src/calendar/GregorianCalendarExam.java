package calendar;

import java.util.Calendar;
import java.util.GregorianCalendar;

//그레고리안캘린더 클래스를 이용한 달력만들기.
public class GregorianCalendarExam
{
   public static void main(String[] args)
   {
      int year = 2018;
      int month = 7;
      
      GregorianCalendar cal = new GregorianCalendar(year,(month-1),1);//1을 빼야 입력한 달의 결과값을 얻을 수 있다.
      
      System.out.println("\t\t\t\t["+year+"년"+month+"월]");
      System.out.println("===================================================");
      System.out.println("일\t월\t화\t수\t목\t금\t토");
      
      
      int maxday = cal.getActualMaximum(cal.DAY_OF_MONTH); // 막날
      int week = cal.get(Calendar.DAY_OF_WEEK); // 요일, 1=일요일, 2=월요일
      
      for(int i = 1; i <= (week-1); i++)
      {
         System.out.print("\t");
      }
      for(int i = 1; i <= maxday; i++)
      {
         if(week <= 7) System.out.print(i+"\t");
         else
         {
            System.out.println();
            System.out.print(i+"\t");
            week=1;
         }
         week += 1;
      }
      System.out.println("\t");
      System.out.println("===================================================");
   }

}