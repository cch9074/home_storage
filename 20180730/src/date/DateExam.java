package date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateExam {

	public static void main(String[] args) {
		Date date = new Date();					
		System.out.println(date.toString());			//현재 날짜와 시간을 출력할 수 있다
		//기본값은 요일/월/일/시간/년도
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 hh시 mm분 ss초");
												//포멧을 지정해줄 수 있다.
		String now = sdf.format(date);
		System.out.println(now);
		
	}

}
