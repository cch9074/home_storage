import java.util.*;
class BankAccount{
	int balance;
	
	void deposit(int scan) {				//입금
		balance+=scan;
		System.out.println(scan +"원 입금이 완료되었습니다.");
		getBalance();
	}
	void withdraw(int scan) {				//출금
		if(balance>=scan) {
			balance-=scan;
			System.out.println(scan +"원 출금이 완료되었습니다.");
			getBalance();
		}else {
			System.out.println("잔고가 부족합니다.");
		}
	}
	int getBalance() {						//잔액
		System.out.println("잔액 : " + balance);
		return balance;
	}
	void getInterrest() {					//연이자
		int intrr = (int)((0.075)*balance);
		System.out.println("이자 " + intrr+"원이 입금되었습니다.");
		balance+=intrr;
	}
}
public class BankAccountTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BankAccount b = new BankAccount();
		boolean flag =true;
	while(flag) {
		Scanner scan =new Scanner(System.in);
		System.out.println("===================================");
		System.out.println("1.입금   2.출금   3.잔액확인   4.이자 받기   5.종료");
		System.out.println("===================================");
	
			int button = scan.nextInt();
			if(button ==1) {
				System.out.println("입금할 금액을 입력해주세요 ");
				b.getBalance();
				int input = scan.nextInt();			//입금
				b.deposit(input); 	
		
			}
			else if(button==2) {
				System.out.println("출금할 금액을 입력해주세요 ");
				b.getBalance();
				int output = scan.nextInt();
				b.withdraw(output);						//출금
				
			}else if(button ==3) {
				
				b.getBalance();
			
			}else if(button==4) {
				b.getInterrest();
				b.getBalance();
			}else if(button ==5) {
				flag=false;
				System.out.println("BankAccount를 종료합니다.");
			}
		}
	}

}
