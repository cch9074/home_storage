
public class Word {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Word w = new Word();
		String str = "Apple";
		String str2 = "Banana";
		//Test
		System.out.println("w.reversWord(Apple)="+w.reverseWord(str));
		System.out.println("w.countWord(Apple)="+w.countWord(str));
		System.out.println("w.sliceWord(Apple)="+w.sliceWord(str));
		System.out.println();
		//Test2
		System.out.println("w.reversWord(Banana)="+w.reverseWord(str2));
		System.out.println("w.countWord(Banana)="+w.countWord(str2));
		System.out.println("w.sliceWord(Banana)="+w.sliceWord(str2));
		
		
	}
	String reverseWord (String str) {
		String result ="";
		for(int i=str.length()-1;i>=0;i--) {
			result +=str.charAt(i);
		}
		return result;
	}
	int countWord(String str) {
		int cnt =0;
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)=='a' || str.charAt(i)=='A')cnt++;
			else if(str.charAt(i)=='e' || str.charAt(i)=='E')cnt++;
			else if(str.charAt(i)=='i' || str.charAt(i)=='I')cnt++;
			else if(str.charAt(i)=='o' || str.charAt(i)=='O')cnt++;
			else if(str.charAt(i)=='u' || str.charAt(i)=='U')cnt++;
		}
		return cnt;
	}
	String sliceWord(String str) {
		String result = "";
		for(int i=1;i<str.length()-1;i++) {
			result+=str.charAt(i);
		}
		return result;
	}
}
