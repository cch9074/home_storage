package exam_20181109.guest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class GuestDAO {
	
	private static  GuestDAO instance = new GuestDAO();
	
	public static GuestDAO getInstance() {
		return instance;
	}
	
	
	public Connection getConnection() throws Exception{
		Connection conn = null;
		Context init = new InitialContext();
		Context evn = (Context) init.lookup("java:/comp/evn");
		DataSource ds = (DataSource)evn.lookup("jdbc/myoracle");
		conn =  ds.getConnection();
		return conn;
				
	}
	public List guestList() {
		
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<GuestVO> list = new ArrayList();
		
		
		String query = "select * from hr guest order by no desc";
		
			try {
				conn = getConnection();
				pstmt = conn.prepareStatement(query);
				
				rs = pstmt.executeQuery();
				GuestVO guest = null;
				while( rs.next()) {
					guest = new GuestVO();
					guest.setNo(rs.getInt("no"));
					guest.setName(rs.getString("name"));
					guest.setSubject(rs.getString("subject"));
					guest.setRegdate(rs.getTimestamp("regdate"));
					guest.setReadcnt(rs.getInt("readcnt"));
					list.add(guest);
				}		
					
				
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if(rs != null) rs.close();
					if(pstmt != null) pstmt.close();
					if(conn != null) conn.close();
				}catch (Exception ee) {
					ee.printStackTrace();
				}
				return list;
			}
			
		}
	}


