package controller.button;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class RootController implements Initializable{
	@FXML private RadioButton btn1;
	@FXML private RadioButton btn2;
	@FXML private RadioButton btn3;
	@FXML private Label label;
	@FXML private ToggleGroup group;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
			System.out.println(newValue.getUserData()+"가 선택되었습니다.");
			System.out.println(oldValue.getUserData().toString()+"가 선택해제되었습니다.");
			label.setText(newValue.getUserData().toString());
			}
			
		});
		
	}

}
