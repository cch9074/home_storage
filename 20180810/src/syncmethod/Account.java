package syncmethod;

public class Account {
	private String accNo;
	private String accName;
	private int balance;
	
	public Account(String accNo, String accName, int balance) {
		this.accNo = accNo;
		this.accName = accName;
		this.balance = balance;
	}
	//�Ա�
	public void deposit(int money) {
		balance += money;
	}
	//���
	public int withdraw(int money) {
		if(balance<money) {
			return 0;
		}
		balance-=money;
		return balance;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
	
}
