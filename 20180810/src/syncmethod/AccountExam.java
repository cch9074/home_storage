package syncmethod;
	
public class AccountExam {
	
	public static void main(String[] args) {
		ShareArea area = new ShareArea();
		area.acc1 = new Account("0001","��ö��",2000);
		area.acc2 = new Account("0002","�迵��",1000);
		area.transfer(100);
		
		Thread t1 = new TransferThread(area);
		Thread t2 = new PrintThread(area);
		t1.start();
		t2.start();
		
	}
}
