package syncmethod;
//공유영역에서 동기화 메서드를 이용해 동기화
public class ShareArea {
	Account acc1;
	Account acc2;
	
	synchronized void transfer(int money) {
		acc1.withdraw(100);
		System.out.print(acc1.getBalance()+"acc1 계좌에100원 인출\t");
		acc2.deposit(100);
		System.out.println(acc2.getBalance() + "acc2 계좌에 100원 입금");
		

	}
	synchronized int getTotal() {
		return acc1.getBalance() + acc2.getBalance();
	}
}
