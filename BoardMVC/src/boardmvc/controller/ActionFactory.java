package boardmvc.controller;

import boardmvc.controller.action.BoardDeleteAction;
import boardmvc.controller.action.BoardDeleteProAction;
import boardmvc.controller.action.BoardListAction;
import boardmvc.controller.action.BoardListProAction;
import boardmvc.controller.action.BoardModifyAction;
import boardmvc.controller.action.BoardModifyProAction;
import boardmvc.controller.action.BoardViewAction;
import boardmvc.controller.action.BoardWriteAction;
import boardmvc.controller.action.BoardWriteProAction;

public class ActionFactory {
	private static ActionFactory instance = new ActionFactory() ;
	
	private ActionFactory() {
		super();
	}
	public static ActionFactory getInstance() {
		return instance;
	}
	public Action getAction(String command) {
		Action action = null;
		System.out.println("ActionFactory : "+command);
		
		if(command.equals("board_list")) {
			action = new BoardListAction();
			
		}else if(command.equals("board_list_pro")) {
			action = new BoardListProAction();
		}
		else if(command.equals("board_write")) {
			action = new BoardWriteAction();
		}else if(command.equals("board_write_pro")) {
			action = new BoardWriteProAction();
		}else if(command.equals("board_view")) {
			action = new BoardViewAction();
		}else if(command.equals("board_modify")) {
			action = new BoardModifyAction();
		}else if(command.equals("board_modify_pro")) {
			action = new BoardModifyProAction();
		}else if(command.equals("board_delete")) {
			action = new BoardDeleteAction();
		}else if(command.equals("board_delete_pro")) {
			action = new BoardDeleteProAction();
		}
		
		
		return action;
	}
}
