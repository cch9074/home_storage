package boardmvc.controller.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;
import boardmvc.db.BoardDAO;

public class BoardListAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {//doGet가된다.
		BoardDAO bDAO = BoardDAO.getInstance();
		String url ="Board?command=board_list";
		String s_query="",addtag="",key="";
		int nowpage = 1;//시작 페이지
		int maxlist=10; //페이지당 라인수 
		int totpage=1;
		int totcount = bDAO.boardCount();
		
		if(totcount%maxlist == 0) {		
			totpage = totcount/maxlist;
		}else{
			totpage = totcount/maxlist + 1;
		}
		
		if(totpage == 0) {			//게시글 총 수가 10개(maxlist)가 안될경우
			totpage = 1;
		}
		if(request.getParameter("page") !=null && !request.getParameter("page").equals("")){
			nowpage = Integer.parseInt(request.getParameter("page"));
			
			//url="list.do?page="+nowpage;
		}
		if(nowpage>totpage) {
			nowpage = totpage;	
		}
		//총 리스트에서 현재페이지 시작번호 구하기
		
//		int pageStart = nowpage * maxlist +1;	
//		int listcount = totcount - ((nowpage-1)*maxlist) +1 ;
//		@SuppressWarnings("unchecked")
//		List<BoardDAO> list = bDAO.boardList(listcount,pageStart);
		
		int pagestart = (nowpage-1)*maxlist +1;
		int endpage = nowpage *maxlist;
		int listcount = totcount-((nowpage-1)*maxlist)+1;
		List list = bDAO.boardList(pagestart, endpage);
		
		request.setAttribute("addtag", addtag);
		request.setAttribute("url", url);
		request.setAttribute("nowpage", nowpage);
		request.setAttribute("totpage", totpage);
		request.setAttribute("totcount", totcount);
		request.setAttribute("pageStart", pagestart);
		request.setAttribute("listcount", listcount);
		request.setAttribute("list", list);
		request.setAttribute("page", nowpage);
		
		RequestDispatcher ds = request.getRequestDispatcher("Board/board_list.jsp");
		ds.forward(request, response);
	}
	
	

}
