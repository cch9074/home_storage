package boardmvc.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;

public class BoardWriteAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stuarg0);
		String page = request.getParameter("page");
		request.setAttribute("nowpage", page);
		RequestDispatcher ds = request.getRequestDispatcher("Board/board_write.jsp");
		ds.forward(request, response);
	}
	

}
