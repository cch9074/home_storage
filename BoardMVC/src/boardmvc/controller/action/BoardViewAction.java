package boardmvc.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;
import boardmvc.db.BoardDAO;
import boardmvc.db.BoardVO;

public class BoardViewAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int idx = Integer.parseInt(request.getParameter("idx"));
		String page = request.getParameter("page");
		//System.out.println(page);
		BoardDAO bDAO = BoardDAO.getInstance();
		Cookie[] cookies = request.getCookies();
		//Cookie cookieEx = null;
		boolean found = false;
		
		for(Cookie c : cookies) {
			if(c.getName().equals("id"+idx)) {
				found = true;
				break;
			}
		}
		//쿠키들 에서 이름 얻어온다음에 이름 같으면 그냥넘기고 없으면 새로만들고 조회수 올려준다 
		if(!found) {
		 	Cookie c = new Cookie("id"+idx,"host");		//쿠키 만듬
		 	c.setMaxAge(24*60*60);					//유효시간
		 	response.addCookie(c);					//보냄
		 	bDAO.boardCountNo(idx);
		}
		
		
		BoardVO bVO = bDAO.boardListNo(idx);
		request.setAttribute("bVO", bVO);
		request.setAttribute("page", page);
		RequestDispatcher ds = request.getRequestDispatcher("/Board/board_view.jsp");
		ds.forward(request, response);
	}
	

}
