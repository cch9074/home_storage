package boardmvc.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;
import boardmvc.db.BoardDAO;
import boardmvc.db.BoardVO;

public class BoardDeleteProAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		int row = bDAO.boardDelete(bVO);
		request.setAttribute("row", row);
				
		//request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("Board/board_delete_pro.jsp");
		ds.forward(request, response);
	//	doGet(request, response);
	}
	

}
