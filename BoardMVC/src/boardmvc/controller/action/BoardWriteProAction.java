package boardmvc.controller.action;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;
import boardmvc.db.BoardDAO;
import boardmvc.db.BoardVO;

public class BoardWriteProAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		String pass = request.getParameter("pass");
		String nowpage = request.getParameter("nowpage");
			
		BoardVO bVO = new BoardVO();
		
		bVO.setName(name);
		bVO.setEmail(email);
		bVO.setSubject(subject);
		bVO.setContents(contents);
		bVO.setPass(pass);
		bVO.setRegdate(new Timestamp(System.currentTimeMillis()));
		
		BoardDAO bDAO = BoardDAO.getInstance();
		int row = bDAO.boardInsert(bVO);
		request.setAttribute("nowpage", nowpage);
		if(row==1) {
			RequestDispatcher rd = request.getRequestDispatcher("Board?command=board_list");
			rd.forward(request, response);
				
		};
	}
	

}
