package boardmvc.controller.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import boardmvc.controller.Action;
import boardmvc.db.BoardDAO;
import boardmvc.db.BoardVO;

public class BoardModifyProAction implements Action{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int idx = Integer.parseInt(request.getParameter("idx"));
		BoardDAO bDAO = BoardDAO.getInstance();
		BoardVO bVO = bDAO.boardListNo(idx);
		
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String contents = request.getParameter("contents");
		
		bVO.setEmail(email);
		bVO.setSubject(subject);
		bVO.setContents(contents);
				
		bDAO.boardEdit(bVO);
		
		request.setAttribute("bVO", bVO);
		RequestDispatcher ds = request.getRequestDispatcher("/Board?command=board_view&idx="+idx);
		ds.forward(request, response);
		
	}
		

}
