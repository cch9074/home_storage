create table tbl_board (
	idx number(4) not null,
	name varchar2(20) not null,
	email varchar2(20),
	subject varchar2(100) not null,
	contents varchar2(4000) not null,
	pass varchar2(20) not null,
	regdate date not null,
	readcnt number(3) not null,
	primary key(idx)
	);
create sequence tbl_board_seq;
